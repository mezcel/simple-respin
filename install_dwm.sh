#!/bin/bash

function set_user_permission {
    me=$(whoami)

    if [ $me == "root" ]; then
        echo ""
        read -p "Give a user (sudo/sudoer) privilege? [y/n] " yn
        
        if [ $yn == "y" ]; then
            read -p "Enter a user name: " myname
            adduser $myname sudo

            if [ -d /etc/apk ]; then
                ## Alpine linux

                clear
                echo -e "Manually add user name to sudoers\n\tExample:\n\tFind \"root\" and enter the following:\n\t\t myusername ALL=(ALL) ALL\n"
                read -p "[press enter to continue]" pauseprompt
                visudo
                echo -e "\n---\n"
            else
                usermod -aG sudo $myname
            fi
        fi
    fi
}

################################################################################
## Debian Linux Packages
################################################################################

function suckless_dependancy_debian {
    sudo apt update
    sudo apt --fix-broken install
    sudo apt update

    sudo apt install -y build-essential

    sudo apt install -y git
    mkdir -p ~/github/git
    git clone https://github.com/git/git.git ~/github/git/git.git

    ## If Not WLS
    if [ $(uname -r | grep Microsoft &> /dev/null; echo $?) -ne 0 ]; then
        sudo apt install -y xinit
        sudo apt install -y arandr
        # sudo apt install --no-install-recommends xserver-xorg-core xserver-xorg-input-all xserver-xorg-video-fbdev xserver-xorg-video-(your videocard, can be intel, nouveau, or ati) dbus
    fi

    ## dwm requirements
    sudo apt install -y libx11-dev
    sudo apt install -y libxft-dev
    sudo apt install -y libxinerama-dev
    sudo apt install -y xclip
    sudo apt install -y xvkbd
    sudo apt install -y libgcr-3-dev
    sudo apt install -y suckless-tools

    if [ -f ~/.xinitrc ]; then ~/.xinitrc.backup.$(date +%d%b%Y_%H%M%S); fi
    echo "exec dwm" > ~/.xinitrc
}

function terminal_apps_debian {
    sudo apt install -y vim
    sudo apt install -y tmux
    sudo apt install -y ranger
    sudo apt install -y aspell
    sudo apt install -y wget
    sudo apt install -y curl
    sudo apt install -y elinks
    sudo apt install -y bc
    sudo apt install -y openssh-server
    sudo apt install -y openssh-client

    ## If Not WLS
    if [ $(uname -r | grep Microsoft &> /dev/null; echo $?) -ne 0 ]; then
        sudo apt install -y alsa-utils
        sudo apt install -y pavucontrol
        sudo apt install -y mplayer
    else
        sudo apt install -y iputils-ping
    fi

    ranger --copy-config=all
}

function gtk_apps_debian {
    ## Xorg DE apps

    sudo apt install -y feh

    sudo apt install -y geany
    sudo apt install -y geany-plugins
    sudo apt install -y mousepad
    sudo apt install -y zathura

    sudo apt install -y pcmanfm
    sudo apt install -y libfm-tools
    sudo apt install -y libusbmuxd-tools
    sudo apt install -y udiskie
    sudo apt install -y xarchiver
    sudo apt install -y gvfs
    sudo apt install -y tumbler
    sudo apt install -y ffmpegthumbnailer

    sudo apt install -y gparted

    ## Crazy light weight screensaver
    # sudo apt install xfishtank

    ## PLAN9's Acme text editor
    ## guidance: https://github.com/evbogdanov/acme
    #sudo apt-get install gcc libx11-dev libxt-dev libxext-dev libfontconfig1-dev
    #git clone https://github.com/9fans/plan9port $HOME/plan9
    #cd $HOME/plan9/plan9port
    #./INSTALL -r $HOME/plan9

    ## If Not WLS
    if [ $(uname -r | grep Microsoft &> /dev/null; echo $?) -ne 0 ]; then
        sudo apt install -y iceweasel
    fi
}

################################################################################
## Archlinux Packages
################################################################################

function suckless_dependancy_archlinux {
    if [ -f /var/lib/pacman/db.lock ]; then sudo rm /var/lib/pacman/db.lock; fi
    sleep 1s

    sudo pacman -Sy

    sudo pacman -S --noconfirm --needed base
    sudo pacman -S --noconfirm --needed base-devel

    sudo pacman -S --noconfirm --needed reflector
    sudo reflector --country 'United States' --sort rate --save /etc/pacman.d/mirrorlist
    sleep 1s

    sudo pacman -S --noconfirm --needed gcc
    sudo pacman -S --noconfirm --needed cmake
    sudo pacman -S --noconfirm --needed extra-cmake-modules

    sudo pacman -S --noconfirm --needed git

    ## dwm requirements
    sudo pacman -S --noconfirm --needed xorg
    sudo pacman -S --noconfirm --needed xorg-xinit
    sudo pacman -S --noconfirm --needed xorg-server
    sudo pacman -S --noconfirm --needed xorg-server-utils
    sudo pacman -S --noconfirm --needed xorg-apps
    sudo pacman -S --noconfirm --needed xorg-xrandr
    sudo pacman -S --noconfirm --needed xorg-xfontsel

    sudo pacman -S --noconfirm --needed arandr

    ## An Archlinux repo list version
    sudo pacman -S --noconfirm --needed st
    sudo pacman -S --noconfirm --needed dmenu
    sudo pacman -S --noconfirm --needed dwm
}

function terminal_apps_archlinux {
    sudo pacman -S --noconfirm --needed vim
    sudo pacman -S --noconfirm --needed tmux
    sudo pacman -S --noconfirm --needed ranger
    sudo pacman -S --noconfirm --needed aspell
    sudo pacman -S --noconfirm --needed hunspell
    sudo pacman -S --noconfirm --needed ispell
    sudo pacman -S --noconfirm --needed bc

    sudo pacman -S --noconfirm --needed p7zip
    sudo pacman -S --noconfirm --needed zip
    sudo pacman -S --noconfirm --needed unzip
    sudo pacman -S --noconfirm --needed unrar

    sudo pacman -S --noconfirm --needed alsa
    sudo pacman -S --noconfirm --needed alsa-utils
    sudo pacman -S --noconfirm --needed alsa-firmware
    sudo pacman -S --noconfirm --needed pulseaudio
    sudo pacman -S --noconfirm --needed pulseaudio-alsa
    sudo pacman -S --noconfirm --needed pavucontrol
    sudo pacman -S --noconfirm --needed vorbis-tools

    sudo pacman -S --noconfirm --needed openssh
    sudo pacman -S --noconfirm --needed ifplugd
    sudo pacman -S --noconfirm --needed iw

    sudo pacman -S --noconfirm --needed mplayer

    ## Unconfigured (Used for SSHD Only)
    ## Untill enabled netctl will be the default
    sudo pacman -S --noconfirm --needed networkmanager
}

function gtk_apps_archlinux {
    sudo pacman -S --noconfirm --needed xterm
    sudo pacman -S --noconfirm --needed gtk2
    sudo pacman -S --noconfirm --needed gtk3

    sudo pacman -S --noconfirm --needed feh

    sudo pacman -S --noconfirm --needed gvim
    sudo pacman -S --noconfirm --needed geany
    sudo pacman -S --noconfirm --needed geany-plugins
    sudo pacman -S --noconfirm --needed mousepad

    sudo pacman -S --noconfirm --needed mupdf
    sudo pacman -S --noconfirm --needed pcmanfm
    sudo pacman -S --noconfirm --needed gvfs
    sudo pacman -S --noconfirm --needed tumbler
    sudo pacman -S --noconfirm --needed xarchiver
    sudo pacman -S --noconfirm --needed evince
    sudo pacman -S --noconfirm --needed udiskie
    sudo pacman -S --noconfirm --needed ffmpegthumbnailer
    sudo pacman -S --noconfirm --needed poppler-glib
    sudo pacman -S --noconfirm --needed libgsf
    sudo pacman -S --noconfirm --needed libopenraw

    sudo pacman -S --noconfirm --needed firefox

    ## Extra office suite (Not Suckless but useful)

    sudo pacman -S --needed --noconfirm libreoffice-still
    sudo pacman -S --needed --noconfirm libreoffice-extension-texmaths
    sudo pacman -S --needed --noconfirm libreoffice-extension-writer2latex

    sudo pacman -S --needed --noconfirm texlive-most
    sudo pacman -S --needed --noconfirm biber
    sudo pacman -S --needed --noconfirm texstudio
    sudo pacman -S --needed --noconfirm pandoc
    sudo pacman -S --needed --noconfirm mupdf

    sudo pacman -S --noconfirm --neeed zenity
    sudo pacman -S --noconfirm --neeed glade
    sudo pacman -S --noconfirm --neeed gimp
}

################################################################################
## Alpine Linux Packages
################################################################################

function suckless_dependancy_alpine {

    sudo apk add git
    sudo apk add make
    sudo apk add gcc
    sudo apk add g++
    sudo apk add libx11-dev
    sudo apk add libxft-dev
    sudo apk add libxinerama-dev 
    sudo apk add ncurses
    sudo apk add dbus-x11

    sudo setup-xorg-base
    sudo apk add xinit
    sudo apk add xorg-server 
    sudo apk add xf86-video-vesa
    sudo apk add xf86-input-evdev
    sudo apk add xf86-input-mouse
    sudo apk add xf86-input-keyboard
    sudo apk add udev

    ## find appropriate video
    ## sudo apk search xf86-video

    ###### GPU
    ##  PCI buses in the system and devices connected to them
    sudo apk add pciutils

    ## AMD
    lspci | grep "AMD"
    if [ $? -eq 0 ]; then
        sudo apk add xf86-video-amdgpu
        sudo apk add xf86-video-ati
    fi

    ## Intel
    lspci | grep "Intel"
    if [ $? -eq 0 ]; then
        sudo apk add xf86-video-intel
    fi

}

function terminal_apps_alpine {

    
    if [ "$USER" == "root" ]; then
        apk add sudo sudo-doc sudo-dev
    fi

    isSudo=$(command -v sudo)
    if [ $isSudo != "/usr/bin/sudo" ]; then
        echo -e "\nsudo is not installed.\n\tSudo is required to install apps\n\t"
        exit
    fi

    sudo apk add build-base
    sudo apk add linux-headers

    sudo apk add bash bash-doc bash-dev
    sudo apk add bash-completion 
    #sudo apk add tzdata

    sudo apk add wireless-tools wireless-tools-doc wireless-tools-dev 
    sudo apk add wpa_supplicant wpa_supplicant-doc wpa_supplicant-openrc

    sudo apk add attr dialog dialog-doc
    sudo apk add grep grep-doc

    sudo apk add util-linux 
    sudo apk add util-linux-doc 
    sudo apk add pciutils 
    sudo apk add usbutils 
    sudo apk add binutils 
    sudo apk add findutils 
    sudo apk add readline

    sudo apk add git git-doc 
    sudo apk add make make-doc 
    sudo apk add gcc gcc-doc 
    sudo apk add g++ 
    sudo apk add libx11-dev 
    sudo apk add libxft-dev 
    sudo apk add libxinerama-dev 
    sudo apk add ncurses 
    sudo apk add dbus-x11
    sudo apk add autoconf 
    sudo apk add glib 
    sudo apk add automake automake-doc 
    sudo apk add libtool 
    sudo apk add intltool 
    sudo apk add perl-xml-parser

    sudo apk add tmux tmux-doc 
    sudo apk add gdb gdb-doc 
    sudo apk add ranger ranger-doc
    sudo apk add bc bc-doc 
    sudo apk add aspell aspell-doc aspell-dev aspell-en 
    sudo apk add hunspell hunspell-doc hunspell-dev hunspell-en
    sudo apk add wget wget-doc 
    sudo apk add elinks elinks-doc elinks-lang
    sudo apk add zip zip-doc
    sudo apk add cfdisk
    
    #sudo apk add alpine-sdk

    ## idk
    sudo apk add man man-pages mdocml-apropos less less-doc nginx

    ## First you will need to install Alsa packages, the Linux sound driver and volume adjuster.
    sudo apk add alsa-utils alsa-utils-doc 
    sudo apk add alsa-lib 
    sudo apk add alsaconf

    ## Then you will need to add all your users (including root) to the audio group
    sudo addgroup $USER audio
    sudo addgroup root audio

    ## Alsa service is not started on install, you need to start it and to add it on rc
    sudo rc-service alsa start
    sudo rc-update add alsa

    sudo apk add pulseaudio 
    sudo apk add pulseaudio-alsa
    sudo apk add alsa-plugins-pulse

    sudo apk add curl-dev 
    sudo apk add flac-dev 
    sudo apk add libvorbis-dev 
    sudo apk add libao-dev 
    sudo apk add speex-dev
    sudo apk add vorbis-tools

    sudo apk add libxxf86dga-dev 
    sudo apk add libxv-dev 
    sudo apk add libmad-dev 
    sudo apk add lame-dev 
    sudo apk add libao-dev
    sudo apk add libtheora-dev 
    sudo apk add xvidcore-dev 
    sudo apk add zlib-dev 
    sudo apk add sdl-dev 
    sudo apk add freetype-dev
    sudo apk add x264-dev 
    sudo apk add faac-dev 
    sudo apk add ttf-dejavu 
    sudo apk add libxvmc-dev 
    sudo apk add alsa-lib-dev 
    sudo apk add live-media-dev
    sudo apk add mesa-dev 
    sudo apk add yasm 
    sudo apk add libpng-dev 
    sudo apk add libvdpau-dev 
    sudo apk add libvpx-dev 
    sudo apk add libcdio-paranoia-dev

    sudo apk add ttf-font-awesome 
    sudo apk add ttf-hack 
    sudo apk add ttf-inconsolata 
    sudo apk add font-inconsolata-nerd
    sudo apk add ttf-ubuntu-font-family

    ## vim cache
    mkdir -p ~/.backup
    mkdir -p ~/.swp
    mkdir -p ~/.undo

    mkdir -p ~/Downloads

    ## customize Almquist ash
    echo 'PS1="\u@\h:\w\a$ "' > ~/.profile
}

function gtk_apps_alpine {
    sudo apk add feh feh-doc 
    sudo apk add pcmanfm pcmanfm-doc 
    sudo apk add zathura
    sudo apk add udisks2-dev 
    sudo apk add udisks-dev 
    sudo apk add lvm2 
    sudo apk add xarchiver 
    sudo apk add gvfs 
    sudo apk add tumbler 
    sudo apk add ffmpegthumbnailer 
    sudo apk add libfm-dev 
    sudo apk add libusbmuxd-dev 
    sudo apk add libusb libusb-dev

    sudo apk add geany geany-dev 
    sudo apk add geany-plugins geany-plugins-dev
    sudo apk add geany-plugins-doc
    sudo apk add geany-plugins-spellcheck 
    sudo apk add geany-plugins-addons 
    sudo apk add geany-plugins-treebrowser 
    sudo apk add geany-plugins-geanyvc 
    sudo apk add geany-plugins-addons 
    sudo apk add geany-plugins-lang 
    sudo apk add geany-plugins-utils

    sudo apk add adwaita-gtk2-theme 
    sudo apk add adwaita-icon-theme 
    sudo apk add gnome-icon-theme

    sudo apk add vlc vlc-doc vlc-dev
    sudo apk add vlc-daemon

    sudo apk add qutebrowser qutebrowser-doc
}

################################################################################
## Setup
################################################################################

function suckless_patches {
    ## Backup and reference/backup copy of suckless tools
    mkdir -p ~/suckless/factory-default/
    cp -rf $(dirname $0)/patches ~/suckless/

    git clone git://git.suckless.org/dwm ~/suckless/factory-default/dwm
    git clone git://git.suckless.org/st ~/suckless/factory-default/st
    git clone git://git.suckless.org/slstatus ~/suckless/factory-default/slstatus
    git clone https://git.suckless.org/dmenu ~/suckless/factory-default/dmenu
    git clone https://git.suckless.org/surf ~/suckless/factory-default/surf

    ## Install suckless tools with patches

    cd ~/suckless/patches/st
    ./auto_st_patch.sh

    cd ~/suckless/patches/slstatus
    ./auto_slstatus_patch.sh

    cd ~/suckless/patches/dmenu
    ./auto_dmenu_patch.sh

    cd ~/suckless/patches/dwm
    ./auto_dwm_patch.sh
}

function custom_themes {
    echo ""
    read -e -p "Install my custom themes? [y/n]: " -i "n" yn
    if [ $yn == "y" ]; then cd $install_dwm_root; bash install_themes.sh; fi
}

function install_prompt {
    echo -e "\n## DWM Installer ##\n"
    echo -e "# How do you want to install DWM?\
            \n  1. DWM with prefered terminal apps\
            \n  2. DWM with prefered GTK apps\
            \n  3. DWM with themes\
            \n  4. ALL\n"
    read -e -p "# Select a number? [1-3]: " -i "4" installNo
    echo ""
}

function setup_git {
    echo -e "\n\nPreparing Git got Github:\n\n"
    read -p "Enter your github user name: " githubusername
    echo ""
    read -p "Enter github user email: " githubuseremail

    git config --global user.name $githubusername
    git config --global user.email $githubuseremail
    echo ""
}

################################################################################
## Main
################################################################################

function main {
    install_dwm_root=$(pwd)
    install_prompt

    if [ $installNo -lt 1 ]; then echo "Exited installer. " && exit; fi

    sudo ping -c3 google.com &>/dev/null
    pingTest=$?
    if [ $pingTest -ne 0 ]; then echo "Installer aborted. No internet." && exit; fi

    ## Install primary software applications
    if [ -d /etc/pacman.d ]; then
        command -v xinit &>/dev/null
        isXinit=$?

        if [ $isXinit -ne 0 ]; then
            echo -e "Notice !!!\n\"xinit\" was not detected.\n\tPerhaps this is a fresh install."
            echo -e "\tTry using one of these installers either to install or to configure the server:\n\tDirectory path is \"$(pwd)/arch-files/\"\n"
            ls -l arch-files/*.sh
            echo -e "\nDo you want to use any of the above scripts?\n\tSelecting \"n\" will continue with the installation script.\n\tSelecting \"y\" will exit the script and \"cd\" into the script directory.\n"
            echo "If this is a fresh Archlinux installation, use \"1.0-iso-install.sh\""
            read -e -p "Use one of these script? [y/n]: " -i "n" yn
            if [ $yn == "y" ]; then
                cd $(pwd)/arch-files/
                pwd
                ls
                echo -e "\tcd into \"$(pwd)/arch-files/\", and run one of the scripts.\n"
                exit
            fi
        fi

        suckless_dependancy_archlinux

        if [ $installNo -ge 1 ]; then terminal_apps_archlinux; fi
        if [ $installNo -ge 2 ]; then gtk_apps_archlinux; fi

        if [ -f ~/.tmux.conf ]; then ~/.tmux.conf.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.tmux.conf ~/

        if [ -f ~/.toprc ]; then ~/.toprc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.toprc ~/

        if [ -f ~/.vimrc ]; then ~/.vimrc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.vimrc_archlinux ~/.vimrc

    elif [ -f /etc/apt/sources.list ]; then
        set_user_permission
        suckless_dependancy_debian

        if [ $installNo -ge 1 ]; then terminal_apps_debian; fi
        if [ $installNo -ge 2 ]; then gtk_apps_debian; fi

        if [ -f ~/.tmux.conf ]; then ~/.tmux.conf.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.tmux.conf ~/

        if [ -f ~/.toprc ]; then ~/.toprc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.toprc ~/

        if [ -f ~/.vimrc ]; then ~/.vimrc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.vimrc_debian ~/.vimrc

    elif [ -d /etc/apk ]; then
        set_user_permission
        suckless_dependancy_alpine

        if [ $installNo -ge 1 ]; then terminal_apps_alpine; fi
        if [ $installNo -ge 2 ]; then gtk_apps_alpine; fi

        if [ -f ~/.tmux.conf ]; then ~/.tmux.conf.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.tmux.conf ~/

        if [ -f ~/.toprc ]; then ~/.toprc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.toprc ~/

        if [ -f ~/.vimrc ]; then ~/.vimrc.backup.$(date +%d%b%Y_%H%M%S); fi
        cp $(dirname $)/home/.vimrc_debian ~/.vimrc
    fi

    suckless_patches
    command -v git &>/dev/null
    if [ $? -eq 0 ]; then setup_git; fi

    if [ $installNo -ge 3 ]; then custom_themes; fi
}

################################################################################
## Run
################################################################################

isSudo=$(command -v sudo)

if [ $isSudo == "/usr/bin/sudo" ]; then
    main
else
    echo -e "\nScript terminated."
    echo -e "\tSudo will be required and sudo is not installed\n"
fi
