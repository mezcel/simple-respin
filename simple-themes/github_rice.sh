#!/bin/bash

function detect_packageManager {
    if [ -f /etc/pacman.conf ]; then
        packagemanager=pacman
    elif [ -f /etc/apt/sources.list ]; then
        packagemanager=aptdpkg
    elif [ -d /etc/apk ]; then
        packagemanager=apk
    fi
}

function retro_terminal {
    ## cool-retro-term
    if [ -d /etc/pacman.d ]; then
        sudo pacman -S --needed qt5-base qt5-declarative qt5-quickcontrols qt5-graphicaleffects
        sudo pacman -S --needed cool-retro-term
    elif [ -f /etc/apt/sources.list ]; then
        sudo apt install build-essential qmlscene qt5-qmake qt5-default qtdeclarative5-dev qml-module-qtquick-controls qml-module-qtgraphicaleffects qml-module-qtquick-dialogs qml-module-qtquick-localstorage qml-module-qtquick-window2 qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel

        git clone --recursive https://github.com/Swordfish90/cool-retro-term.git ~/github/Swordfish90/cool-retro-term

        cd ~/github/Swordfish90/cool-retro-term
        qmake && make
        ./cool-retro-term
        aliasCount=$(grep -o -i "alias cool-retro-term=" ~/.bashrc | wc -l)
        if [ $aliasCount -lt 1 ]; then
            echo -e "alias cool-retro-term=\"~/github/Swordfish90/cool-retro-term/cool-retro-term\"" >> ~/.bashrc
        elif [ $aliasCount -ge 1 ]; then
            grep -v "alias cool-retro-term=" ~/.bashrc > temp && mv temp ~/.bashrc
            echo -e "alias cool-retro-term=\"~/github/Swordfish90/cool-retro-term/cool-retro-term\"" >> ~/.bashrc
        fi
    fi

}

function github_packages {
    mkdir -p ~/github/

    command -v git &>/dev/null
    if [$? -eq 1]; then
        echo -e "\nTerminated.\n\tgit is required to continue.\n"
        exit
    fi

    ## GTK themes
    git clone https://github.com/EliverLara/Nordic.git ~/.themes/Nordic
    git clone https://github.com/EliverLara/Nordic-Polar.git ~/.themes/Nordic-Polar

    ## Gtk icon theme
    git clone https://github.com/Bonandry/adwaita-plus.git ~/github/Bonandry/adwaita-plus
    cd ~/github/Bonandry/adwaita-plus/
    sudo bash install.sh

    ## Nord terminal colors
    git clone https://github.com/khamer/base16-termite.git ~/github/khamer/base16-termite

    git clone https://github.com/geany/geany-themes.git ~/github/geany/geany-themes
    cd ~/github/geany/geany-themes
    bash install.sh

    ## Another solarized theme pack for the tty
    git clone https://github.com/joepvd/tty-solarized.git ~/github/joepvd/tty-solarized

    ## A library of st themes
    git clone https://github.com/honza/base16-st.git ~/github/honza/base16-st

    ## Not WLS
    if [ $(uname -r | grep Microsoft &>/dev/null; echo $?) -ne 0  ]; then
        ## terminal fonts
        command -v ruby &>/dev/null
        if [$? -eq 0]; then
            ## powerline font (requires ruby)
            git clone https://github.com/powerline/fonts.git ~/github/powerline/fonts
            sudo mkdir -p /usr/share/consolefonts/
            sleep 2s
            sudo cp -rf ~/github/powerline/fonts/Terminus/PSF/*.psf.gz /usr/share/consolefonts/
            sleep 2s

            ## Nerdfonts (2.2G)
            ## git clone https://github.com/ryanoasis/nerd-fonts.git ~/github/ryanoasis/nerd-fonts
            #git clone https://github.com/just3ws/nerd-font-cheatsheets.git ~/github/just3ws/nerd-font-cheatsheets
        fi

        ## tty console
        ## solarized
        git clone https://github.com/adeverteuil/console-solarized.git ~/github/adeverteuil/console-solarized
        sudo cp ~/github/adeverteuil/console-solarized/console-solarized /usr/local/bin/
        sudo cp ~/github/adeverteuil/console-solarized/console-solarized.conf /etc/
        sudo cp ~/github/adeverteuil/console-solarized/console-solarized@.service /etc/systemd/system/
        sudo mkdir -p /etc/systemd/system/getty@.service.d/
        sleep 2s
        sudo cp ~/github/adeverteuil/console-solarized/solarized.conf /etc/systemd/system/getty@.service.d/
        sleep 2s
        sudo systemctl daemon-reload

        ## nord tty
        git clone https://github.com/lewisacidic/nord-tty.git ~/github/lewisacidic/nord-tty
        git clone https://github.com/arcticicestudio/nord-xresources.git ~/github/arcticicestudio/nord-xresources

        if [ ! -d /etc/apk ]; then
            ## If not-Alpine
            ## A decorative Grub theme
            git clone https://github.com/shvchk/poly-dark.git ~/github/shvchk/poly-dark
            sleep 1s
            cd ~/github/shvchk/poly-dark/
            sleep 1s
            chmod +x install.sh
            sleep 1s
            sudo ./install.sh
            sleep 1s
        fi
    fi

    ## cool-retro-term
    #retro_terminal

    echo -e "\nFinished adding themes from Github.\n"
    sleep 2s

}

function install_github_themes {
    sudo ping -c3 google.com
    if [ $? -ne 0 ]; then
        echo "Aborted installation because google.com was not pinged."
        exit
    else
        detect_packageManager

        if [ $packagemanager == "pacman" ]; then
            sudo pacman -S --needed --noconfirm git ruby
            sudo pacman -S --noconfirm --needed lxappearance
            sudo pacman -S --noconfirm --needed arc-solid-gtk-theme
            sudo pacman -S --noconfirm --needed gnome-themes-extra
        elif [ $packagemanager == "aptpkg" ]; then
            sudo apt install git ruby
            sudo apt install -y lxappearance
            sudo apt install -y arc-theme
            sudo apt install -y gnome-themes-extra
        fi

        github_packages
    fi
}

install_github_themes

