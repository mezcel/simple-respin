#!/bin/bash

##
## ~/.dwm_xinit_colors
## set gtk2, gtk3, cli, and dwm
##

## optional input color
inputColor=$1

function decorativeColors {
	## Foreground Color using ANSI escape
	FG_BLACK=$(tput setaf 0)
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)
	FG_WHITE=$(tput setaf 7)
	FG_NoColor=$(tput sgr0)

	## Background Color using ANSI escape
	BG_BLACK=$(tput setab 0)
	BG_RED=$(tput setab 1)
	BG_GREEN=$(tput setab 2)
	BG_YELLOW=$(tput setab 3)
	BG_BLUE=$(tput setab 4)
	BG_MAGENTA=$(tput setab 5)
	BG_CYAN=$(tput setab 6)
	BG_WHITE=$(tput setab 7)
	BG_NoColor=$(tput sgr0)

	## set mode using ANSI escape
	MODE_BOLD=$(tput bold)
	MODE_DIM=$(tput dim)
	MODE_BEGIN_UNDERLINE=$(tput smul)
	MODE_EXIT_UNDERLINE=$(tput rmul)
	MODE_REVERSE=$(tput rev)
	MODE_ENTER_STANDOUT=$(tput smso)
	MODE_EXIT_STANDOUT=$(tput rmso)

	# clear styles using ANSI escape
	STYLES_OFF=$(tput sgr0)
}

function inputPrompt {
	if [ -z $inputColor ]; then
		echo -e "\n## Compile DWM and load a GTK theme ##"
		echo "${FG_GREEN}"
		echo -e "Available themes:\n\t* solarized\n\t* red\n\t* blue\n\t* nord\n\t* black\n\t* cream\n\t* default\n"
		read -e -p "Select theme? (leave blank to cancel): " -i "solarized" inputColor
		echo "${STYLES_OFF}"
	fi
}

function dependencyCheck {
	echo -e "${MODE_ENTER_STANDOUT}\nChecking feh ...${STYLES_OFF}"
	isFeh=$(command -v feh &>/dev/null; echo $?)
	sleep 1s
	echo -e "${MODE_ENTER_STANDOUT}\nChecking dwm ...${STYLES_OFF}"
	isDwm=$(command -v dwm &>/dev/null; echo $?)
	sleep 1s

	## Dependancy Check

	if [ ! -d ~/terminalsexy/Xresources ] || [ ! -d ~/terminalsexy/Linux_Console ]; then
		echo -e "${BG_RED}\nExited Script.\nOne of the following dirs were not found\n\t~/terminalsexy/Xresources\n\t~/terminalsexy/Linux_Console\n ${STYLES_OFF}"
		read -p "[press any key to continue]"
		exit
	elif [ ! -d ~/suckless/dwm ] || [ ! -d ~/suckless/st ] ; then
		echo -e "${BG_RED}\nExited Script.\nOne of the following dirs were not found\n\t~/suckless/dwm\n\t~/.themes\n\t~/suckless/st\n ${STYLES_OFF}"
		read -p "[press any key to continue]"
		exit
	elif [ $isFeh -eq 1 ]; then
		echo -e "${BG_RED}\nExited Script.\nThe \"feh\" image viewer was no detected.\n ${STYLES_OFF}"
		read -p "[press any key to continue]"
		exit
	elif [ $isDwm -eq 1 ]; then
		echo -e "${BG_RED}\nExited Script.\nDWM was no detected.\n ${STYLES_OFF}"
		read -p "[press any key to continue]"
		exit
	else
		echo -e "${BG_GREEN}\nPassed dependency check ${STYLES_OFF}"
		sleep 1s
	fi
}

function setGtkRC {
	gtkthemename=$1
	gtkiconthemename=$2

	## mod gtk2
	echo -e "$promptFG$promptBG\nSet GTK2 ${STYLES_OFF}"

	if [ -f ~/.gtkrc-2.0 ]; then
		cp ~/.gtkrc-2.0 ~/.gtkrc-2.0.backup.$(date +%d%b%Y_%H%M%S)

		grep -v "gtk-theme-name" ~/.gtkrc-2.0 > temp && mv temp ~/.gtkrc-2.0
		grep -v "gtk-icon-theme-name" ~/.gtkrc-2.0 > temp && mv temp ~/.gtkrc-2.0
		sleep 1s
	else
		#echo -e "include \"~/.gtkrc-2.0.mine\" " > ~/.gtkrc-2.0
		echo -e "gtk-cursor-theme-name=\"Adwaita\" " >> ~/.gtkrc-2.0
		echo -e "gtk-toolbar-style=GTK_TOOLBAR_BOTH " >> ~/.gtkrc-2.0
		echo -e "gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR " >> ~/.gtkrc-2.0
		echo -e "gtk-button-images=1 " >> ~/.gtkrc-2.0
		echo -e "gtk-menu-images=1 " >> ~/.gtkrc-2.0
		echo -e "gtk-xft-hintstyle=\"hintfull\" " >> ~/.gtkrc-2.0
		sleep 1s
	fi

	## Append script
	echo "gtk-theme-name=\"$gtkthemename\"" > ~/.gtkrc-2.0
	echo "gtk-icon-theme-name=\"$gtkiconthemename\"" >> ~/.gtkrc-2.0
	sleep 2s

	## mod gtk3
	echo -e "$promptFG$promptBG\nSet GTK3 ${STYLES_OFF}"

	mkdir -p ~/.config/gtk-3.0/
	if [ -f ~/.config/gtk-3.0/settings.ini ]; then
		cp ~/.config/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini.backup.$(date +%d%b%Y_%H%M%S)

		## delete current setting
		grep -v "gtk-theme-name" ~/.config/gtk-3.0/settings.ini > temp && mv temp ~/.config/gtk-3.0/settings.ini
		grep -v "gtk-icon-theme-name" ~/.config/gtk-3.0/settings.ini > temp && mv temp ~/.config/gtk-3.0/settings.ini
		sleep 1s
	else
		echo "[Settings]" > ~/.config/gtk-3.0/settings.ini
	fi

	## "[settings]" count
	settingsCount=$(grep -o -i "\[Settings\]" ~/.config/gtk-3.0/settings.ini | wc -l)
	if [ $settingsCount -lt 2 ]; then
		## only need 1, but 2 sugests that I manually parsed and appended it before
		## redundant unless some other app added specific theme settings
		echo "[Settings]" >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-toolbar-style=GTK_TOOLBAR_BOTH " >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR " >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-button-images=1 " >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-menu-images=1 " >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-cursor-theme-name=Adwaita " >> ~/.config/gtk-3.0/settings.ini
		echo "gtk-xft-hintstyle=hintfull " >> ~/.config/gtk-3.0/settings.ini
		sleep 1s
	fi

	## Append script
	echo "gtk-theme-name=$gtkthemename" >> ~/.config/gtk-3.0/settings.ini
	echo "gtk-icon-theme-name=$gtkiconthemename" >> ~/.config/gtk-3.0/settings.ini
	sleep 1s
}

function setWallpaper {
	pictureFile=$1

	## write an new ~/.fehbg
	echo -e "#!/bin/sh\nfeh --no-fehbg --bg-fill $pictureFile" > ~/.fehbg
	echo -e "$promptFG$promptBG\nSet wallpaper ${STYLES_OFF}"
	sleep 2s
}

function setDwmConfigs {
	dwmConfig=$1

	## load dwm config.h
	echo -e "$promptFG$promptBG\nUpdate dwm/config.h ${STYLES_OFF}"
	sudo cp $dwmConfig ~/suckless/dwm/config.h
	sleep 2s

	echo -e "$promptFG$promptBG\nCompile DWM ... ${STYLES_OFF}"
	sleep 1s
	echo ""
	cd ~/suckless/dwm
	sudo make clean
	echo ""
	sleep 1s
	sudo make install
	sleep 2s
}

function setCli {
	linuxTtyExecutable=$1
	xreseourcesFile=$2
	stTheme=$3

	bash $linuxTtyExecutable
	echo -e "$promptFG$promptBG\nSet console Linux TTY colors ${STYLES_OFF}"
	sleep 1s

	cp $xreseourcesFile ~/.Xresources
	echo -e "$promptFG$promptBG\nSet Xresources Cli colors ${STYLES_OFF}"
	sleep 1s
	echo ""

	bash ~/suckless/patches/st/auto_st_patch.sh $stTheme
}

function setGeanySyntaxColor {
	geanyConf=$1
	geanyVteFg=$2
	geanyVteBg=$3

	if [ -f ~/.config/geany/geany.conf ]; then
		grep -v "color_scheme=" ~/.config/geany/geany.conf > temp && mv temp ~/.config/geany/geany.conf
		grep -v "colour_fore=" ~/.config/geany/geany.conf > temp && mv temp ~/.config/geany/geany.conf
		grep -v "colour_back=" ~/.config/geany/geany.conf > temp && mv temp ~/.config/geany/geany.conf

		echo -e "\n[geany]" >> ~/.config/geany/geany.conf
		echo "color_scheme=$geanyConf" >> ~/.config/geany/geany.conf

		echo -e "\n[VTE]" >> ~/.config/geany/geany.conf
		echo "colour_fore=$geanyVteFg" >> ~/.config/geany/geany.conf
		echo "colour_back=$geanyVteBg" >> ~/.config/geany/geany.conf
	fi

	echo -e "$promptFG$promptBG\nSet Geany syntax highlighting ${STYLES_OFF}"
	sleep 1s
}

function applyConfigPaths {
	setCli $linuxTtyExecutable $xreseourcesFile $stTheme
	setGeanySyntaxColor $geanyConf $geanyVteFg $geanyVteBg

	setWallpaper $pictureFile
	setGtkRC $gtkthemename $gtkiconthemename
	setDwmConfigs $dwmConfig
}

function defineConfigPaths {
	case "$inputColor" in
		"solarized" )
			## prompt colors
			promptFG=$FG_BLACK
			promptBG=$BG_CYAN

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/solarized.dark.sh
			xreseourcesFile=~/terminalsexy/Xresources/solarized.dark
			geanyConf=solarized-dark.conf
			geanyVteFg=#081215
			geanyVteBg=#5C6A00
			stTheme=solarized

			## gtk
			gtkthemename=Nordic-Solarized
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-solarized-medium.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_solarized.h
			;;
		"red" )
			## prompt colors
			promptFG=$FG_WHITE
			promptBG=$BG_RED

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/myRed.light.sh
			xreseourcesFile=~/terminalsexy/Xresources/myRed.light
			geanyConf=darcula_red.conf
			geanyVteFg=#480B0B
			geanyVteBg=#BD5454
			stTheme=dracula

			## gtk
			gtkthemename=Nordic-Red
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-red-black.png
			dwmConfig=~/suckless/dwm/config_red.h
			;;
		"blue" )
			## prompt colors
			promptFG=$FG_WHITE
			promptBG=$BG_BLUE

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/ocean.dark.sh
			xreseourcesFile=~/terminalsexy/Xresources/myBlue.light
			geanyConf=mc_mezcel.conf
			geanyVteFg=#85ADD1
			geanyVteBg=#0359B9
			stTheme=nordtheme

			## gtk
			gtkthemename=Nordic-Blue
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-cream-blue.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_blue.h
			;;
		"nord" )
			## prompt colors
			promptFG=$FG_BLACK
			promptBG=$BG_CYAN

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/ocean.dark.sh
			xreseourcesFile=~/terminalsexy/Xresources/myNord.light
			geanyConf=nord.conf
			geanyVteFg=#2F343F
			geanyVteBg=#8FBCBB
			stTheme=nordtheme

			## gtk
			gtkthemename=Nordic
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-nord.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_nord.h
			;;
		"black")
			## prompt colors
			promptFG=$FG_BLACK
			promptBG=$FG_WHITE

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/grayscale.dark.sh
			xreseourcesFile=~/terminalsexy/Xresources/grayscale.dark
			geanyConf=monokai.conf
			geanyVteFg=#BFBFBF
			geanyVteBg=#000000
			stTheme=black

			## gtk
			gtkthemename=Adwaita-dark
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-black-white.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_black.h
			;;
		"cream")
			## prompt colors
			promptFG=$FG_BLACK
			promptBG=$BG_WHITE

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/ocean.dark.sh
			#linuxTtyExecutable=~/terminalsexy/Linux_Console/3024.light.sh
			xreseourcesFile=~/terminalsexy/Xresources/3024.light
			geanyConf=tango-light.conf
			geanyVteFg=#000000
			geanyVteBg=#FFFFFF
			stTheme=solarized_light

			## gtk
			if [ -d /usr/share/themes/Redmond ]; then
				gtkthemename=Redmond
			else
				gtkthemename=Raleigh
			fi
			gtkiconthemename=Adwaita++-Light

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-cream-red.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_cream.h
			;;
		"default")
			## prompt colors
			promptFG=$FG_BLACK
			promptBG=$BG_WHITE

			## tty related colors
			linuxTtyExecutable=~/terminalsexy/Linux_Console/default.sh
			xreseourcesFile=~/terminalsexy/Xresources/grayscale.dark
			geanyConf=default.conf
			geanyVteFg=#BFBFBF
			geanyVteBg=#000000
			stTheme=

			## gtk
			gtkthemename=Adwaita-dark
			gtkiconthemename=Adwaita++-Dark

			## wallpaper
			pictureFile=~/Pictures/wallpapers/dwm-wallpapers-solid/dwm-nord-red.png

			## dwm config file
			dwmConfig=~/suckless/dwm/config_default.h
			;;
		*)
			echo -e "${FG_RED}\nThe selected theme \"$inputColor\" was not set.\n\t- Use only the colors provided in prompt.\n ${STYLES_OFF}"
			sleep 2s
			exit
			;;
	esac
}

function launchXorg {
	## xinit if in tty
	if [ "$TERM" = "linux" ]; then
		startx
	else
		echo -e "$promptFG$promptBG\nDone.\nLaunch DWM to utilize configuration changes.\n ${STYLES_OFF}"
		read -p "[press any key to continue]"
		echo""
	fi

}

function main {
	decorativeColors
	echo -e "\n##########################################\n# Compile DWM and load a GTK theme \n##########################################"

	dependencyCheck
	inputPrompt

	defineConfigPaths
	applyConfigPaths
}

main

echo -e "\nDone.\n"
