#!/bin/bash

## Modified GTK Themes

sudo mkdir -p ~/.themes
sudo cp -rf .themes ~/

echo -e "\nLaunch Lxappearance and select the desired theme to activate it. \n"

## GEANY COLOR SCHEMES

sudo mkdir -p ~/.config/geany/colorschemes/
sudo cp geany_colorschemes/* ~/.config/geany/colorschemes/
echo -e "\nChange geany colors from within Geany:\n\tView --> Change_Color_Scheme\n"

## CLI COLOR SCHEMES

sudo cp -rf terminalsexy ~/
echo -e "\nCopied terminalsexy\n"

## DWM config.h files

mkdir -p ~/suckless/dwm/
cp dwm_colors/config_* ~/suckless/dwm/
echo -e "\nCopied dwm color configs\n"

if [ $(uname -r | grep Microsoft &> /dev/null; echo $?) -eq 0 ]; then
	## .bashrc on WLS or Linux
	cp .bashrc ~/.bashrc.backup.$(date +%d%b%Y_%H%M%S)
	cp .bashrc ~/
else
	## xinit on Linux
	cp .xinitrc ~/.xinitrc.backup.$(date +%d%b%Y_%H%M%S)
	cp .xinitrc ~/
fi

## color theme switcher

cp dwm_xinit_colors.sh ~/

## My Wallpapers

mkdir -p ~/Pictures/wallpapers/
sleep 1s
unzip dwm-wallpapers-solid.zip -d ~/Pictures/wallpapers/dwm-wallpapers-solid
echo -e "\nCopied wallpapers\n"

## Install more rice from github

bash github_rice.sh
echo -e "\nInstalled additional rice from github\n"
