# nordic-respin

### About:

* GTK Themes for Xorg and Geany.
* Terminal color themes for TTY and Geany.

The GTK2 and GTK3 themes are minimalized respins based off of: [Nordic-Polar](https://github.com/EliverLara/Nordic-Polar)

### Themes: Red, Blue, Solarized, Black

### Install

```sh
## Copy themes to ~/.themes
## Copy terminalsext CLI colors to ~/
## Copy colorschemes to Geany text editor
## Copy Dwm config.h color templates to ~/suckless/dwm/

./install.sh
```

### Apply theme
```sh
## Set gtk2/gtk3, tty/cli colors, and DWM config colors

./dwm_xinit_colors.sh
```

### Background Notes:

This theme pack is used with my Xorg DWM UI. [distro-themes](https://gihub.com/mezcel/distro-themes)

* Themes are designed to work on top of the configurations set by [distro-themes](https://gihub.com/mezcel/distro-themes)
* [distro-themes](https://gihub.com/mezcel/distro-themes) will automacally pull and apply themes from this repo.
* This is its own repo because I anticipate continued theme modifications in the future.
