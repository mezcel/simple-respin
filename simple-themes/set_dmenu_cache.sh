#!/bin/bash

function setDmenuCache {
	## limit and overwrite the dmenu_run list

	echo -e "\nSeting a personalized Dmenu list prompt ...\n"
	sleep 2s

	## desktop apps

	declare -a myappDesktopArr

	#myappDesktopArr=("abiword" "arandr" "chromeium" "firefox" "geany" "gimp" "glade" "gnumeric" "iceweasel" "inkscape" "leafpad" "libreoffice" "libreoffice-calc" "libreoffice-writer" "lxappearance" "midori" "mousepad" "mplayer" "mupdf" "pcmanfm" "pinta" "remarkable" "retext" "st" "surf" "termite" "thunar" "texstudio" "vim-gtk" "vlc" "xterm" "zathura")

	myappDesktopArr=("arandr" "firefox" "geany" "gimp" "libreoffice" "libreoffice-calc" "libreoffice-writer" "lxappearance" "mousepad" "mupdf" "pcmanfm" "remarkable" "st" "surf" "vim-gtk" "vlc" "xterm")

	hasXterm=0
	hasSt=0
	echo "## desktop ##" > ~/.cache/dmenu_run
	echo "## desktop ##" > ~/.dmenu_cache
	sleep 1s
	for dApp in ${myappDesktopArr[@]}; do
		command -v $dApp &>/dev/null
		if [ $? -eq 0 ]; then
			if [ $dApp == "surf" ]; then
				echo -e "surf -Bgz .75 google.com" >> ~/.cache/dmenu_run
				echo -e "surf -Bgz .75 google.com" >> ~/.dmenu_cache
			else
				echo -e "$dApp" >> ~/.cache/dmenu_run
				[[ $dApp == "xterm" ]]; hasXterm=1
				[[ $dApp == "st" ]]; hasSt=1
			fi
		fi
	done

	## prefered terminal emulator

	if [ $hasXterm -eq 1 ]; then
		desiredCli=xterm
	elif [ $hasSt -eq 1 ]; then
		desiredCli=st
	else
		exit
	fi

	## cli apps

	declare -a myappCliArr
	#myappCliArr=("alsamixer" "bmon" "elinks" "htop" "iotop" "mc" "nano" "nmcli" "nmon" "ranger" "tmux" "top" "vim" "vlc" "w3c" "wavemon" "wicd")

	myappCliArr=("alsamixer" "bmon" "elinks" "htop" "iotop" "ranger" "tmux" "top" "vim" "nvlc")

	echo -e "\n## cli apps ##" >> ~/.cache/dmenu_run
	echo -e "\n## cli apps ##" >> ~/.cache/dmenu_run
	for tApp in ${myappCliArr[@]}; do
		command -v $tApp &>/dev/null
		if [ $? -eq 0 ]; then
			echo -e "$desiredCli -e $tApp" >> ~/.cache/dmenu_run
			echo -e "$desiredCli -e $tApp" >> ~/.dmenu_cache
		fi
	done

	## home dir scripts
	echo -e "\n## scripts ##" >> ~/.cache/dmenu_run
	for f in ~/*.sh
	do
		f_sh=$(echo $f | awk -F"$(whoami)/" '{print $2}' | awk -F'.' '{print $1}')
		echo "$desiredCli -e ~/$f_sh.sh" >> ~/.cache/dmenu_run
		echo "$desiredCli -e ~/$f_sh.sh" >> ~/.dmenu_cache
	done

	echo -e "\n## misc ##\nrofi -modi drun,run -show\nxterm -e \"sudo shutdown now\"\nxterm -e \"sudo reboot\"" >> ~/.cache/dmenu_run
	echo -e "\n## misc ##\nrofi -modi drun,run -show\nxterm -e \"sudo shutdown now\"\nxterm -e \"sudo reboot\"" >> ~/.dmenu_cache
	echo ""
	sleep 2s
}

setDmenuCache
