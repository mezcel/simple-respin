#!/bin/bash

mkdir -p ~/Music/streaming_music/
cd ~/Music/streaming_music/
sleep 1s

wget https://somafm.com/groovesalad32.pls
wget https://somafm.com/u80s32.pls
wget https://somafm.com/missioncontrol32.pls
wget https://somafm.com/folkfwd32.pls
wget https://somafm.com/fluid32.pls
wget https://somafm.com/poptron32.pls
wget https://somafm.com/sf103332.pls
wget https://somafm.com/cliqhop32.pls
wget https://somafm.com/thistle32.pls
wget https://somafm.com/indiepop32.pls
wget https://somafm.com/bootliquor32.pls
wget https://somafm.com/sonicuniverse32.pls

## example:
## nvlc ~/Music/SomaFM/groovesalad32.pls


## Icecast, http://badradio.nz/
#wget http://dir.xiph.org/listen/6042/listen.m3u
