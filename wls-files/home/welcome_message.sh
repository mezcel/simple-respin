#!/bin/bash

## Openbox welcom message

function tput_color_variables {
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)

	FG_NoColor=$(tput sgr0)
	BG_NoColor=$(tput sgr0)
}

function display_string {
	text=$1
	color=$2
	FG_NoColor=$(tput sgr0)
	echo -e "${color}$text${FG_NoColor}"
}


function greetings {
	display_string "[Suckless] WLS (Debian Salsa)" $FG_YELLOW
}

function my_keybindings {
	display_string "DWM Shortct Keys:          System Utils: (cli apps)" $FG_CYAN
	display_string "* dmenu = Alt-p            * wifi connection = n/a" $FG_GREEN
	display_string "* st    = Shift-Alt-Enter  * volume control  = n/a" $FG_GREEN
	display_string "* exit  = Shift-Alt-q      * file manager    = ranger" $FG_GREEN
}

function main {
	clear
	tput_color_variables
	greetings
	my_keybindings
	echo ""
	#read -p "press any key to continue..."
}

main
