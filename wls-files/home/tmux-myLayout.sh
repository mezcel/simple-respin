#!/bin/bash

#
# tmux-myLayout.sh
#

onoff=$1

if [ -z $onoff ]; then
	tmux new-session -s "ctrl-b" -n "home"\; send-keys 'pwd' C-m \; new-window \; send-keys 'bmon' C-m \; split-window -h -p 55 \; send-keys 'wavemon' C-m \; split-window -v -p 50 \; send-keys 'nmon' C-m \; select-pane -t 0 \; new-window -n "music" \; send-keys 'nvlc ~/Music/Vapor*/Hyp* -LZ' C-m \; split-window -h -p 25 \; send-keys 'alsamixer' C-m \; select-pane -t 0 \; new-window -n "misc" \; send-keys 'pwd' C-m \; select-window -t 0 \;

else

	if [ $onoff == "off" ]; then
		#tmux kill-server
        tmux kill-session -t "ctrl-b"
	else
		echo -e "\nInput Error:"
		echo -e "\tEnter \"$(basename $0) off\" to kill tmux server\n\tOR"
		echo -e "\tJust enter \"$(basename $0)\" to launch tmux template\n"
	fi
fi
