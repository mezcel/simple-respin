"" automatically display line numbers
set number
set linebreak
set autoindent

"" set temp swap files in a custom dir
" set swapfile
set backupdir=.backup/,~/.backup/,/tmp//
set directory=.swp/,~/.swp/,/tmp//
set undodir=.undo/,~/.undo/,/tmp//

"" Indent fold presets
"set foldmethod=indent
"set foldmethod=syntax
"set foldmethod=indent
"set foldnestmax=10
set nofoldenable
"set foldlevel=2

set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

"" copy  and paste into vim terminal
set paste

"" prevent automatic backup files
" set nobackup
if v:progname =~? "evim"
	finish
endif

"" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim
if has("vms")
	set nobackup		" do not keep a backup file, use versions instead
endif

if &t_Co > 2 || has("gui_running")
	"" Switch on highlighting the last used search pattern.
	set hlsearch
endif

"" Add optional packages.
"" The matchit plugin makes the % command work better, but it is not backwards compatible.
if has('syntax') && has('eval')
	packadd matchit
endif

""" Word processing
"" activate :WP
"" goto misspelled word ]s or [s
"" list spelling options z=
func! WordProcessor()
	" movement changes
	map j gj
	map k gk
	" formatting text
	setlocal formatoptions=1
	setlocal noexpandtab
	setlocal wrap
	setlocal linebreak
	" spelling and thesaurus
	setlocal spell spelllang=en_us

	"" Download and Use a Pre-defined Thesaurus
	"" wget http://www.gutenberg.org/dirs/etext02/mthes10.zip
	"set thesaurus+=/home/test/.vim/thesaurus/mthesaur.txt
	" complete+=s makes autocompletion search the thesaurus
	"set complete+=s
endfu
com! WP call WordProcessor()

