#!/bin/bash

## my bonzai tree Github hosted apps
mkdir -p  ~/github/mezcel/
mkdir -p  ~/gist.github/mezcel/

git clone https://github.com/mezcel/electron-container.git ~/github/mezcel/electron-container
git clone https://github.com/mezcel/printf-time.git ~/github/mezcel/printf-time
git clone https://github.com/mezcel/jq-tput-terminal.git ~/github/mezcel/jq-tput-terminal
git clone https://github.com/mezcel/carousel-score.git ~/github/mezcel/carousel-score
git clone https://github.com/mezcel/python-curses.git ~/github/mezcel/python-curses
git clone https://github.com/mezcel/distro-themes.git ~/github/mezcel/distro-themes
git clone https://github.com/mezcel/catechism-scrape.git ~/github/mezcel/catechism-scrape

## hidden repos
git clone https://github.com/mezcel/scrapy-spider.git ~/github/mezcel/scrapy-spider
git clone https://github.com/mezcel/adeptus-mechanicus-stc.git ~/github/mezcel/drone-rpg

## Gists Notes

git clone https://gist.github.com/eab7764d1f9e67d051fd59ec7ce3e066.git ~/gist.github/mezcel/git-notes
git clone https://gist.github.com/64db9afd5419e557c0ee53ed935d516e.git ~/gist.github/mezcel/my-screen-gama
git clone https://gist.github.com/8ac1119e0bb94c581128184d332beee4.git ~/gist.github/mezcel/scrapy-help
git clone https://gist.github.com/c90ce696785821d1921f8c2104fb60d3.git ~/gist.github/mezcel/stations
