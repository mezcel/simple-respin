#!/bin/bash

## Vim cache
mkdir -p ~/.backup/
mkdir -p ~/.swp/
mkdir -p ~/.undo/

## Modified GTK Themes
mkdir -p ~/.themes
cp -rf simple-themes/.themes ~/

## GEANY COLOR SCHEMES
mkdir -p ~/.config/geany/colorschemes/
cp simple-themes/colorschemes/* ~/.config/geany/colorschemes/
echo -e "\nChange geany colors from within Geany:\
        \n\tView --> Change_Color_Scheme\n"

## CLI COLOR SCHEMES
cp -rf simple-themes/terminalsexy ~/
echo -e "\nCopied terminalsexy\n"

## DWM config.h files
mkdir -p ~/suckless/dwm/
cp simple-themes/dwm_color_scheme/*.h ~/suckless/dwm/
echo -e "\nCopied dwm color configs\n"

## dot files
if [ $(uname -r | grep Microsoft &> /dev/null; echo $?) -eq 0 ]; then
    ## .bashrc on WLS or Linux
    #bash wls-files/suckless_debian_wls.sh
    if [ -f .xinitrc ]; then cp .bashrc ~/.bashrc.backup.$(date +%d%b%Y_%H%M%S); fi
    cp simple-themes/.bashrc ~/
else
    ## xinit on Linux
    if [ -f .xinitrc ]; then cp .xinitrc ~/.xinitrc.backup.$(date +%d%b%Y_%H%M%S); fi
    cp simple-themes/.xinitrc ~/
fi

## color theme switcher
cp simple-themes/dwm_appearance.sh ~/

## My Wallpapers
mkdir -p ~/Pictures/wallpapers/
sleep 1s
unzip simple-themes/dwm-wallpapers-solid.zip -d ~/Pictures/wallpapers/dwm-wallpapers-solid
echo -e "\nCopied wallpapers\n"

## a generiic arc gtk theme
if [ -d /etc/pacman.d ]; then sudo pacman -S --noconfirm --needed arc-solid-gtk-theme; fi

## Install more rice from github
echo ""
read -e -p "Install even more themes posted by others on github? [y/n]: " -i "n" yn
if [ $yn == "y" ]; then
    cd simple-themes/
    bash github_rice.sh
    echo -e "\nInstalled additional rice from github\n"
fi
