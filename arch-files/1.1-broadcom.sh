#!/bin/sh
## This script assumes you have https://github.com/mezcel/arch-openbox.git in your home dir

#
## Broadcom Driver Library (Optional)
## B43 - Broadcom
## Most of my computers dont need this, but one of them does
## this script will add /lib/firmware/b43
#

#cd $(dirname $0)
thisDirPath=$(pwd)

FG_RED=$(tput setaf 1)
FG_GREEN=$(tput setaf 2)
FG_YELLOW=$(tput setaf 3)
FG_NoColor=$(tput sgr0)

function broadcom_firmware() {

	## display wifi driver
	echo "Available Wireless Network Drivers:"
	lspci -nn | grep -i network
	echo ""
	read -p "[press any key to continue]"

	## Copy driver scripts

	if [ ! -d /lib/firmware/b43 ]; then
		## Broadcom BCM4332
		## cp -pR <your saved firmware dir> /lib/firmware/
		sudo cp -rf $thisDirPath/extra-drivers/b43 /lib/firmware/
		echo "one moment ..."
		sleep 2s
		echo "done."


		echo -e "BCM43**\n\n$(ls -l /lib/firmware/b43*)"
		if [ -d /lib/firmware/b43 ]; then
			echo "$FG_GREEN"
			echo -e "\nDir /lib/firmware/b43 now exists. \n"
		else
			echo "$FG_RED"
			echo -e "\nDir /lib/firmware/b43 does not exists.\nFigure out why. \n"
		fi
		echo "$FG_NoColor"

		read -e -p "Do you want to reboot now? [y/N] : " -i "y" yn

		if [ $yn == 'y' ]; then
			sudo reboot
		else
			echo "$FG_YELLOW"
			echo -e "\nSystem did not reboot.\nRebooting helps to ensure your systems is initialized with access to Broadcom functionalities.\n"
			echo "$FG_NoColor"
		fi

	fi
}

broadcom_firmware
