#!/bin/bash
# https://wiki.archlinux.org/index.php/archiso#Setup
# run this script in sudo

## Everything about this script should be in Sudo

thisDirPath=$(dirname $0)

FG_RED=$(tput setaf 1)
FG_GREEN=$(tput setaf 2)
FG_YELLOW=$(tput setaf 3)
FG_CYAN=$(tput setaf 6)
FG_NoColor=$(tput sgr0)

if [ -f /var/lib/pacman/db.lck ]; then
	sudo rm /var/lib/pacman/db.lck
fi

function makeBackupCopy {
	filePath=$1
	if [ -f $filePath ]; then
		timeStamp=$(date +%F_%H:%m:%s)
		sudo cp $filePath $filePath.$timeStamp
	fi
}

function copyDir2skel {
	filePath=$1
	destination=~/archlive/airootfs/etc/skel/
	if [ -d $filePath ]; then
		sudo cp -rf $filePath $destination
		sleep 5s
	fi
}

function copyFile2skel {
	filePath=$1
	destination=~/archlive/airootfs/etc/skel/
	if [ -f $filePath ]; then
		sudo cp $filePath $destination
		sleep 1s
	fi
}

function makeMyDistro {
	sudo pacman -Sy --needed --noconfirm archiso
	sleep 5s

	## choose: releng or baseline
	##	releng is your personalized arch
	##	baseline is just arch

	sudo cp -rf /usr/share/archiso/configs/releng/ ~/archlive
	sleep 5s

	## list of all packages on my system
	# sudo pacman -Qqe > ~/archlive/packages.x86_64
	mkdir -p ~/Downloads/
	sudo pacman -Qqe > ~/Downloads/packages.x86_64
	sleep 1s
	sudo cp ~/Downloads/packages.x86_64 ~/archlive/packages.x86_64
	sleep 1s

	## prepare files for usb os lfs

	#sudo mkdir -p ~/archlive/airootfs/usr/local/bin/
	#sudo mkdir -p ~/archlive/airootfs/usr/share/
	#sudo mkdir -p ~/archlive/airootfs/etc/
	sudo mkdir -p ~/archlive/airootfs/etc/skel/
	#sudo mkdir -p ~/archlive/airootfs/etc/systemd/system/
	#sudo mkdir -p ~/archlive/airootfs/etc/systemd/system/getty@.service.d/
	#sudo mkdir -p ~/archlive/airootfs/usr/share/xfce4/terminal/colorschemes/
	#sudo mkdir -p ~/archlive/airootfs/lib/firmware/

	sleep 2s

	## Copy Dirs
	copyDir2skel ~/.config
	copyDir2skel ~/.icons
	copyDir2skel ~/.themes
	copyDir2skel ~/Pictures

	git clone https://github.com/mezcel/simple-respin.git ~/github/mezcel/simple-respin

	copyDir2skel ~/github
	copyDir2skel ~/terminalsexy
	copyDir2skel ~/suckless
	#copyDir2skel ~/AUR
	#copyDir2skel ~/.screenlayout

	#sudo cp -rf /usr/share/consolefonts ~/archlive/airootfs/usr/share/
	#sleep 2s
	#sudo cp -rf /lib/firmware/b43 ~/archlive/airootfs/lib/firmware/
	#sleep 2s

	## Cpoy files
	copyFile2skel ~/.xinitrc
	copyFile2skel ~/.vimrc
	copyFile2skel ~/.bashrc
	#copyFile2skel ~/.bash_profile
	copyFile2skel ~/.nanorc
	copyFile2skel ~/.tmux.conf
	#copyFile2skel ~/.screenrc
	copyFile2skel ~/.zshrc
	copyFile2skel ~/.Xresources
	copyFile2skel ~/.gtkrc-2.0
	copyFile2skel ~/.vimrc
	copyFile2skel ~/.fehbg
	copyFile2skel ~/dwm_appearance.sh

	#sudo cp /usr/local/bin/console-solarized ~/archlive/airootfs/usr/local/bin/
	#sudo cp /etc/console-solarized.conf ~/archlive/airootfs/etc/
	#sudo cp /etc/systemd/system/console-solarized@.service ~/archlive/airootfs/etc/systemd/system/
	#sudo cp /etc/systemd/system/getty@.service.d/solarized.conf ~/archlive/airootfs/etc/systemd/system/getty@.service.d/
	#sudo cp -r /usr/share/xfce4/terminal/colorschemes/*.theme ~/archlive/airootfs/usr/share/xfce4/terminal/colorschemes/
}

ping -c 3 google.com
isOnline=$?
if [ $isOnline -eq 0 ]; then
	makeMyDistro

	echo "$FG_GREEN"
	echo "#####################"
	echo "# Your Live Skell Dir"
	echo "#####################"
	echo '~/archlive/airootfs/etc/skel/'
	echo "$FG_YELLOW"

	ls -al ~/archlive/airootfs/etc/skel/

	echo "$FG_CYAN
	## build when ~/archlive/airootfs/etc/skel/ is the way you want it
	## add desired firmware dir files to ~/archlive/airootfs/lib/firmware/
	#
	# cd ~/archlive
	# sudo ./build.sh -v
	$FG_NoColor"
else
	echo "$FG_RED"
	echo "Must be online to use this script, try again."
fi
