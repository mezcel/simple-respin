#!/bin/bash

## Networking and dev utils

thisDirPath=$(pwd)

function skel_update() {
	## copy arch install/configs for all new users

	cd ~/
	sleep 2s
	sudo cp -rf . /etc/skel/
	sleep 2s
}

function define_arch_users() {
	## Configure Users
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_NoColor=$(tput sgr0)

	## Root
	while true; do
		echo "$FG_GREEN"
		read -e -p "Define a new SUDO Password? (y/n): " -i "y" yn
		case $yn in
			[Yy]* )
				read -p "Enter a Sudo ROOT password: " rootpassword
				(echo $rootpassword; echo $rootpassword;) | passwd
				echo ""
				break
				;;
			[Nn]* )
				exit;;
			* )
				echo "$FG_RED Please answer yes or no. $FG_NoColor"
				;;
		esac
	done

	## User
	while true; do
		echo "$FG_GREEN"
		read -e -p "Define a new Arch USER? (y/n): " -i "y" yn
		case $yn in
			[Yy]* )
				read -p "Enter a user name: " myusername
				read -p "Enter a password for $myusername: " userpassword
				useradd -m -g users -s /bin/bash "$myusername"
				(echo $userpassword; echo $userpassword;) | passwd "$myusername"
				sudo echo -e "\n$myusername ALL=(ALL) ALL" >> /etc/sudoers
				break
				;;
			[Nn]* )
				exit
				;;
			* ) echo
				echo "$FG_RED Please answer \"y\" or \"n\". $FG_NoColor"
				;;
		esac
	done
}

function backup_server_scripts() {
    ## if a server script exists, then back it up

    hosts_backup="/etc/hosts"

    if [ -f "$hosts_backup" ]
    then mv "$hosts_backup" "$hosts_backup.backup.$(date +%d%b%Y_%H%M%S)"
        # rm /etc/hosts
    fi

    hostname_backup="/etc/hostname"

    if [ -f "$hostname_backup" ]
    then mv "$hostname_backup" "$hostname_backup.backup.$(date +%d%b%Y_%H%M%S)"
    fi

    sshd_config_backup="/etc/ssh/sshd_config"

    if [ -f "$sshd_config_backup" ]
    then mv "$sshd_config_backup" "$sshd_config_backup.backup.$(date +%d%b%Y_%H%M%S)"
    fi
}

function customize_host() {
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	BG_NoColor=$(tput sgr0)
	FG_CYAN=$(tput setaf 6)

    echo "$FG_GREEN"
    echo "Customize Host:"

    read -p "Enter a a Name for your Host Computer (archlinux) : " -i "archlinux" myHostName

    sudo echo "$myHostName" >> /etc/hostname
    sudo echo "127.0.0.1	localhost" >> /etc/hosts
    sudo echo "::1		localhost" >> /etc/hosts
    sudo echo "127.0.1.1	$myHostName.localdomain $myHostName" >> /etc/hosts

    echo "$FG_CYAN"
    echo "Customize SSH Configuration:"
    echo ""
    echo "Customize /etc/ssh/sshd_config or use defaults?"
    echo '	"y" to manually customize'
    echo '	"n" to use preset defaults'
    echo ""
    read -e -p "Edit the ssh config file, sshd_config? [y/N] : " -i "n" yn

    case $yn in
		[Yy]* )
			read -e -p "Port (22) : " -i "22" myPort
			read -e -p "ListenAddress (10.42.0.1) : " -i "10.42.0.1" myListenAddress
			read -e -p "PermitRootLogin (no) : " -i "no" myPermitRootLogin
			read -e -p "AllowUsers (mezcel) : " -i "mezcel" myAllowUsers
			read -e -p "AuthorizedKeysFile (.ssh/authorized_keys) : " -i ".ssh/authorized_keys" myAuthorizedKeysFile
			read -e -p "ChallengeResponseAuthentication (no) : " -i "no" myChallengeResponseAuthentication
			read -e -p "UsePAM (yes) : " -i "yes" myUsePAM
			read -e -p "PrintMotd (no) : " -i "no" myPrintMotd
			read -e -p "Subsystem (sftp	/usr/lib/ssh/sftp-server) : " -i "sftp	/usr/lib/ssh/sftp-server" mySubsystem
			read -e -p "X11Forwarding (yes) : " -i "yes" myX11Forwarding
			read -e -p "X11UseLocalhost (yes) : " -i "yes" myX11UseLocalhost

			sudo echo "Port  $myPort" >> /etc/ssh/sshd_config
			sudo echo "ListenAddress  $myListenAddress" >> /etc/ssh/sshd_config
			sudo echo "PermitRootLogin   $myPermitRootLogin" >> /etc/ssh/sshd_config
			sudo echo "AllowUsers   $myAllowUsers" >> /etc/ssh/sshd_config
			sudo echo "AuthorizedKeysFile    $myAuthorizedKeysFile" >> /etc/ssh/sshd_config
			sudo echo "ChallengeResponseAuthentication  $myChallengeResponseAuthentication" >> /etc/ssh/sshd_config
			sudo echo "UsePAM  $myUsePAM" >> /etc/ssh/sshd_config
			sudo echo "PrintMotd  $myPrintMotd" >> /etc/ssh/sshd_config
			sudo echo "Subsystem  $mySubsystem" >> /etc/ssh/sshd_config
			sudo echo "X11Forwarding  $myX11Forwarding" >> /etc/ssh/sshd_config
			sudo echo "X11UseLocalhost  $myX11UseLocalhost" >> /etc/ssh/sshd_config
			;;

	esac

    echo "$BG_NoColor
    Done
    "
}

function optimize_mirrorlist() {
	#thisDirPath=$(dirname $0)
    ## Pacman Mirrorlist Generator
    ## https://www.archlinux.org/mirrorlist/

    FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	BG_NoColor=$(tput sgr0)

    mirrorlist_backup="/etc/pacman.d/mirrorlist"

    if [ ! -f "$mirrorlist_backup" ]; then
		sudo cp "$mirrorlist_backup" "$mirrorlist_backup.backup.$(date +%d%b%Y_%H%M%S)"
    fi

	## assumes my mirrorlist is in the same dir
	if [ -f $thisDirPath/mirrorlist ]; then
		sudo cp $thisDirPath/mirrorlist $mirrorlist_backup
    fi

    echo "$BG_NoColor"
}

if [ ! -f /var/lib/pacman/db.lck ]; then
	sudo rm /var/lib/pacman/db.lck
	sleep 1s
fi

backup_server_scripts
customize_host
optimize_mirrorlist

skel_update
define_arch_users
