#!/bin/bash

#
# Install base app for my developer Linux box
#

mygpu=$1
[[ -z $mygpu ]]; mygpu="na"

## Unlock pacman if it was locked
if [ -f /var/lib/pacman/db.lck ]; then
	sudo rm /var/lib/pacman/db.lck
fi

starttime=$(date +%s)

## Server UI and Dev Tools
sudo pacman -Sy
sudo pacman -S --needed base
sudo pacman -S --needed base-devel
#sudo pacman -S --needed qt5
sudo pacman -S --needed --noconfirm ruby
sudo pacman -S --needed --noconfirm vim
sudo pacman -S --needed --noconfirm git
sudo pacman -S --needed --noconfirm tmux
sudo pacman -S --needed --noconfirm ranger
sudo pacman -S --needed --noconfirm mc
sudo pacman -S --needed --noconfirm tree
sudo pacman -S --needed --noconfirm acpi
sudo pacman -S --needed --noconfirm upower
sudo pacman -S --needed --noconfirm htop
sudo pacman -S --needed --noconfirm xclip
sudo pacman -S --needed --noconfirm xorg-xclipboard

sudo pacman -S --needed --noconfirm p7zip
sudo pacman -S --needed --noconfirm zip
sudo pacman -S --needed --noconfirm unzip
sudo pacman -S --needed --noconfirm unrar
sudo pacman -S --needed --noconfirm xclip
sudo pacman -S --needed --noconfirm xsel

sudo pacman -S --needed --noconfirm hunspell
sudo pacman -S --needed --noconfirm hunspell-en_US
sudo pacman -S --needed --noconfirm aspell
sudo pacman -S --needed --noconfirm libmythes
sudo pacman -S --needed --noconfirm mythes-en
sudo pacman -S --needed --noconfirm enchant
sudo pacman -S --needed --noconfirm languagetool

sudo pacman -S --needed --noconfirm openssh
sudo pacman -S --needed --noconfirm macchanger
sudo pacman -S --needed --noconfirm hostapd
sudo pacman -S --needed --noconfirm iptables
sudo pacman -S --needed --noconfirm nftables
sudo pacman -S --needed --noconfirm create_ap
sudo pacman -S --noconfirm --needed bmon
sudo pacman -S --noconfirm --needed wavemon
sudo pacman -S --needed --noconfirm elinks
sudo pacman -S --needed --noconfirm w3m
#sudo pacman -S --needed --noconfirm wicd

## Desktop Environment

cpuvendor=$(cat /proc/cpuinfo | grep vendor | uniq | awk '{printf $3}')
[[ -z $cpuvendor ]]; cpuvendor="unknown"

if [ $mygpu -eq "all" ]; then
    sudo pacman -S --needed --noconfirm xf86-video-intel
	sudo pacman -S --needed --noconfirm xf86-video-amdgpu
	sudo pacman -S --needed --noconfirm xf86-video-ati
    sudo pacman -S --needed --noconfirm xf86-video-fbdev
    sudo pacman -S --needed --noconfirm xf86-video-vesa

    sudo pacman -S --needed --noconfirm nvidia
    sudo pacman -S --needed --noconfirm nvidia-utils
    sudo pacman -S --needed --noconfirm lib32-nvidia-utils
    sudo pacman -S --needed --noconfirm nvidia-390xx
    sudo pacman -S --needed --noconfirm nvidia-390xx-utils
    sudo pacman -S --needed --noconfirm lib32-nvidia-390xx-utils
elif [ $cpuvendor -eq "GenuineIntel" ]; then
    sudo pacman -S --needed --noconfirm xf86-video-intel
elif [ $cpuvendor -eq "AuthenticAMD" ]; then
	sudo pacman -S --needed --noconfirm xf86-video-amdgpu
	sudo pacman -S --needed --noconfirm xf86-video-ati
else
    ## universal generic
    sudo pacman -S --needed --noconfirm xf86-video-fbdev
    sudo pacman -S --needed --noconfirm xf86-video-vesa
fi

sudo pacman -S --needed --noconfirm mesa
sudo pacman -S --needed --noconfirm lib32-mesa

sudo pacman -S --needed --noconfirm xorg
sudo pacman -S --needed --noconfirm xorg-xinit
sudo pacman -S --needed --noconfirm xrandr
sudo pacman -S --needed --noconfirm arandr
sudo pacman -S --needed --noconfirm openbox
sudo pacman -S --needed --noconfirm obconf
sudo pacman -S --needed --noconfirm lxappearance
sudo pacman -S --needed --noconfirm lxappearance-obconf
sudo pacman -S --needed --noconfirm arc-solid-gtk-theme
sudo pacman -S --needed --noconfirm lxinput
sudo pacman -S --needed --noconfirm lxrandr
sudo pacman -S --needed --noconfirm xterm
sudo pacman -S --needed --noconfirm feh

sudo pacman -S --needed --noconfirm pcmanfm
sudo pacman -S --needed --noconfirm gimp
sudo pacman -S --needed --noconfirm gvfs
sudo pacman -S --needed --noconfirm tumbler
sudo pacman -S --needed --noconfirm xarchiver
sudo pacman -S --needed --noconfirm evince
sudo pacman -S --needed --noconfirm udiskie
sudo pacman -S --needed --noconfirm ffmpegthumbnailer
sudo pacman -S --needed --noconfirm poppler-glib
sudo pacman -S --needed --noconfirm libgsf
sudo pacman -S --needed --noconfirm libopenraw
sudo pacman -S --needed --noconfirm freetype2

sudo pacman -S --needed --noconfirm thunar
sudo pacman -S --needed --noconfirm thunar-volman
sudo pacman -S --needed --noconfirm thunar-archive-plugin
sudo pacman -S --needed --noconfirm thunar-media-tags-plugin

sudo pacman -S --needed --noconfirm alsa
sudo pacman -S --needed --noconfirm alsa-utils
sudo pacman -S --needed --noconfirm alsa-firmware
sudo pacman -S --needed --noconfirm alsa-plugins
sudo pacman -S --needed --noconfirm pulseaudio
sudo pacman -S --needed --noconfirm pulseaudio-alsa
sudo pacman -S --needed --noconfirm pavucontrol
sudo pacman -S --needed --noconfirm vorbis-tools
sudo pacman -S --needed --noconfirm mplayer
sudo pacman -S --needed --noconfirm vlc

sudo pacman -S --needed --noconfirm firefox

sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts
sudo pacman -S --needed --noconfirm adobe-source-sans-pro-fonts
sudo pacman -S --needed --noconfirm ttf-ubuntu-font-family
sudo pacman -S --needed --noconfirm ttf-inconsolata

sudo pacman -S --needed --noconfirm libreoffice-still
sudo pacman -S --needed --noconfirm libreoffice-extension-texmaths
sudo pacman -S --needed --noconfirm libreoffice-extension-writer2latex

sudo pacman -S --needed --noconfirm geany
sudo pacman -S --needed --noconfirm geany-plugins
sudo pacman -S --needed --noconfirm mousepad

sudo pacman -S --needed --noconfirm texlive-most
sudo pacman -S --needed --noconfirm biber
sudo pacman -S --needed --noconfirm texstudio
sudo pacman -S --needed --noconfirm pandoc
sudo pacman -S --needed --noconfirm mupdf

sudo pacman -S --noconfirm --neeed bluez
sudo pacman -S --noconfirm --neeed bluez-utils
sudo pacman -S --noconfirm --neeed zenity
sudo pacman -S --noconfirm --neeed gtk3
sudo pacman -S --noconfirm --neeed glade

sudo pacman -S --noconfirm --needed firejail

## Dotfiles

#cd $(dirname $0)
#thisDir=$(pwd)

## Home Directory dot files

cp -rf ./home/.config ~/

[[ -f ~/.bashrc ]]; cp ~/.bashrc ~/.bashrc.backup.$(date +%d%b%Y_%H%M%S)
cp ./home/.bashrc ~/

[[ -f ~/.tmux.conf ]]; cp ~/.tmux.conf ~/.tmux.conf.backup.$(date +%d%b%Y_%H%M%S)
cp ./home/.tmux.conf ~/

[[ -f ~/.vimrc ]]; cp ~/.vimrc ~/.vimrc.backup.$(date +%d%b%Y_%H%M%S)
cp ./home/.vimrc ~/

[[ -f ~/.xinitrc ]]; cp ~/.xinitrc ~/.xinitrc.backup.$(date +%d%b%Y_%H%M%S)
cp ./home/.xinitrc ~/

[[ -f ~/.Xresources ]]; cp ~/.Xresources ~/.Xresources.backup.$(date +%d%b%Y_%H%M%S)
cp ./home/.Xresources ~/

[[ -f ~/welcome_message.sh ]]; cp ~/welcome_message.sh ~/welcome_message.backup.$(date +%d%b%Y_%H%M%S).sh
cp ./home/welcome_message.sh ~/

ranger --copy-config=all

#mkdir -p ~/Pictures/Wallpapers/
#wget -O ~/Pictures/Wallpapers/dwm.png "https://www.deviantart.com/mezcel/art/Dwm-wallpapers-solid-817533656"

## Github

function github_packages {
	mkdir -p ~/github/
	sleep 2s
	## powerline font
	git clone https://github.com/powerline/fonts.git ~/github/powerline/fonts
	sudo mkdir -p /usr/share/consolefonts/
	sleep 2s
	sudo cp -rf ~/github/powerline/fonts/Terminus/PSF/*.psf.gz /usr/share/consolefonts/
	sleep 2s

	## tty console
	## solarized
	git clone https://github.com/adeverteuil/console-solarized.git ~/github/adeverteuil/console-solarized
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized /usr/local/bin/
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized.conf /etc/
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized@.service /etc/systemd/system/
	sudo mkdir -p /etc/systemd/system/getty@.service.d/
	sleep 2s
	sudo cp ~/github/adeverteuil/console-solarized/solarized.conf /etc/systemd/system/getty@.service.d/
	sleep 2s
	sudo systemctl daemon-reload

	## nord tty
	git clone https://github.com/lewisacidic/nord-tty.git ~/github/lewisacidic/nord-tty

	## Nord terminal colors
	git clone https://github.com/khamer/base16-termite.git ~/github/khamer/base16-termite

	## A decorative Grub theme
	git clone https://github.com/shvchk/poly-dark.git ~/github/shvchk/poly-dark
	sleep 1s
	cd ~/github/shvchk/poly-dark/
	sleep 1s
	chmod +x install.sh
	sleep 1s
	sudo ./install.sh
	sleep 1s

	echo -e "\nFinished adding themes from Github.\n"
	sleep 2s
}

function setup_git {
	echo "${FG_CYAN}"
	echo -e "Enter your Github user profile.\n\tYou will need this in order to commit changes to a repo."
	echo "${FG_GREEN}# git config --global user.name${FG_CYAN}"
	read -p "github user name: " githubusername
	echo ""
	echo "${FG_GREEN}# git config --global user.email${FG_CYAN}"
	read -p "github user email: " githubuseremail

	echo -e "---"
	git config --global user.name $githubusername
	git config --global user.email $githubuseremail
	#git config --global  core.editor "nano"

	echo -e "\nDone.\n\t# git config --global user.name $githubusername\n\t# git config --global user.email $githubuseremail\n"
	sleep 2s
}

github_packages
setup_git

## AURS

function dlAurs {
	aurPackage=$1
	git clone https://aur.archlinux.org/$aurPackage.git ~/AUR/$aurPackage
	sleep 1s
}

function dl_utils_aur {

	# aurPackages=( "python-gtkspellcheck" "remarkable" "inxi" "dia2code" "xrdp" "newaita-icons-git" )
	aurPackages=( "python-gtkspellcheck" "remarkable" )

	for i in "${aurPackages[@]}"
	do
		aurPackage=$i
		dlAurs $aurPackage
		sleep 5s
	done
	sleep 1s
}

dl_utils_aur

## Suckless
echo -e "\n## Suckless\n"
sudo pacman -S --needed --noconfirm dmenu

if [ -d ../patches ]; then
	mkdir -p ~/suckless
	sleep 1s
	echo -e "\n10s delay to copy the ../patches dir.\n"
	cd ../
	cp -rf patches ~/suckless/
	isCopied=0
	isDwm=0
	isSt=0
	isSlstatus=0
	sucklessCounter=0
	while [ $isCopied -eq 0 ]
	do
		[[ -d ~/suckless/patches/dwm ]]; isDwm=1
		[[ -d ~/suckless/patches/st ]]; isSt=1
		[[ -d ~/suckless/patches/slstatus ]]; isSlstatus=1

		if [ isDwm -eq 1 ] && [ isSt -eq 1 ] && [ isSlstatus -eq 1 ] && [ $sucklessCounter -ge 20 ]; then
			isCopied=1
		fi
		sucklessCounter=$((${sucklessCounter} + 1))
		sleep 1s
	done

	if [ -d ~/suckless/patches ]; then
		cd ~/suckless/patches/st
		./auto_st_patch.sh

		cd ~/suckless/patches/dwm
		./auto_dwm_patch.sh

		cd ~/suckless/patches/slstatus
		./auto_slstatus_patch.sh
	else
		echo -e "\n The dir ~/suckless/patches does was not found.\nManually copy it after this script is concluded.\n\tcp -rf ../patches ~/suckless/ \n"
		echo -e "Attempting to open the ranger file manger where you can manuall move files as necessary.\nOtherwise remember you need to go back and do the do.\n"
		echo -e "\tcp -rf ../patches ~/suckless/\n"
		read -p "[press any key to continue]"
		command -v ranger &>/dev/null
		if [ $? -eq 0 ]; then
			ranger
		else
			echo "ranger is not installed."
			sleep 5s
		fi
	fi
fi

## Extra Developer Tools

#sudo pacman -S nodejs
#sudo pacman -S electron
#sudo pacman -S eclipse-cpp

## Games
echo -e "\n## Games\n"
sleep 5s
sudo pacman -S wesnoth
sudo pacman -S dwarffortress
# git clone https://aur.archlinux.org/tome4.git ~/AUR/tome4

## Mirrors

sudo pacman -S --needed --noconfirm reflector

## optimize remote package list
command -v reflector
if [ $? -eq 0 ]; then
	echo -e "\nOrganizing mirrorlist ...\n"
	sleep 4s
	sudo reflector --country 'United States' --sort rate --save /etc/pacman.d/mirrorlist
fi

## clean out unused packages
sudo pacman -Sc

endtime=$(date +%s)

echo -e "\nDone.\nInstaller script has ended.\n\nStart time: $starttime, End time: $endtime, Elapsed time: $((${endtime} - ${starttime}))"
