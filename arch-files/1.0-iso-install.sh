#!/bin/sh
## This script assumes you have https://github.com/mezcel/arch-openbox.git in your home dir

## Arch Linux Boot from Iso
## Really fast install via Usb, but about 30min via slow internet

thisDirPath=$(dirname $0)

## terminal beautification
function decorativeColors() {

	## Forground Color using ANSI escape

	FG_BLACK=$(tput setaf 0)
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)
	FG_WHITE=$(tput setaf 7)
	FG_NoColor=$(tput sgr0)

	## Background Color using ANSI escape

	BG_BLACK=$(tput setab 0)
	BG_RED=$(tput setab 1)
	BG_GREEN=$(tput setab 2)
	BG_YELLOW=$(tput setab 3)
	BG_BLUE=$(tput setab 4)
	BG_MAGENTA=$(tput setab 5)
	BG_CYAN=$(tput setab 6)
	BG_WHITE=$(tput setab 7)
	BG_NoColor=$(tput sgr0)

	## set mode using ANSI escape

	MODE_BOLD=$(tput bold)
	MODE_DIM=$(tput dim)
	MODE_BEGIN_UNDERLINE=$(tput smul)
	MODE_EXIT_UNDERLINE=$(tput rmul)
	MODE_REVERSE=$(tput rev)
	MODE_ENTER_STANDOUT=$(tput smso)
	MODE_EXIT_STANDOUT=$(tput rmso)

	# clear styles using ANSI escape

	STYLES_OFF=$(tput sgr0)
}

function promptSetPartitionSize {
	partitionPrompt=1
	while [ $partitionPrompt -eq 1 ]
	do
		clear
		echo ""
		echo "##########################################"
		echo "How large do you want your Arch partition?"
		echo "##########################################"
		echo 'Example: 20'
		echo '  1G to 20G is the minimum recommended size'
		echo "Note:"
		echo "  a blank entry defaults to Entire Disk"
		echo "  q = quit"
		echo ""
		echo "Your hard disk pre-erased allocation:"
		lsblk
		echo ""
		read -p "Enter the Arch partition size: " archPartition
		echo ""

		re='^[0-9]+$'
		if [[ $archPartition =~ $re ]]; then
			## is a number
			if [ ${#archPartition} -gt 1 ]; then
				archPartition=+${archPartition}G
				partitionPrompt=0
			else
				archPartition="try again: too small, make it at least 10"
				echo $archPartition
				echo ""
				sleep 3s
			fi
		else
			## not a number
			if [ "$archPartition" == 'q' ]; then
				archPartition=""
				partitionPrompt=0
				echo ""
				echo "Quiting intallation"
				sleep 5s
				exit
			elif [ -z "$archPartition" ]; then
				archPartition=""
				partitionPrompt=0
			else
				archPartition="try again: enter an integer or leave blank"

				echo $archPartition
				echo ""
				sleep 3s
			fi
		fi
	done
}

function format_HDD() {
	promptSetPartitionSize

    # del existing partitions
    (echo d; echo 1; echo ""; echo w;) | fdisk /dev/sda
	sleep 4s
    (echo d; echo 2; echo ""; echo w;) | fdisk /dev/sda
	sleep 4s
    (echo d; echo 3; echo ""; echo w;) | fdisk /dev/sda
	sleep 4s
    (echo d; echo 4; echo ""; echo w;) | fdisk /dev/sda
	sleep 4s

    # make new partitions
    (echo o; echo n; echo p; echo 2; echo ""; echo +2G; echo w; echo "";) | fdisk /dev/sda
	sleep 4s
    (echo n; echo p; echo 1; echo ""; echo "$archPartition"; echo w; echo "";) | fdisk /dev/sda
	sleep 4s
    (echo y;) | mkfs.ext4 /dev/sda1
	sleep 4s
    mkswap /dev/sda2
	sleep 4s
    swapon /dev/sda2
	sleep 4s
    # Mount
    # mkdir -p /mnt
    mount /dev/sda1 /mnt
	sleep 4s
}

function format_SSD_HDD() {
	## del any existing partitions
	(echo d; echo 1; echo ""; echo w;) | fdisk /dev/mmcblk0
	sleep 1s
	(echo d; echo 2; echo ""; echo w;) | fdisk /dev/mmcblk0
	sleep 1s
	(echo d; echo 3; echo ""; echo w;) | fdisk /dev/mmcblk0
	sleep 1s
	(echo d; echo 4; echo ""; echo w;) | fdisk /dev/mmcblk0
	sleep 1s
	(echo d; echo ""; echo w;) | fdisk /dev/mmcblk0
	sleep 1s

	## make new partitions
	## +256M UEFI Boot partition is a guess, Win10 likes 4K-25M, yet 256M-500M is recomended by the ArchWiki, Ubuntu doesent even care
	(echo o; echo n; echo p; echo 1; echo ""; echo +256M; echo w; echo "";) | fdisk /dev/mmcblk0
	sleep 1s
	## I put in swap just as a curtesy
	(echo n; echo p; echo 3; echo ""; echo +4G; echo w; echo "";) | fdisk /dev/mmcblk0
	sleep 1s
	(echo n; echo p; echo 2; echo ""; echo ""; echo w; echo "";) | fdisk /dev/mmcblk0
	sleep 1s

	## Format for uefi boot system
	(echo y;) | mkfs.fat -F32 /dev/mmcblk0p1
	sleep 1s
	(echo y;) | mkfs.ext4 /dev/mmcblk0p2
	sleep 1s

	## extra swap just to have it
	mkswap /dev/mmcblk0p3
	sleep 1s
	swapon /dev/mmcblk0p3
	sleep 1s

	## Mount
	mount /dev/mmcblk0p2 /mnt
	sleep 1s
}

function format_SSD_HDD_DUAL() {
    isDoLoop=1
    while [ $isDoLoop -eq 1 ]
    do
        echo "## lsblk"
        echo "#########"
        lsblk
        echo ""
        echo "Which partition do you want to store Arch into ?"
        echo "Example: mmcblk0p3 or sda6"
        echo ""
        read -p "Enter partition name: " partitionName
        echo "You selected $partitionName."
        read -p "Confirm ? [y/n] " yn1

        if [ $yn1 == 'y' ]; then
            (echo y;) | mkfs.ext4 /dev/$partitionName
            sleep 5s
	        mount /dev/$partitionName /mnt
            sleep 1s
            isDoLoop=0
            sleep 1s
        else
            read -p "Try again ? [y/n] " yn2
             if [ $yn1 == 'y' ]; then
                isDoLoop=1
                sleep 1s
             else
                isDoLoop=0
                echo ""
                echo "Aborting dual boot installation..."
                sleep 5s
                exit
             fi
        fi
    done
}

function packstrap_from_iso() {
	thisDirPath=$(dirname $0)
    ## my regional mirrorlist for pacman

	mirrorlist_backup="/etc/pacman.d/mirrorlist"
    if [ -f "$mirrorlist_backup" ]
    then mv "$mirrorlist_backup" "$mirrorlist_backup.backup.$(date +%d%b%Y_%H%M%S)"
    fi
	sleep 1s

	sudo cp $thisDirPath/mirrorlist /etc/pacman.d/mirrorlist
	sleep 1s

	## Customize my Skel
    ## copy my current home to skel
    # cp -r ~/* /etc/skel
	# cp -r ~/.* /etc/skel

    ## Usb merge resource from usb
    time cp -ax / /mnt
	sleep 4s
    cp -vaT /run/archiso/bootmnt/arch/boot/x86_64/vmlinuz /mnt/boot/vmlinuz-linux
	sleep 2s
    # pacman -Sy
    ## Install the base packages
    # pacstrap /mnt base base-devel linux linux-firmware
    ## Generate an fstab
    genfstab -p /mnt >> /mnt/etc/fstab
	sleep 4s
}

function chroot_localization() {
	# Chroot into your newly installed system
	# remove a lot of things that were imported from usb iso
	(
		# Localization
		echo timedatectl set-ntp true;
		echo ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime;
		echo "echo en_US.UTF-8 UTF-8 >> /etc/locale.gen";
		echo locale-gen;
		echo hwclock --systohc;
		echo "echo LANG=en_US.UTF-8 >> /etc/locale.conf";
		# Restore the configuration of journald
		echo sed -i 's/Storage=volatile/#Storage=auto/' /etc/systemd/journald.conf;
		# Remove special udev rule
		echo rm /etc/udev/rules.d//81-dhcpcd.rules;
		# Disable and remove the services created by archiso
		echo systemctl disable pacman-init.service choose-mirror.service;
		echo rm /etc/systemd/system/choose-mirror.service;
		echo rm /etc/systemd/system/pacman-init.service;
		echo rm /etc/systemd/system/etc-pacman.d-gnupg.mount;
		echo rm -rf /etc/systemd/system/getty@tty1.service.d;
		echo rm /etc/systemd/scripts/choose-mirror;
		# Remove special scripts of the Live environment
		echo rm /etc/systemd/system/getty@tty1.service.d/autologin.conf;
		echo rm /root/.automated_script.sh;
		echo rm /root/.zlogin;
		echo rm /etc/mkinitcpio-archiso.conf;
		echo rm -r /etc/initcpio/;
		# Import archlinux keys
		echo pacman-key --init;
		echo pacman-key --populate archlinux;
		echo sleep 5s;
		# Initramfs
		echo mkinitcpio -p linux;
		echo sleep 10s;
		echo exit;
	) | arch-chroot /mnt /bin/bash
}

function chroot_biosGrub() {
	## GRUB Stuff
	## Install Grub Bootloader
	(
		## Install Grub Bootloader
		echo pacman -S --needed grub;
		echo grub-install --target=i386-pc --recheck --force  /dev/sda;
		echo sleep 2s;
		echo cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo;
		echo grub-mkconfig -o /boot/grub/grub.cfg;
		echo sleep 2s;
		echo exit;
	) | arch-chroot /mnt /bin/bash

}

function chroot_uefiGrub() {
	# wifi-menu
	# sleep 3s # Waits 3 seconds.
	# pacman -Sy

	## GRUB Stuff
	## im gona want to enable internet to dl grub and efibootmgr
	## maybe in the near future I wont need to mess with even this step
	(
		## Install Grub Bootloader
		echo pacman -S --noconfirm grub;
		echo pacman -S --noconfirm efibootmgr;
		echo mkdir -p /boot/efi;
		echo mount /dev/mmcblk0p1 /boot/efi;
		echo sleep 4s;
		echo grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi;
		echo sleep 2s;
		echo grub-mkconfig -o /boot/grub/grub.cfg;
		echo sleep 2s;
		## tricky stuff to make sure it always boot
		echo mkdir -p /boot/efi/EFI/BOOT;
		echo cp /boot/efi/EFI/GRUB/grubx64.efi /boot/efi/EFI/BOOT/BOOTX64.EFI;
		echo "echo bcf boot add 1 fs0:\\\EFI\GRUB\\\grubx64.efi \\\"My GRUB bootloader\\\" >> /boot/efi/startup.nsh";
		echo "echo exit >> /boot/efi/startup.nsh";
		echo sleep 4s;
		echo exit;
	) | arch-chroot /mnt /bin/bash
}

function chroot_biosGrub_DUAL() {
	# wifi-menu
	# sleep 3s # Waits 3 seconds.
	# pacman -Sy

	## GRUB Stuff
	## im gona want to enable internet to dl grub and efibootmgr
	## maybe in the near future I wont need to mess with even this step
	(
		## Install Grub Bootloader
		# echo pacman -S --noconfirm grub;
		# echo pacman -S --noconfirm efibootmgr;
		echo mkdir -p /boot/efi;
		echo mount /dev/mmcblk0p3 /boot/efi;
		echo sleep 4s;
		echo grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi;
		echo sleep 2s;
		echo grub-mkconfig -o /boot/grub/grub.cfg;
		echo sleep 2s;
		## tricky stuff to make sure it always boot
		echo mkdir -p /boot/efi/EFI/BOOT;
		echo cp /boot/efi/EFI/GRUB/grubx64.efi /boot/efi/EFI/BOOT/BOOTX64.EFI;
		echo "echo bcf boot add 1 fs0:\\\EFI\GRUB\\\grubx64.efi \\\"My GRUB bootloader\\\" >> /boot/efi/startup.nsh";
		echo "echo exit >> /boot/efi/startup.nsh";
		echo sleep 4s;
		echo exit;
	) | arch-chroot /mnt /bin/bash
}

function biosgrub() {
	timedatectl set-ntp true

	## Partition Destination Drive
	echo "$FG_YELLOW
	About to prepare destination drive...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	format_HDD

	## Packstrap and cutomization
	echo "$FG_YELLOW
	About to prepare packstrap...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	packstrap_from_iso

	echo "$FG_YELLOW
	About to prepare chroot customizations...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	chroot_localization

	## GRUB STUFF
	echo "$FG_YELLOW
	About to prepare grub...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	chroot_biosGrub
}

function uefigrub() {
	timedatectl set-ntp true

	## Partition Destination Drive
	echo "$FG_YELLOW
	About to prepare destination drive...
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	format_SSD_HDD

	## Packstrap and cutomization
	echo "$FG_YELLOW
	About to prepare packstrap
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	packstrap_from_iso

	echo "$FG_YELLOW
	About to prepare chroot customizations...
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	chroot_localization

	## GRUB STUFF
	echo "$FG_YELLOW
	About to prepare grub
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	chroot_uefiGrub
}

function biosgrub_DUAL() {
	timedatectl set-ntp true

	## Partition Destination Drive
	echo "$FG_YELLOW
	About to prepare destination drive...
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	format_SSD_HDD_DUAL

	## Packstrap and cutomization
	echo "$FG_YELLOW
	About to prepare packstrap...
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	packstrap_from_iso

	echo "$FG_YELLOW
	About to prepare chroot customizations...
	$FG_NoColor"
	sleep 3s # Waits 3 seconds.
	chroot_localization

    ## GRUB STUFF
    echo ""
    read -e -p "Install Grub? [y/n] " -i "y" yn
    echo ""
    if [ $yn == 'y' ]; then
        echo "$FG_YELLOW
	    About to prepare grub...
	    $FG_NoColor"
	    sleep 3s # Waits 3 seconds.
	    chroot_biosGrub_DUAL
    fi
	sleep 3s # Waits 3 seconds.
}

function closeOut() {
	## Done
	umount -R /mnt

	sleep 5s # Waits 5 seconds.
	echo "$FG_YELLOW
	Just a 5 sec delay to allow unmounting to settle...
	"
	sleep 5s
	# shutdown now
	echo "$FG_YELLOW
	Done. Rebbot your system.
	"
	#reboot
}

function main_menu() {
	installer_menuPath=$(dirname $0)

	dialogBackTitle="Install from iso"
	dialogTitle="Bootloader Options"
	bootInstallMenuItem=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--cancel-label "Cancel" \
		--menu "Select you boot firmware type:" 0 0 0 \
		"1"     "Grub on BIOS Bootloader" \
		"2"     "Grub on UEFI Bootloader" \
		"3"		"Add Just Arch OS to Dual-Boot partition" )

	case $bootInstallMenuItem in
		1)
			biosgrub
			sleep 4s
			closeOut
			break
			;;
		2)
			uefigrub
			sleep 4s
			closeOut
			break
			;;
		3)
			clear
			biosgrub_DUAL
			sleep 4s
			closeOut
			break
			;;
		*)
			return
			;;
	esac

	clear

	## Done
	umount -R /mnt

}

## Start the DIY installer

if [ -f "/etc/arch-release" ]; then
	decorativeColors
	main_menu
	echo -e "\n\nDone. Arch Linux is installed.\n\nRestart when you are ready."

	echo "Computer will shut down shortly. A 10sec wait..."
	sleep 10s
	#poweroff
	sudo reboot
else
	echo "!!! This installer only works for Arch Linux !!!"
fi
