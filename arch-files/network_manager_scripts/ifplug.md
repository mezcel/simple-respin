# Network configuration/Ethernet

[arch wiki](https://wiki.archlinux.org/index.php/Network_configuration/Ethernet#ifplugd_for_laptops)

```sh
sudo pacman -S --needed ifplugd

## Check the "Ethernet controller"
sudo lspci -v

## Next, check that the driver was loaded
sudo dmesg | grep module_name

## ifplugd is a daemon which will automatically configure your Ethernet device when a cable is plugged in and automatically unconfigure it if the cable is pulled.

sudo systemctl enable ifplugd@.service
sudo systemctl start ifplugd@.service
```

### DHCPCD

```sh
sudo pacman -S --needed dhcpcd

## To start the daemon for all network interfaces,
sudo systemctl enable dhcpcd.service
sudo systemctl start dhcpcd.service

```
