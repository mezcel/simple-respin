#!/bin/bash

## A Dialog UI for nmcli

function identify_interfaces {
	myETH=$(ls /sys/class/net/ | grep -E enp)
	myWL=$(ls /sys/class/net/ | grep -E wl)
}

function nmcli_wifi_connect {
	sudo nmcli networking on

	dialogBackTitle="nmcli-menu"
	dialogTitle="Connect to Wifi Access Point"

	sudo nmcli device wifi rescan

	declare -a nmcliArray
	nmcliArray=($(nmcli -c no -m tabular -f SSID dev wifi))

	declare -a menuArray
	for ((i=1; i<${#nmcliArray[@]}; i+=1)); do
		rawVal=${nmcliArray[$i]}
		cleanedVal=${rawVal// /}
		menuArray+=("$(($i))" "$cleanedVal" )
	done

	selectedWifi=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--menu "wifi list" \
			12 50 ${#nmcliArray[@]} \
			${menuArray[@]}
			) || return

	ssidName=$(echo "${nmcliArray[$(($selectedWifi))]}" | xargs)

	wifiPassword=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--insecure --passwordbox "Wifi Password:" 10 50 "yourpassword" ) || return

	myTerminalAlias=$(whoami)@$(hostname)

	dialog \
		--backtitle "Network Connection" \
		--title "Attemting to log in" \
		--infobox "Process running:\n\n$myTerminalAlias:\nnmcli device wifi connect $ssidName password \$wifiPassword\n\nPlease just wait... its going to happen..." 11 80

	# nmcli networking on

	# nmcli device wifi connect $ssidName password $wifiPassword
	sudo nmcli device wifi connect $ssidName password $wifiPassword
	#clear
	echo -e "nmcli device wifi connect $ssidName\nwait delay..."; sleep 5s

	sudo nmcli connection up $ssidName

	echo -e "nmcli connection up $ssidName\n"
	read -e -p "Do you want to START or ENABLE this connection? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		sudo netctl enable $ssidName
		sudo netctl start $ssidName
	else
		sudo netctl start $ssidName
	fi

}

function new_access_point {
	identify_interfaces

	echo -e "\nWired or wireless?\n\tNetwork Interface:\n\t1)\tethernet = $myETH\n\t2)\twireless lan = $myWL\n"
	read -e -p "Enter an option number? [1 or 2]: " -i "2" onetwo
	if [ $onetwo -eq 1 ]; then
		## ethernet

		editConnection=$(dialog 2>&1 >/dev/tty \
			--backtitle "WI-FI, Hotspot, Internet Connection Settings (NetworkManager)" \
			--title "CONNECTION ACCESS POINT SETUP" \
			--form "\n  Network Interface: wl = $myWL, eth = $myETH" \
			 0 0 0 \
				"SSID:"	1	1	"ssid_name"	1 30 40 0 \
				"Mode: (wifi/ethernet)"	2	1	"ethernet"	2 30 40 0 \
				"Device HWaddr ifname:"	3	1	"$myETH"	3 30 40 0 \
				"Automatically connect:"	4	1	 "yes"	4 30 40 0 \
				"IPv4 Method: (shared/manual)"	5	1	"shared"	5 30 40 0 \
				"Password:"	6	1	"password1234"    6 30 40 0 \
				"ipv4.addresses: * ethernet *"	7	1	"192.168.0.100/24"	7 30 40 0 \
				"ipv4.gateway: * ethernet *"	8	1	"192.168.0.1"	8 30 40 0 )
	else
		## wifi

		editConnection=$(dialog 2>&1 >/dev/tty \
			--backtitle "WI-FI, Hotspot, Internet Connection Settings (NetworkManager)" \
			--title "CONNECTION ACCESS POINT SETUP" \
			--form "\n  Network Interface: wl = $myWL, eth = $myETH" \
			 0 0 0 \
				"SSID:"	1	1	"ssid_name"	1 30 40 0 \
				"Mode: (wifi/ethernet)"	2	1	"wifi"	2 30 40 0 \
				"Device HWaddr ifname:"	3	1	"$myWL"	3 30 40 0 \
				"Automatically connect:"	4	1	 "yes"	4 30 40 0 \
				"IPv4 Method: (shared/manual)"	5	1	"shared"	5 30 40 0 \
				"Password:"	6	1	"password1234"    6 30 40 0 \
				"ipv4.addresses: * ethernet *"	7	1	"10.42.0.1/24"	7 30 40 0 \
				"ipv4.gateway: * ethernet *"	8	1	"10.42.0.1"	8 30 40 0 )
	fi

    selectedSubMenuItem1=($editConnection)
    myconshow=($(nmcli device))

    if [[ " ${myconshow[@]} " =~ "${selectedSubMenuItem1[0]}" ]]; then
        dialog \
            --infobox "The SSID \"${selectedSubMenuItem1[0]}\", which you defined, allready exists.\n\nOnly make a NEW connection that is NEW to your system." 10 40

        #sleep 3
        read -t 3

    else
        echo "perform_connection_setup, wait delay..."; sleep 5s
        perform_connection_setup
    fi

}

function perform_connection_setup {
	## Template
	## nmcli con add type "wifi" ifname "wlp1s0" con-name "mylocalSSIDname" autoconnect yes ssid "theRouterSSIDname"
	## nmcli con modify "mylocalSSIDname" 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method "shared"
	## nmcli con modify "mylocalSSIDname" wifi-sec.key-mgmt wpa-psk
	## nmcli con modify "mylocalSSIDname" wifi-sec.psk "mypassword"
	## nmcli con modify "mylocalSSIDname" ipv4.addresses "192.168.0.22/16"
	## nmcli con modify "mylocalSSIDname" ipv4.gateway "192.168.0.1"
	##
	## OR
	## cp /etc/netctl/examples/wireless-wpa /etc/netctl/"mylocalSSIDname"
	## echo "Interface=wlp1s0" >> /etc/netctl/"mylocalSSIDname"
	## echo "Connection=wireless" >> /etc/netctl/"mylocalSSIDname"
	## echo "Security=wpa" >> /etc/netctl/"mylocalSSIDname"
	## echo "ESSID=routerssid" >> /etc/netctl/"mylocalSSIDname"
	## echo "IP=dhcp" >> /etc/netctl/"mylocalSSIDname"
	## echo "Key=mypassword" >> /etc/netctl/"mylocalSSIDname"

    ## involves password and radio
    if [[ "${selectedSubMenuItem1[1]}" == "wifi" ]]; then
        # Mode is wifi
        sudo nmcli con add type ${selectedSubMenuItem1[1]} ifname ${selectedSubMenuItem1[2]} con-name ${selectedSubMenuItem1[0]} autoconnect ${selectedSubMenuItem1[3]} ssid ${selectedSubMenuItem1[0]}
        sudo nmcli con modify ${selectedSubMenuItem1[0]} 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method ${selectedSubMenuItem1[4]}
        sudo nmcli con modify ${selectedSubMenuItem1[0]} wifi-sec.key-mgmt wpa-psk
        sudo nmcli con modify ${selectedSubMenuItem1[0]} wifi-sec.psk ${selectedSubMenuItem1[5]}
        sudo nmcli radio on
    fi

    ## involves ip's
    if [[ "${selectedSubMenuItem1[1]}" == "ethernet" ]]; then
        # Mode is ethernet
        sudo nmcli con add type ${selectedSubMenuItem1[1]} ifname ${selectedSubMenuItem1[2]} con-name ${selectedSubMenuItem1[0]} autoconnect ${selectedSubMenuItem1[3]}
        sudo nmcli con modify ${selectedSubMenuItem1[0]} ipv4.method ${selectedSubMenuItem1[4]}
        sudo nmcli con modify ${selectedSubMenuItem1[0]} ipv4.addresses ${selectedSubMenuItem1[6]}
        sudo nmcli con modify ${selectedSubMenuItem1[0]} ipv4.gateway ${selectedSubMenuItem1[7]}
    fi

    sudo nmcli con up ${selectedSubMenuItem1[0]}
    echo "nmcli con up ${selectedSubMenuItem1[0]}"
	echo "1/2 wait delay..."; sleep 5s
    sudo nmcli networking on
    echo "netctl start ${selectedSubMenuItem1[0]}"
	echo "2/2 wait delay..."; sleep 5s

	read -e -p "Do you want to START or ENABLE this connection? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		sudo netctl enable ${selectedSubMenuItem1[0]}
		sudo netctl start ${selectedSubMenuItem1[0]}
	else
		sudo netctl start ${selectedSubMenuItem1[0]}
	fi
}

function sshd_service {

	echo -e "\n## Enable SSH #############\n"

	sudo systemctl enable sshd
	sleep 2s
	sudo systemctl start sshd
	sleep 2s

}

function start_ssh_client {
	clear
	echo ""
	read -p "Enter Port: " mySSHport
	read -p "Enter host (ex: user@server-address): " mySSHhost
	echo "## ssh -p port user@server-address"
	echo "##################################"
	ssh -p $mySSHport $mySSHhost
	echo ""
}

function dissconnect_accesspoint {

	dialogBackTitle="nmcli-menu"
	dialogTitle="Disconnect from Wifi Accesspoint"

	declare -a nmcliArray
	nmcliArray=($(nmcli -c no -m tabular -f SSID dev wifi))

	declare -a menuArray
	for ((i=1; i<${#nmcliArray[@]}; i+=1)); do
		rawVal=${nmcliArray[$i]}
		cleanedVal=${rawVal// /}
		menuArray+=("$(($i))" "$cleanedVal" )
	done

	selectedWifi=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--menu "wifi list" \
			12 50 ${#nmcliArray[@]} \
			${menuArray[@]}
			) || return

	ssidName=$(echo "${nmcliArray[$(($selectedWifi))]}" | xargs)

	sudo nmcli connection down $ssidName
}

function delete_accesspoint {
	## nmcli connection delete id <connection name>
	echo ""
	echo "Delete access point not implemented"
	echo "# use this instead # nmcli connection delete id <connection name>"
	echo ""
}

function go_offline {
	echo -e "\n# systemctl stop ssh\n#################"
	sudo systemctl stop ssh

	echo -e "\n# nmcli radio off\n#################"
	sudo nmcli radio off

	echo -e "\n# nmcli networking off\n#################"
	sudo nmcli networking off

	echo -e "\n# systemctl stop NetworkManager\n#################"
	sudo systemctl stop NetworkManager
}

function exit_summary {
	## Display Network State
	clear
	echo "exit_summary, wait delay..."; sleep 5s
	clear
	#reset

	# ip a
	echo "--- Display networking state ---"
	echo ""

	echo -e "\n## nmcli dev wifi\n#################"
	sudo nmcli dev wifi

	echo -e "\n## nmcli dev status\n#################"
	sudo nmcli dev status

	echo -e "\n## nmcli radio\n#################"
	sudo nmcli radio

	echo -e "\n## systemctl status sshd\n#################"
	sudo systemctl status sshd

	echo -e "\n## nmcli con show\n#################"
	sudo nmcli con show

	echo "## cat /etc/NetworkManager/system-connections/Network to view and mod your connections points "

	command -v netctl &>/dev/null
	isNetctl=$?
	if [ $isNetctl -eq 0 ]; then
		echo ""
		sudo systemctl status netctl
		echo ""
	fi

	read -p "presss any key to continue ..." exitVar

	## clear user defined bash vars and history
	#exec bash
	exit
}

function main_connection_menu {

	# sudo systemctl start NetworkManager

	dialogBackTitle="Network Manager"
	dialogTitle="Access Point Options"
	selectedMenuItem=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--cancel-label "Back" \
		--menu "Select network option:" 0 0 0 \
			"1"	"Connect to SSID" \
			"2"	"New Access Point" \
			"3"	"Dissconnect Access Point" \
			"4"	"Delete Access Point" \
			"5"	"Go Offline" \
			"6"	"Display Network State" ) || return

	case $selectedMenuItem in
		1)
			clear
			nmcli_wifi_connect
			;;
		2)
			clear
			new_access_point
			;;
		3)
			clear
			dissconnect_accesspoint
			;;
		4)
			clear
			delete_accesspoint
			;;
		5)
			clear
			go_offline
			;;
		6)
			clear
			exit_summary
			;;
	esac
}

function network_dependency {
	ping -c3 google.com
	isPing=$?
	if [ $isPing -eq 0 ]; then
		sudo pacman -Sy
		sudo pacman -S --needed iproute2
		sudo pacman -S --needed ifplugd
		sudo pacman -S --needed openssh
		sudo pacman -S --needed git
		sudo pacman -S --needed networkmanager
	fi

	command -v NetworkManager
	if [ $? -ne 0 ]; then
		echo -e "\nAborted.\n\tNetworkManager was not detected.\n"
		exit
	fi
	sudo systemctl enable NetworkManager
	sleep 5s
	sudo systemctl start NetworkManager
	sleep 5s

	sudo systemctl enable dhcpcd.service
	sleep 5s
	sudo systemctl start dhcpcd.service
	sleep 5s
}

network_dependency
main_connection_menu
exit_summary
exit
