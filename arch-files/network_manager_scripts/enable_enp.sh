#!/bin/bash

## guidance: https://wiki.archlinux.org/index.php/Dhcpcd

function dl_prereq {
	sudo pacman -S --needed ifplugd
	sudo pacman -S --needed dhcpcd
}

function set_eth_ip {
	myEth=$(ls /sys/class/net | grep -E enp)
	#sudo hostnamectl set-hostname kangaroo
	sudo ip addres add 10.42.0.1/24 broadcast + dev $myEth
	sudo ip rout add 24 via 10.42.0.1 dev $myEth
	sudo ip link set $myEth up
}

function enable_dhcpcd {
	#sudo dhcpcd $myEth
	sudo systemctl enable dhcpcd.service
	sudo systemctl start dhcpcd.service
}

function use_ifplug {
	sudo systemctl enable ifplugd@.service
	sudo systemctl start ifplugd@.service
}

function show_status {
	#sudo lspci -v
	myEth=$(ls /sys/class/net | grep -E enp)
	#sudo ip addr show
	sudo ip addr show $myEth
	#sudo systemctl list-units | grep dhcp
}

function enable_sshd {
	sudo systemctl enable sshd
	sudo systemctl start sshd
	sudo systemctl status sshd
}

function write_conf_file {
	sudo touch /etc/dhcpcd.conf
	myEth=$(ls /sys/class/net | grep -E enp)
	myIp=10.42.0.1
	sudo echo "interface $myEth" > /etc/dhcpcd.conf
	sudo echo "profile static_$myEth" >> /etc/dhcpcd.conf
	sudo echo "static ip_address=$myIp/24" >> /etc/dhcpcd.conf
	sudo echo "static routers=$myIp" >> /etc/dhcpcd.conf
	sudo echo "static domain_name=$myIp" >> /etc/dhcpcd.conf
}

## run

dl_prereq
enable_dhcpcd
use_ifplug
show_status
