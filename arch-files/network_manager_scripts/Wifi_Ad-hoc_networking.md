# Wifi Ad-hoc networking

[arch wiki](https://wiki.archlinux.org/index.php/Ad-hoc_networking)

```sh
## Set the operation mode to ibss
sudo iw interface set type ibss

## Bring the interface up (an additional step like rfkill unblock wifi might be needed)
sudo ip link set interface up

## create an ad-hoc network
sudo iw interface ibss join your_ssid frequency
```

### WPA supplicant

/etc/wpa_supplicant-adhoc.conf

```sh
ctrl_interface=DIR=/run/wpa_supplicant GROUP=wheel

# use 'ap_scan=2' on all devices connected to the network
# this is unnecessary if you only want the network to be created when no other networks are available
ap_scan=2

network={
    ssid="MySSID"
    mode=1
    frequency=2432
    proto=RSN
    key_mgmt=WPA-PSK
    pairwise=CCMP
    group=CCMP
    psk="secret passphrase"
}
```

Run wpa_supplicant on all devices connected to the network
```sh
wpa_supplicant -B -i interface -c /etc/wpa_supplicant-adhoc.conf -D nl80211,wext
```
