#!/bin/bash

## A Dialog UI for nmcli

function identify_interfaces {
	myETH0=$(ls /sys/class/net/ | grep -E enp)
	myWL=$(ls /sys/class/net/ | grep -E wl)
}

function nmcli_wifi_connect {
	nmcli networking on

	dialogBackTitle="nmcli-menu"
	dialogTitle="Connect to Wifi Access Point"

	nmcli device wifi rescan

	declare -a nmcliArray
	nmcliArray=($(nmcli -c no -m tabular -f SSID dev wifi))

	declare -a menuArray
	for ((i=1; i<${#nmcliArray[@]}; i+=1)); do
		rawVal=${nmcliArray[$i]}
		cleanedVal=${rawVal// /}
		menuArray+=("$(($i))" "$cleanedVal" )
	done

	selectedWifi=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--menu "wifi list" \
			12 50 ${#nmcliArray[@]} \
			${menuArray[@]}
			) || return

	ssidName=$(echo "${nmcliArray[$(($selectedWifi))]}" | xargs)

	wifiPassword=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--insecure --passwordbox "Wifi Password:" 10 50 "yourpassword" ) || return

	myTerminalAlias=$(whoami)@$(hostname)

	dialog \
		--backtitle "Network Connection" \
		--title "Attemting to log in" \
		--infobox "Process running:\n\n$myTerminalAlias:\nnmcli device wifi connect $ssidName password \$wifiPassword\n\nPlease just wait... its going to happen..." 11 80

	# nmcli networking on

	# nmcli device wifi connect $ssidName password $wifiPassword
	nmcli device wifi connect $ssidName password $wifiPassword
	#clear
	echo -e "nmcli device wifi connect $ssidName\nwait delay..."; sleep 5s

	nmcli connection up $ssidName

	echo -e "nmcli connection up $ssidName\n"
	read -e -p "Do you want to START or ENABLE this connection? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		netctl enable $ssidName
		netctl start $ssidName
	else
		netctl start $ssidName
	fi

}

function new_access_point {
	identify_interfaces

    editConnection=$(dialog 2>&1 >/dev/tty \
        --backtitle "WI-FI, Hotspot, Internet Connection Settings (NetworkManager)" \
        --title "CONNECTION ACCESS POINT SETUP" \
        --form "\n  Network Interface: wl = myWL, eth0 = $myETH0" \
         0 0 0 \
            "SSID:"	1	1	"A_Ssid_Connection_Name"	1 30 40 0 \
            "Mode: (wifi/ethernet)"	2	1	"wifi"	2 30 40 0 \
            "Device HWaddr ifname:"	3	1	"$myWL"	3 30 40 0 \
            "Automatically connect:"	4	1	 "yes"	4 30 40 0 \
            "IPv4 Method: (shared/manual)"	5	1	"shared"	5 30 40 0 \
            "Password:"	6	1	"password1234"    6 30 40 0 \
            "ipv4.addresses: * ethernet *"	7	1	"192.168.0.100/24"	7 30 40 0 \
            "ipv4.gateway: * ethernet *"	8	1	"192.168.0.1"	8 30 40 0 )


    selectedSubMenuItem1=($editConnection)
    myconshow=($(nmcli device))

    if [[ " ${myconshow[@]} " =~ "${selectedSubMenuItem1[0]}" ]]; then
        dialog \
            --infobox "The SSID \"${selectedSubMenuItem1[0]}\", which you defined, allready exists.\n\nOnly make a NEW connection that is NEW to your system." 10 40

        #sleep 3
        read -t 3

    else
        echo "perform_connection_setup, wait delay..."; sleep 5s
        perform_connection_setup
    fi

}

function perform_connection_setup {
	## Template
	## nmcli con add type "wifi" ifname "wlp1s0" con-name "mylocalSSIDname" autoconnect yes ssid "theRouterSSIDname"
	## nmcli con modify "mylocalSSIDname" 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method "shared"
	## nmcli con modify "mylocalSSIDname" wifi-sec.key-mgmt wpa-psk
	## nmcli con modify "mylocalSSIDname" wifi-sec.psk "mypassword"
	## nmcli con modify "mylocalSSIDname" ipv4.addresses "192.168.0.22/16"
	## nmcli con modify "mylocalSSIDname" ipv4.gateway "192.168.0.1"
	##
	## OR
	## cp /etc/netctl/examples/wireless-wpa /etc/netctl/"mylocalSSIDname"
	## echo "Interface=wlp1s0" >> /etc/netctl/"mylocalSSIDname"
	## echo "Connection=wireless" >> /etc/netctl/"mylocalSSIDname"
	## echo "Security=wpa" >> /etc/netctl/"mylocalSSIDname"
	## echo "ESSID=routerssid" >> /etc/netctl/"mylocalSSIDname"
	## echo "IP=dhcp" >> /etc/netctl/"mylocalSSIDname"
	## echo "Key=mypassword" >> /etc/netctl/"mylocalSSIDname"

    ## involves password and radio
    if [[ "${selectedSubMenuItem1[1]}" == "wifi" ]]; then
        # Mode is wifi
        nmcli con add type ${selectedSubMenuItem1[1]} ifname ${selectedSubMenuItem1[2]} con-name ${selectedSubMenuItem1[0]} autoconnect ${selectedSubMenuItem1[3]} ssid ${selectedSubMenuItem1[0]}
        nmcli con modify ${selectedSubMenuItem1[0]} 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method ${selectedSubMenuItem1[4]}
        nmcli con modify ${selectedSubMenuItem1[0]} wifi-sec.key-mgmt wpa-psk
        nmcli con modify ${selectedSubMenuItem1[0]} wifi-sec.psk ${selectedSubMenuItem1[5]}
        nmcli radio on
    fi

    ## involves ip's
    if [[ "${selectedSubMenuItem1[1]}" == "ethernet" ]]; then
        # Mode is ethernet
        nmcli con add type ${selectedSubMenuItem1[1]} ifname ${selectedSubMenuItem1[2]} con-name ${selectedSubMenuItem1[0]} autoconnect ${selectedSubMenuItem1[3]}
        nmcli con modify ${selectedSubMenuItem1[0]} ipv4.method ${selectedSubMenuItem1[4]}
        nmcli con modify ${selectedSubMenuItem1[0]} ipv4.addresses ${selectedSubMenuItem1[6]}
        nmcli con modify ${selectedSubMenuItem1[0]} ipv4.gateway ${selectedSubMenuItem1[7]}
    fi

    nmcli con up ${selectedSubMenuItem1[0]}
    echo "nmcli con up ${selectedSubMenuItem1[0]}"
	echo "1/2 wait delay..."; sleep 5s
    nmcli networking on
    echo "netctl start ${selectedSubMenuItem1[0]}"
	echo "2/2 wait delay..."; sleep 5s

	read -e -p "Do you want to START or ENABLE this connection? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		netctl enable ${selectedSubMenuItem1[0]}
		netctl start ${selectedSubMenuItem1[0]}
	else
		netctl start ${selectedSubMenuItem1[0]}
	fi
}

function sshd_service {
	dialogBackTitle="SSH Server"
	dialogTitle="SSHD Options"
	selectedSSHMenu=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--cancel-label "Back" \
		--menu "SSHD Options:" 0 0 0 \
			"1"	"systemctl start sshd" \
			"2"	"systemctl enable sshd" \
			"3"	"systemctl stop sshd" \
			"4"	"systemctl restart sshd" ) || return

	if [[ $selectedSSHMenu == 1 ]]; then
		clear
		systemctl start sshd
	fi

	if [[ $selectedSSHMenu == 2 ]]; then
		clear
		systemctl enable sshd
		systemctl start sshd
	fi

	if [[ $selectedSSHMenu == 3 ]]; then
		clear
		systemctl stop sshd
	fi

	if [[ $selectedSSHMenu == 4 ]]; then
		clear
		systemctl restart sshd
	fi

}

function start_ssh_client {
	clear
	echo ""
	read -p "Enter Port: " mySSHport
	read -p "Enter host (ex: user@server-address): " mySSHhost
	echo "## ssh -p port user@server-address"
	echo "##################################"
	ssh -p $mySSHport $mySSHhost
	echo ""
}

function dissconnect_accesspoint {

	dialogBackTitle="nmcli-menu"
	dialogTitle="Disconnect from Wifi Accesspoint"

	declare -a nmcliArray
	nmcliArray=($(nmcli -c no -m tabular -f SSID dev wifi))

	declare -a menuArray
	for ((i=1; i<${#nmcliArray[@]}; i+=1)); do
		rawVal=${nmcliArray[$i]}
		cleanedVal=${rawVal// /}
		menuArray+=("$(($i))" "$cleanedVal" )
	done

	selectedWifi=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--menu "wifi list" \
			12 50 ${#nmcliArray[@]} \
			${menuArray[@]}
			) || return

	ssidName=$(echo "${nmcliArray[$(($selectedWifi))]}" | xargs)

	nmcli connection down $ssidName
}

function delete_accesspoint {
	## nmcli connection delete id <connection name>
	echo ""
	echo "Delete access point not implemented"
	echo "# use this instead # nmcli connection delete id <connection name>"
	echo ""
}

function go_offline {
	echo -e "\n# systemctl stop ssh\n#################"
	systemctl stop ssh

	echo -e "\n# nmcli radio off\n#################"
	nmcli radio off

	echo -e "\n# nmcli networking off\n#################"
	nmcli networking off

	echo -e "\n# systemctl stop NetworkManager\n#################"
	systemctl stop NetworkManager
}

function exit_summary {
	## Display Network State
	clear
	echo "exit_summary, wait delay..."; sleep 5s
	clear
	#reset

	# ip a
	echo "--- Display networking state ---"
	echo ""

	echo -e "\n## nmcli dev wifi\n#################"
	nmcli dev wifi

	echo -e "\n## nmcli dev status\n#################"
	nmcli dev status

	echo -e "\n## nmcli radio\n#################"
	nmcli radio

	echo -e "\n## systemctl status sshd\n#################"
	systemctl status sshd

	echo -e "\n## nmcli con show\n#################"
	nmcli con show

	echo "## cat /etc/NetworkManager/system-connections/Network to view and mod your connections points "

	command -v netctl &>/dev/null
	isNetctl=$?
	if [ $isNetctl -eq 0 ]; then
		echo ""
		sudo systemctl status netctl
		echo ""
	fi

	read -p "presss any key to continue ..." exitVar

	## clear user defined bash vars and history
	#exec bash
	exit
}

function main_connection_menu {

	# sudo systemctl start NetworkManager

	dialogBackTitle="Network Manager"
	dialogTitle="Access Point Options"
	selectedMenuItem=$(dialog 2>&1 >/dev/tty \
		--backtitle "$dialogBackTitle" \
		--title "$dialogTitle" \
		--cancel-label "Back" \
		--menu "Select network option:" 0 0 0 \
			"1"	"Connect to SSID" \
			"2"	"New Access Point" \
			"3"	"SSH Server" \
			"4"	"Start SSH Client" \
			"5"	"Dissconnect Access Point" \
			"6"	"Delete Access Point" \
			"7"	"Go Offline" \
			"8"	"Display Network State" ) || return

	case $selectedMenuItem in
		1)
			clear
			nmcli_wifi_connect
			;;
		2)
			clear
			new_access_point
			;;
		3)
			clear
			sshd_service
			systemctl status sshd
			;;
		4)
			clear
			start_ssh_client
			;;
		5)
			clear
			dissconnect_accesspoint
			;;
		6)
			clear
			delete_accesspoint
			;;
		7)
			clear
			go_offline
			;;
		8)
			clear
			exit_summary
			;;
	esac
}


function network_dependency {
	clear
	echo -e "\n## NetworkManager state:\n########################\n"

	if [ -f "/etc/debian_version" ]; then
		apt-cache show network-manager
		if [ $? -ne 0 ]; then
			sudo apt install wireless-tools wpasupplicant network-manager
		fi

	elif [ -f "/etc/arch-release" ]; then
		sudo pacman -Qi networkmanager
		if [ $? -ne 0 ]; then
			sudo pacman -S --needed networkmanager
		fi

	else
		echo "This installer is intended for Arch or Debian based distros"
	fi

	read -e -p "Do you want to START or ENABLE NetworkManager? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		sudo systemctl enable NetworkManager
		sudo systemctl start NetworkManager
	else
		sudo systemctl start NetworkManager
	fi

	echo "NetworkManager, wait delay..."; sleep 5s

	read -e -p "Do you want to START or ENABLE dhcpcd.service? [start/enable]: " -i "start" startEnable
	if [ $startEnable == "enable" ]; then
		sudo systemctl enable dhcpcd.service
		sudo systemctl start dhcpcd.service
	else
		sudo systemctl start dhcpcd.service
	fi
	echo "dhcpcd.service, wait delay..."; sleep 5s
}

network_dependency
main_connection_menu
exit_summary
exit
