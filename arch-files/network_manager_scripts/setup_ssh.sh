#!/bin/bash

sudo pacman -S --needed dhcpcd

sudo systemctl enable dhcpcd.service
sudo systemctl start dhcpcd.service

myEthernetInterface=$(ls /sys/class/net/ | grep -E enp)
echo -e "\nEthernet Interface\t= $myEthernetInterface"
myWifiInterface=$(ls /sys/class/net/ | grep -E wl)
echo -e "Wifi Interface\t\t= $myWifiInterface\n"

echo -e "Review the \"/etc/ssh/sshd_config\" file"
read -p "[press any key to continue]" pauseVar
sudo vim /etc/ssh/sshd_config

sudo ip link set dev $myEthernetInterface up
sudo dhcpcd $myEthernetInterface

sudo pacman -S --needed openssh

sudo systemctl enable sshd
sudo systemctl start sshd

exit

## Notes:

## https://www.ostechnix.com/configure-static-dynamic-ip-address-arch-linux/

sudo pacman -S --needed openssh
sudo systemctl start sshd
sudo systemctl status sshd

///////////////////////////////////////
Method 1: Configure Static IP Address in Arch Linux using netctl

sudo systemctl stop dhcpcd.service
sudo systemctl stop dhcpcd
sudo systemctl disable dhcpcd

sudo cp /etc/netctl/examples/ethernet-static /etc/netctl/enp0s3
sudo nano /etc/netctl/enp0s3
	Description='A basic static ethernet connection'
	Interface=enp0s3
	Connection=ethernet
	IP=static
	Address=('192.168.1.102/24')
	Gateway=('192.168.1.1')
	DNS=('8.8.8.8' '8.8.4.4')

sudo netctl enable enp0s3
sudo netctl start enp0s3
sudo systemctl start dhcpcd.service

ip addr

///////////////////////////////////////
Method 2: Configure Static IP Address in Arch Linux using systemd

sudo systemctl stop dhcpcd
sudo systemctl disable dhcpcd

sudo pacman -Rns netctl

sudo nano /etc/systemd/network/enp0s3.network
	[Match]
	Name=enp0s3

	[Network]
	Address=192.168.1.102/24
	Gateway=192.168.1.1
	DNS=8.8.8.8
	DNS=8.8.4.4

sudo systemctl list-unit-files
sudo systemctl disable netctl@enp0s3.service

sudo systemctl enable systemd-networkd
sudo systemctl start systemd-networkd

reboot
