#!/bin/bash

#thisDir=$(pwd)
#thisDir=$(dirname $0)

function tput_color_variables {
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)

	BG_NoColor=$(tput sgr0)
	FG_NoColor=$(tput sgr0)
}

function additional_software_prompt {
	read -e -p "Do you also want to install LibreOffice with LaTex? [y/N]: " -i "n" yesnoLibreLatex
	echo -e "\nDeveloper tools:\n\tzenity, java, glade, bluez, firejail\n"
	read -e -p "Do you also want to install developer toos? [y/N]: " -i "n" yesnoDeveloper
	read -e -p "Do you want all available GPU paackages? (\"all\" = all multiple GPSs, \"n\" = generic AMD or Intel GPU) [all/N]: " -i "n" mygpu
	echo ""
	sleep 3s
}

function arch_server_pacman {
	sudo pacman -Sy

	echo "$FG_GREEN"
	read -p "Full system upgrade (-Syyu)? [y/N] : " yn
	echo "$FG_NoColor"
	case $yn in
		[Yy]* )
			sudo pacman -Syyu --noconfirm

			echo "$FG_YELLOW"
			echo "After a full upgrade, rebooting can help pacman run smoother."
			echo "$FG_NoColor"
			read -p "Do you want to reboot now? [y/N] : " yn
			;;
		* )
			echo "$FG_YELLOW"
			echo "Continuing without a reboot"
			echo "$FG_NoColor"
			sleep 2s
			;;
	esac

	sudo pacman -S --noconfirm --needed base
	sudo pacman -S --noconfirm --needed base-devel

	sudo pacman -S --noconfirm --needed gcc
	sudo pacman -S --noconfirm --needed cmake
	sudo pacman -S --noconfirm --needed extra-cmake-modules
	sudo pacman -S --noconfirm --needed python
	sudo pacman -S --noconfirm --needed python-pip
	sudo pacman -S --noconfirm --needed python-setuptools
	sudo pacman -S --noconfirm --needed python2
	sudo pacman -S --noconfirm --needed python2-pip
	sudo pacman -S --noconfirm --needed python2-setuptools
	sudo pacman -S --noconfirm --needed python2-xdg
	#sudo pacman -S --noconfirm --needed java-runtime-common jdk-openjdk jre-openjdk
	#sudo pacman -S --noconfirm --needed nodejs npm
	sudo pacman -S --noconfirm --needed ruby
	sudo pacman -S --noconfirm --needed perl

	sudo pacman -S --noconfirm --needed xorg
	sudo pacman -S --noconfirm --needed xorg-xinit
	sudo pacman -S --noconfirm --needed xorg-server
	sudo pacman -S --noconfirm --needed xorg-server-utils
	sudo pacman -S --noconfirm --needed xorg-apps
	sudo pacman -S --noconfirm --needed xorg-xrandr
	sudo pacman -S --noconfirm --needed xorg-xfontsel

	sudo pacman -S --noconfirm --needed git
	sudo pacman -S --noconfirm --needed wget
	sudo pacman -S --noconfirm --needed sed
	sudo pacman -S --noconfirm --needed curl

	#sudo pacman -S --noconfirm --needed net-tools networkmanagercommand -v NetworkManager
	if [ $? -ne 0 ]; then
		## install wicd only if NetworkManager is not installed
		## if you got to this point, good jorb getting internet access
		sudo pacman -S --noconfirm --needed wicd
	fi

	sudo pacman -S --noconfirm --needed openssh
	sudo pacman -S --noconfirm --needed macchanger
	sudo pacman -S --noconfirm --needed hostapd
	sudo pacman -S --noconfirm --needed iptables
	sudo pacman -S --noconfirm --needed nftables
	sudo pacman -S --noconfirm --needed create_ap
	sudo pacman -S --noconfirm --needed bmon
	sudo pacman -S --noconfirm --needed wavemon
	sudo pacman -S --noconfirm --needed elinks
	sudo pacman -S --noconfirm --needed w3m
	sudo pacman -S --noconfirm --needed vim
	sudo pacman -S --noconfirm --needed tmux
	sudo pacman -S --noconfirm --needed ranger
	sudo pacman -S --noconfirm --needed mc
	sudo pacman -S --noconfirm --needed tree
	sudo pacman -S --noconfirm --needed aspell
	sudo pacman -S --noconfirm --needed hunspell
	sudo pacman -S --noconfirm --needed ispell
	sudo pacman -S --noconfirm --needed enchant
	#sudo pacman -S --noconfirm --needed languagetool
    sudo pacman -S --noconfirm --needed xclip
	sudo pacman -S --noconfirm --needed xsel
	sudo pacman -S --noconfirm --needed pv

	sudo pacman -S --noconfirm --needed p7zip
	sudo pacman -S --noconfirm --needed zip
	sudo pacman -S --noconfirm --needed unzip
	sudo pacman -S --noconfirm --needed unrar
	sudo pacman -S --noconfirm --needed adobe-source-code-pro-fonts
	sudo pacman -S --noconfirm --needed adobe-source-sans-pro-fonts
	sudo pacman -S --noconfirm --needed ttf-ubuntu-font-family
	sudo pacman -S --noconfirm --needed ttf-inconsolata

	sudo pacman -S --noconfirm --needed alsa
	sudo pacman -S --noconfirm --needed alsa-utils
	sudo pacman -S --noconfirm --needed alsa-firmware
	sudo pacman -S --noconfirm --needed pulseaudio
	sudo pacman -S --noconfirm --needed pulseaudio-alsa
	sudo pacman -S --noconfirm --needed pavucontrol
	sudo pacman -S --noconfirm --needed vorbis-tools
	#sudo pacman -S --noconfirm --needed mplayer
	#sudo pacman -S --noconfirm --needed tlp
	#sudo pacman -S --noconfirm --needed acpi upower powertop

	sudo pacman -S --noconfirm --needed acpi
	sudo pacman -S --noconfirm --needed upower
	sudo pacman -S --noconfirm --needed htop
	sudo pacman -S --noconfirm --needed xclip
	sudo pacman -S --noconfirm --needed xorg-xclipboard
	#sudo pacman -S --noconfirm --needed screenfetch neofetch cmatrix
	sudo pacman -S --noconfirm --needed reflector

	#sudo pacman -S --noconfirm --neeed bluez
	#sudo pacman -S --noconfirm --neeed bluez-utils

	## optimize remote package list
	command -v reflector
	if [ $? -eq 0 ]; then
		echo -e "\nOrganizing mirrorlist ...\n"
		sleep 4s
		sudo reflector --country 'United States' --sort rate --save /etc/pacman.d/mirrorlist
	fi

	## clean out unused packages
	sudo pacman -Sc
}

function xorg_desktop_pacman {

	cpuvendor=$(cat /proc/cpuinfo | grep vendor | uniq | awk '{printf $3}')
	[[ -z $cpuvendor ]]; cpuvendor="unknown"

	if [ $mygpu -eq "all" ]; then
		sudo pacman -S --needed --noconfirm xf86-video-intel
		sudo pacman -S --needed --noconfirm xf86-video-amdgpu
		sudo pacman -S --needed --noconfirm xf86-video-ati
		sudo pacman -S --needed --noconfirm xf86-video-fbdev
		sudo pacman -S --needed --noconfirm xf86-video-vesa

		sudo pacman -S --needed --noconfirm nvidia
		sudo pacman -S --needed --noconfirm nvidia-utils
		sudo pacman -S --needed --noconfirm lib32-nvidia-utils
		sudo pacman -S --needed --noconfirm nvidia-390xx
		sudo pacman -S --needed --noconfirm nvidia-390xx-utils
		sudo pacman -S --needed --noconfirm lib32-nvidia-390xx-utils
	elif [ $cpuvendor -eq "GenuineIntel" ]; then
		sudo pacman -S --needed --noconfirm xf86-video-intel
	elif [ $cpuvendor -eq "AuthenticAMD" ]; then
		sudo pacman -S --needed --noconfirm xf86-video-amdgpu
		sudo pacman -S --needed --noconfirm xf86-video-ati
	else
		## universal generic
		sudo pacman -S --needed --noconfirm xf86-video-fbdev
		sudo pacman -S --needed --noconfirm xf86-video-vesa
	fi

	sudo pacman -S --needed --noconfirm mesa
	sudo pacman -S --needed --noconfirm lib32-mesa

	sudo pacman -S --needed --noconfirm transset-df
	sudo pacman -S --needed --noconfirm xcompmgr

	sudo pacman -S --noconfirm --needed xrandr
	sudo pacman -S --noconfirm --needed arandr
	sudo pacman -S --noconfirm --needed xterm
	sudo pacman -S --noconfirm --needed st

	sudo pacman -S --noconfirm --needed xorg-twm
	sudo pacman -S --noconfirm --needed gtk2
	sudo pacman -S --noconfirm --needed gtk3
	sudo pacman -S --noconfirm --needed openbox
	sudo pacman -S --noconfirm --needed obconf
	sudo pacman -S --noconfirm --needed lxappearance
	sudo pacman -S --noconfirm --needed lxappearance-obconf
	sudo pacman -S --noconfirm --needed lxinput
	sudo pacman -S --noconfirm --needed lxrandr
	sudo pacman -S --noconfirm --needed dmenu
	sudo pacman -S --noconfirm --needed rofi
	#sudo pacman -S --noconfirm --needed surf

	sudo pacman -S --noconfirm --needed feh
	sudo pacman -S --noconfirm --needed mupdf
	sudo pacman -S --noconfirm --needed pcmanfm
	sudo pacman -S --noconfirm --needed gvfs
	sudo pacman -S --noconfirm --needed tumbler
	sudo pacman -S --noconfirm --needed xarchiver
	sudo pacman -S --noconfirm --needed evince
	sudo pacman -S --noconfirm --needed udiskie
	#sudo pacman -S --noconfirm --needed thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin
	sudo pacman -S --noconfirm --needed ffmpegthumbnailer
	sudo pacman -S --noconfirm --needed poppler-glib
	sudo pacman -S --noconfirm --needed libgsf
	sudo pacman -S --noconfirm --needed libopenraw
	sudo pacman -S --noconfirm --needed freetype2
	sudo pacman -S --noconfirm --needed geany
	sudo pacman -S --noconfirm --needed geany-plugins
	sudo pacman -S --noconfirm --needed mousepad
	sudo pacman -S --noconfirm --needed arc-solid-gtk-theme

	#sudo pacman -S --noconfirm --needed vlc
	#sudo pacman -S --noconfirm --needed chromium gnumeric abiword retext

	#echo -e "\n### Word Processing ###\n"
	#read -e -p "Do you want to install LibreOffice and LaTex? [y/N]: " -i "n" yesnoLibreLatex
	if [ $yesnoLibreLatex == "y" ]; then
		sudo pacman -S --needed --noconfirm libreoffice-still
		sudo pacman -S --needed --noconfirm libreoffice-extension-texmaths
		sudo pacman -S --needed --noconfirm libreoffice-extension-writer2latex

		sudo pacman -S --needed --noconfirm texlive-most
		sudo pacman -S --needed --noconfirm biber
		sudo pacman -S --needed --noconfirm texstudio
		sudo pacman -S --needed --noconfirm pandoc
		sudo pacman -S --needed --noconfirm mupdf
	fi

	mkdir -p ~/.icons/
	mkdir -p ~/.themes/
	mkdir -p ~/Pictures/
	mkdir -p ~/Downloads/
	mkdir -p ~/github/mezcel/
	mkdir -p ~/AUR/
}

function github_packages {
	mkdir -p ~/github/

	sudo pacman -S --needed --noconfirm git
	sudo pacman -S --needed --noconfirm ruby

	## My DWM & GTK & CLI theme
	git clone https://github.com/mezcel/nordic-respin.git ~/github/mezcel/nordic-respin
	#bash ~/github/mezcel/nordic-respin/install.sh

	## powerline font
	git clone https://github.com/powerline/fonts.git ~/github/powerline/fonts
	sudo mkdir -p /usr/share/consolefonts/
	sleep 2s
	sudo cp -rf ~/github/powerline/fonts/Terminus/PSF/*.psf.gz /usr/share/consolefonts/
	sleep 2s

	## Nerdfonts (2.2G)
	## git clone https://github.com/ryanoasis/nerd-fonts.git ~/github/ryanoasis/nerd-fonts
	#git clone https://github.com/just3ws/nerd-font-cheatsheets.git ~/github/just3ws/nerd-font-cheatsheets

	## cursor
	#git clone https://github.com/mustafaozhan/Breeze-Adapta.git ~/github/mustafaozhan/Breeze-Adapta
	#echo -e "\n* Breeze-Adapta mouse cursor = ~/github/mustafaozhan/Breeze-Adapta" >> ~/github/README.md
	#mkdir -p ~/.icons/
	#sleep 2s
	#cp -r ~/github/mustafaozhan/Breeze-Adapta ~/.icons/
	#sleep 2s
	#cd ~/github/mustafaozhan/Breeze-Adapta/
	#./install.sh
	#sleep 2s

	## icon theme
	git clone https://github.com/Bonandry/adwaita-plus.git ~/github/Bonandry/adwaita-plus
	cd ~/github/Bonandry/adwaita-plus/
	./install.sh

	## minimal dark window frame gtk theme (xfce4)
	#mkdir -p ~/.themes/
	#sleep 2s
	#git clone https://github.com/fakedeltatime/flat-two-color.git ~/.themes/flat-two-color

	## tty console
	## solarized
	git clone https://github.com/adeverteuil/console-solarized.git ~/github/adeverteuil/console-solarized
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized /usr/local/bin/
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized.conf /etc/
	sudo cp ~/github/adeverteuil/console-solarized/console-solarized@.service /etc/systemd/system/
	sudo mkdir -p /etc/systemd/system/getty@.service.d/
	sleep 2s
	sudo cp ~/github/adeverteuil/console-solarized/solarized.conf /etc/systemd/system/getty@.service.d/
	sleep 2s
	sudo systemctl daemon-reload

	## nord tty
	git clone https://github.com/lewisacidic/nord-tty.git ~/github/lewisacidic/nord-tty
	git clone https://github.com/arcticicestudio/nord-xresources.git ~/github/arcticicestudio/nord-xresources

	## Nord terminal colors
	git clone https://github.com/khamer/base16-termite.git ~/github/khamer/base16-termite

	#git clone https://github.com/sindresorhus/github-markdown-css.git ~/github/sindresorhus/github-markdown-css

	## Retext CSS
	#git clone https://gist.github.com/8849279.git ~/gist.github/EndangeredMassa/ReText.conf

	## A decorative Grub theme
	git clone https://github.com/shvchk/poly-dark.git ~/github/shvchk/poly-dark
	sleep 1s
	cd ~/github/shvchk/poly-dark/
	sleep 1s
	chmod +x install.sh
	sleep 1s
	sudo ./install.sh
	sleep 1s

	echo -e "\n${FG_MAGENTA}Finished adding themes from Github.${BG_NoColor}\n"
	sleep 2s
}

function setup_git {
	echo "${FG_CYAN}"
	echo -e "Enter your Github user profile.\n\tYou will need this in order to commit changes to a repo."
	echo "${FG_GREEN}# git config --global user.name${FG_CYAN}"
	read -p "github user name: " githubusername
	echo ""
	echo "${FG_GREEN}# git config --global user.email${FG_CYAN}"
	read -p "github user email: " githubuseremail

	echo -e "${FG_MAGENTA}---"
	git config --global user.name $githubusername
	git config --global user.email $githubuseremail
	#git config --global  core.editor "nano"

	echo -e "\nDone.\n\t# git config --global user.name $githubusername\n\t# git config --global user.email $githubuseremail\n${BG_NoColor}"
	sleep 2s
}

function my_github_repos {
	## my (wip/bonzai tree) Github hosted apps

	mkdir -p ~/github/mezcel/
	mkdir -p ~/gist.github/mezcel/

	#git clone https://github.com/mezcel/electron-container.git ~/github/mezcel/electron-container
	git clone https://github.com/mezcel/printf-time.git ~/github/mezcel/printf-time
	git clone https://github.com/mezcel/jq-tput-terminal.git ~/github/mezcel/jq-tput-terminal
	#igit clone https://github.com/mezcel/carousel-score.git ~/github/mezcel/carousel-score
	git clone https://github.com/mezcel/python-curses.git ~/github/mezcel/python-curses
	git clone https://github.com/mezcel/distro-themes.git ~/github/mezcel/distro-themes
	#git clone https://github.com/mezcel/catechism-scrape.git ~/github/mezcel/catechism-scrape

	## hidden repos
	#git clone https://github.com/mezcel/scrapy-spider.git ~/github/mezcel/scrapy-spider
	#git clone https://github.com/mezcel/adeptus-mechanicus-stc.git ~/github/mezcel/drone-rpg

	## Gists Notes

	git clone https://gist.github.com/eab7764d1f9e67d051fd59ec7ce3e066.git ~/gist.github/mezcel/git-notes
	git clone https://gist.github.com/64db9afd5419e557c0ee53ed935d516e.git ~/gist.github/mezcel/my-screen-gama
	git clone https://gist.github.com/8ac1119e0bb94c581128184d332beee4.git ~/gist.github/mezcel/scrapy-help
	git clone https://gist.github.com/4bd3ec48c0f3d9c27e33fc6943a29903.git ~/gist.github/mezcel/keyboard-trainer
	git clone https://gist.github.com/c90ce696785821d1921f8c2104fb60d3.git ~/gist.github/mezcel/stations

}

function main_apps {
	tput_color_variables

	## Unlock pacman if it was locked
	if [ -f /var/lib/pacman/db.lck ]; then
		sudo rm /var/lib/pacman/db.lck
	fi

	arch_server_pacman

	echo -e "\n\n${FG_MAGENTA}Continue with user specific application ?\n\tEverything from here on is Desktop environment stuff.\n$FG_NoColor"

	read -p "Continue? [y/n]: " -i "n" yn
	case $yn in
		[Yy]* )
			additional_software_prompt
			xorg_desktop_pacman
			github_packages
			my_github_repos
			setup_git

			## Home Directory dot files

			cd $thisDir

			if [ -d $thisDir/home ]; then

				cp -rf home/.config ~/

				[[ -f ~/.bashrc ]]; cp ~/.bashrc ~/.bashrc.backup.$(date +%d%b%Y_%H%M%S)
				cp home/.bashrc ~/

				[[ -f ~/.tmux.conf ]]; cp ~/.tmux.conf ~/.tmux.conf.backup.$(date +%d%b%Y_%H%M%S)
				cp home/.tmux.conf ~/

				[[ -f ~/.vimrc ]]; cp ~/.vimrc ~/.vimrc.backup.$(date +%d%b%Y_%H%M%S)
				cp home/.vimrc ~/

				[[ -f ~/.xinitrc ]]; cp ~/.xinitrc ~/.xinitrc.backup.$(date +%d%b%Y_%H%M%S)
				cp home/.xinitrc ~/

				[[ -f ~/.Xresources ]]; cp ~/.Xresources ~/.Xresources.backup.$(date +%d%b%Y_%H%M%S)
				cp home/.Xresources ~/

				#[[ -f ~/welcome_message.sh ]]; cp ~/welcome_message.sh ~/welcome_message.backup.$(date +%d%b%Y_%H%M%S).sh
				#cp home/welcome_message.sh ~/
			else
				echo -e "\nNo home dir, \"$thisDir/home\",detected\n"

			fi

			ranger --copy-config=all
			;;
		* )
			echo -e "\n${FG_MAGENTA}Home dir (dot) files were not added. $FG_NoColor\n"
			sleep 2s
			;;
	esac
}

main_apps

echo -e "\n${FG_CYAN}Done downloading and installing my main apps.\n FG_NoColor"
sleep 2s
