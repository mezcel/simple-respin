# Broadcom Network Drivers

The official firmware is from here: [broadcom.com](https://www.broadcom.com/support/download-search/?pf=Wireless+LAN+Infrastructure)

But I just grabbed the built in drivers from ```Manjaro```. ```/lib/firmware/b43```

```sh
# Id the networking driver used by the computer

lspci -vnn -d 14e4:
```

> in this case mine was BCM4322/BCM7584M

Get appropriate drivers...

```sh
# Drivers will be placed within: /bin/firmware/

sudo cp -pR ./extra-drivers/b43 /bin/firmware/b43
```

---

## wiki links

[broadcom_wireless#Installation](https://wiki.archlinux.org/index.php/broadcom_wireless#Installation)

[brcm80211](https://wireless.wiki.kernel.org/en/users/Drivers/brcm80211#supported_chips)

[b43](https://wireless.wiki.kernel.org/en/users/drivers/b43)

[broadcom.com](https://www.broadcom.com/support/download-search/?pf=Wireless+LAN+Infrastructure)

[Wireless network configuration](https://wiki.archlinux.org/index.php/Wireless_network_configuration#Installing_driver/firmware)

```sh
# official package refferences

sudo pacman -S --needed b43
sudo pacman -S --needed brsm80211
sudo pacman -S --needed broadcom-wl
sudo pacman -S --needed broadcom-wl-dkms
```