## README

![logo](https://www.archlinux.org/static/logos/archlinux-logo-dark-90dpi.ebdee92a15b3.png)

Note: In late 2019 Archlinux made huge changes to their boot installer ISO. This was not tested for ISOs after Fall 2019. Not sure when changes in Arch will be fully integrated into Arch-based distros.

## Instructions for a new Arch Linux installation.

### Get an Arch

* If this is your first Arch installation, walk away from my repo, and just get a properly maintained distro.
* list of Arch based distros: [link](https://wiki.archlinux.org/index.php/Arch-based_distributions).

* Get a fresh arch linux iso from a regional mirror: [link](https://www.archlinux.org/download/).
	* Use ```1.0-iso-install.sh``` to install arch

* Regardless of how arch was installed, the following will set up your system.

### Install sequence:

1. Arch server ```2.0-define-server.sh```
2. Desktop Environment ```3.0-main-apps.sh```
3. AUR packages ```4.2-AURs.sh``` ***(optional extras)***
4. Bundle my Arch into its own Distro/LiveUsb Iso ***(when you have made your ideal setup)***

---

Note:

You will still need to install you own "office suite" and web browser.
