#!/bin/sh
## This script assumes you have https://github.com/mezcel/arch-openbox.git in your home dir

## Arch Linux Boot from Iso
## Really fast install via Usb, but about 30min via slow internet

thisDirPath=$(dirname $0)

## terminal beautification
function decorativeColors() {

	## Forground Color using ANSI escape

	FG_BLACK=$(tput setaf 0)
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)
	FG_WHITE=$(tput setaf 7)
	FG_NoColor=$(tput sgr0)

	## Background Color using ANSI escape

	BG_BLACK=$(tput setab 0)
	BG_RED=$(tput setab 1)
	BG_GREEN=$(tput setab 2)
	BG_YELLOW=$(tput setab 3)
	BG_BLUE=$(tput setab 4)
	BG_MAGENTA=$(tput setab 5)
	BG_CYAN=$(tput setab 6)
	BG_WHITE=$(tput setab 7)
	BG_NoColor=$(tput sgr0)

	## set mode using ANSI escape

	MODE_BOLD=$(tput bold)
	MODE_DIM=$(tput dim)
	MODE_BEGIN_UNDERLINE=$(tput smul)
	MODE_EXIT_UNDERLINE=$(tput rmul)
	MODE_REVERSE=$(tput rev)
	MODE_ENTER_STANDOUT=$(tput smso)
	MODE_EXIT_STANDOUT=$(tput rmso)

	# clear styles using ANSI escape

	STYLES_OFF=$(tput sgr0)
}

function format_HDD() {
    # del existing partitions
    (echo d; echo 1; echo ""; echo w;) | fdisk /dev/sda
	sleep 2s
    (echo d; echo 2; echo ""; echo w;) | fdisk /dev/sda
	sleep 2s
    (echo d; echo 3; echo ""; echo w;) | fdisk /dev/sda
	sleep 2s
    (echo d; echo 4; echo ""; echo w;) | fdisk /dev/sda
	sleep 2s
    # make new partitions
    (echo o; echo n; echo p; echo 2; echo ""; echo +2G; echo w; echo "";) | fdisk /dev/sda
	sleep 2s
    (echo n; echo p; echo 1; echo ""; echo ""; echo w; echo "";) | fdisk /dev/sda
	sleep 2s
    (echo y;) | mkfs.ext4 /dev/sda1
	sleep 2s
    mkswap /dev/sda2
	sleep 2s
    swapon /dev/sda2
	sleep 2s
    # Mount
    # mkdir -p /mnt
    mount /dev/sda1 /mnt
	sleep 2s
}

function packstrap_from_iso() {
	thisDirPath=$(dirname $0)
    ## my regional mirrorlist for pacman

	mirrorlist_backup="/etc/pacman.d/mirrorlist"
    if [ -f "$mirrorlist_backup" ]
    then cp "$mirrorlist_backup" "$mirrorlist_backup.backup.$(date +%H%M%S)"
    fi
	sleep 1s

	## get the only repo for arch32
	## https://www.archlinux32.org/mirrorlist/
	#mymirror=$(grep princeton /etc/pacman.d/mirrorlist)
	#sleep 1s
	#echo $mymirror > /etc/pacman.d/mirrorlist
	echo 'Server = https://mirror.math.princeton.edu/pub/archlinux32/$arch/$repo/' > /etc/pacman.d/mirrorlist

	## Customize my Skel
    ## copy my current home to skel
    # cp -r ~/* /etc/skel
	# cp -r ~/.* /etc/skel

    ## Usb merge resource from usb
    time cp -ax / /mnt
	sleep 2s
    #cp -vaT /run/archiso/bootmnt/arch/boot/x86_64/vmlinuz /mnt/boot/vmlinuz-linux
    cp -vaT /run/archiso/bootmnt/arch/boot/i6*/vmlinuz /mnt/boot/vmlinuz-linux
	sleep 2s
    # pacman -Sy
    ## Install the base packages
    # pacstrap /mnt base base-devel
    ## Generate an fstab
    genfstab -p /mnt >> /mnt/etc/fstab
	sleep 1s
}

function chroot_localization() {
	# Chroot into your newly installed system
	# remove a lot of things that were imported from usb iso
	(
		# Localization
		echo timedatectl set-ntp true;
		echo ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime;
		echo "echo en_US.UTF-8 UTF-8 >> /etc/locale.gen";
		echo locale-gen;
		echo hwclock --systohc;
		echo "echo LANG=en_US.UTF-8 >> /etc/locale.conf";
		# Restore the configuration of journald
		echo sed -i 's/Storage=volatile/#Storage=auto/' /etc/systemd/journald.conf;
		# Remove special udev rule
		echo rm /etc/udev/rules.d//81-dhcpcd.rules;
		# Disable and remove the services created by archiso
		echo systemctl disable pacman-init.service choose-mirror.service;
		echo rm /etc/systemd/system/choose-mirror.service;
		echo rm /etc/systemd/system/pacman-init.service;
		echo rm /etc/systemd/system/etc-pacman.d-gnupg.mount;
		echo rm -rf /etc/systemd/system/getty@tty1.service.d;
		echo rm /etc/systemd/scripts/choose-mirror;
		# Remove special scripts of the Live environment
		echo rm /etc/systemd/system/getty@tty1.service.d/autologin.conf;
		echo rm /root/.automated_script.sh;
		echo rm /root/.zlogin;
		echo rm /etc/mkinitcpio-archiso.conf;
		echo rm -r /etc/initcpio/;
		# Import archlinux keys
		echo pacman-key --init;
		echo pacman-key --populate archlinux;
		echo sleep 5s;
		# Initramfs
		echo mkinitcpio -p linux;
		echo sleep 10s;
		echo exit;
	) | arch-chroot /mnt /bin/bash
}

function chroot_biosGrub() {
	## GRUB Stuff
	## Install Grub Bootloader
	(
		## Install Grub Bootloader
		echo pacman -S --needed grub;
		echo grub-install --target=i386-pc --recheck --force  /dev/sda;
		echo sleep 2s;
		echo cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo;
		echo grub-mkconfig -o /boot/grub/grub.cfg;
		echo sleep 2s;
		echo exit;
	) | arch-chroot /mnt /bin/bash

}

function biosgrub() {
	timedatectl set-ntp true

	## Partition Destination Drive
	echo "$FG_YELLOW
	About to prepare destination drive...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	format_HDD

	## Packstrap and cutomization
	echo "$FG_YELLOW
	About to prepare packstrap...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	packstrap_from_iso

	echo "$FG_YELLOW
	About to prepare chroot customizations...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	chroot_localization

	## GRUB STUFF
	echo "$FG_YELLOW
	About to prepare grub...
	$FG_NoColor"
	sleep 7s # Waits 7 seconds.
	chroot_biosGrub
}

function closeOut() {
	## Done
	umount -R /mnt

	sleep 5s # Waits 5 seconds.
	echo "$FG_YELLOW
	Just a 5 sec delay to allow unmounting to settle...
	"
	sleep 5s
	# shutdown now
	echo "$FG_YELLOW
	Done. Rebbot your system.
	"

	reboot
}

## Start the DIY installer

decorativeColors
biosgrub
closeOut
	
	