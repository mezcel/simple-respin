#!/bin/bash

thisDirPath=$(dirname $0)

function dlAurs {
	aurPackage=$1
	git clone https://aur.archlinux.org/$aurPackage.git ~/AUR/$aurPackage
	sleep 1s
}

function compileMyAUR {
	aurPackage=$1
	dlAurs $aurPackage

	cd ~/AUR/$aurPackage
	sleep 1s
	## install dependencies, install, clean after build, no confirmation prompts
	makepkg -sic --noconfirm
	sleep 1s
}

function dl_utils_aur {
	isCompileUtils=$1

	aurPackages=( "python-gtkspellcheck" "remarkable" "inxi" "dia2code" "xrdp" "newaita-icons-git" "wordgrinder" "cava" )

	for i in "${aurPackages[@]}"
	do
		aurPackage=$i
		if [ $isCompileUtils -eq 0 ]; then
			dlAurs $aurPackage
		else
			compileMyAUR $aurPackage
		fi
		sleep 3s
	done
	sleep 1s
}

function dl_themes_aur {
	isCompileThemes=$1

	aurPackages=("google-chrome")

	for i in "${aurPackages[@]}"
	do
		aurPackage=$i
		if [ $isCompileUtils -eq 0 ]; then
			dlAurs $aurPackage
		else
			compileMyAUR $aurPackage
		fi
		sleep 3s
	done
	sleep 1s
}

function getAurs {
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_NoColor=$(tput sgr0)

	sudo pacman -S --noconfirm --needed base
	sudo pacman -S --noconfirm --needed base-devel

	echo "$FG_GREEN"
	echo "## AURs"
	echo "#######"
	echo ""
	echo "Download and compile, or just download AUR for later installation?"
	read -t 10 -p "	Download and compile? [y/N]" -i "n" yn
	echo "$FG_NoColor"

	if [ $EUID -eq 0 ]; then
		yn="n"
		isCompile=0
		echo "$FG_YELLOW"
		echo "Since you are in ROOT, AURs will not be installed, but instead, AURs will be Downloaded and copied to skel"
		echo "$FG_NoColor"
		sleep 5s
	fi


	if [ $yn == 'y' ]; then
		isCompile=1
		echo "$FG_YELLOW"
		echo "Downloading and compiling"
		echo "$FG_NoColor"
		sleep 3s
	else
		isCompile=0
		echo "$FG_YELLOW"
		echo "Just downloading and NOT compiling"
		echo "$FG_NoColor"
		sleep 3s
	fi

	dl_utils_aur $isCompile
	dl_themes_aur $isCompile
}

if [ -f /var/lib/pacman/db.lck ]; then
	sudo rm /var/lib/pacman/db.lck
fi

mkdir -p ~/AUR/

getAurs
