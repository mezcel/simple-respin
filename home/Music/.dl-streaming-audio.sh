#!/bin/bash

mkdir -p ~/Music/streaming_audio/SomaFM
mkdir -p ~/Music/streaming_audio/SomaFM/32k/
mkdir -p ~/Music/streaming_audio/TheBlastFM/
mkdir -p ~/Music/streaming_audio/TheBlastFM/32k/

mkdir -p ~/Music/streaming_audio/Icecast/EWTN

##cd ~/Music/streaming_audio/
sleep 1s

## Somafm Streaming Radio 32k AAC
wget https://somafm.com/groovesalad32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/u80s32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/missioncontrol32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/folkfwd32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/fluid32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/poptron32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/sf103332.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/cliqhop32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/thistle32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/indiepop32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/bootliquor32.pls -P ~/Music/streaming_audio/SomaFM/32k/
wget https://somafm.com/sonicuniverse32.pls -P ~/Music/streaming_audio/SomaFM/32k/

## Somafm Streaming Radio 128k AAC
wget https://somafm.com/groovesalad130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/u80s130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/missioncontrol130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/folkfwd130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/fluid130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/poptron130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/sf1033130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/cliqhop130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/thistle130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/indiepop130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/bootliquor130.pls -P ~/Music/streaming_audio/SomaFM/
wget https://somafm.com/sonicuniverse130.pls -P ~/Music/streaming_audio/SomaFM/

## Icecast.org

## EWTN Live Radio
wget http://ewtn-ice.streamguys1.com/classics-aac.m3u -P ~/Music/streaming_audio/Icecast/EWTN/
wget http://ewtn-ice.streamguys1.com/english-aac.m3u -P ~/Music/streaming_audio/Icecast/EWTN/

echo '[playlist]
File1=http://ewtn-ice.streamguys1.com:80/english-aac
Title1=EWTN Podcast Radio
NumberOfEntries=1' > ~/Music/streaming_audio/Icecast/EWTN/ewtn.pls


## TheBlast.FM
echo '[playlist]
File1=http://theimplosion-stream.theblast.fm.fast-serv.com:80/128
Title1=TheBlast.FM
NumberOfEntries=1' > ~/Music/streaming_audio/TheBlastFM/theimplosion.pls

echo '[playlist]
File1=http://theimplosion-stream.theblast.fm.fast-serv.com:80/32
Title1=TheBlast.FM
NumberOfEntries=1' > ~/Music/streaming_audio/TheBlastFM/32k/theimplosion32.pls


## Done Display Message

echo -e "\nDone.\n\n"
echo -e "Streaming playlist ( .pls and .m3u ) links were stored in:  ~/Music/streaming_audio/"
echo -e "
Usage:
    I use mplayer or nvlc audio medial players:
    Mplayer:    mplayer -playlist ~/Music/streaming_audio/SomaFM/fluid130.pls
    Mplayer:    mplayer -playlist http://ewtn-ice.streamguys1.com:80/english-aac
    Vlc:        nvlc ~/Music/streaming_audio/SomaFM/fluid130.pls
"
