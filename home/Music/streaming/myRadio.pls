[playlist]

NumberOfEntries=14

## Christian Metal ##
File1=http://theimplosion-stream.theblast.fm.fast-serv.com:80/128
Title1=TheBlast.FM - The Implosion
Length1=-1

## Christian Hiphop ##
File2=http://62.210.24.124:8811/stream
Title2=(#1 - 7/250) Kingdomgrindradio
Length2=-1

## Christian Talk Radio ##
File3=http://ewtn-ice.streamguys1.com:80/english-aac
Title3=EWTN Podcast Radio
Length3=-1

## Liquid Trap ##
File4=http://ice1.somafm.com/fluid-128-aac
Title4=SomaFM: Fluid (#6): Drown in the electronic sound of instrumental hiphop, future soul and liquid trap.
Length4=-1

## Synthwave ##
File5=http://139.162.14.151:9090/160mp3
Title5=(#1 - 42/400) Synthwave Retrowave Radio
Length5=-1

## DnB ##
File6=http://5.39.71.159:8625/stream
Title6=(#1 - 5/9999) Futuredrumz.com Jungle Drum & Bass Radio
Length6=-1

File7=http://uk1.internet-radio.com:8118/listen.pls
Title7=undergroundradio.com
Length7=-1

File8=http://stream.drumandbass.fm:9002
Title8=drumandbass.fm
Length8=-1

## Somafm ##
File9=http://ice4.somafm.com/sf1033-128-aac
Title9=SomaFM: SF 10-33 (#1): Ambient music mixed with the sounds of San Francisco public safety radio traffic.
Length9=-1

File10=http://ice4.somafm.com/cliqhop-128-aac
Title10=SomaFM: cliqhop idm (#1): Blips'n'beeps backed mostly w/beats. Intelligent Dance Music.
Length10=-1

File11=http://ice4.somafm.com/poptron-128-aac
Title11=SomaFM: PopTron (#1): Electropop and indie dance rock with sparkle and pop.
Length11=-1

File12=http://ice4.somafm.com/folkfwd-128-aac
Title12=SomaFM: Folk Forward (#1): Indie Folk, Alt-folk and the occasional folk classics.
Length12=-1

File13=http://ice4.somafm.com/missioncontrol-128-aac
Title13=SomaFM: Mission Control (#1): Celebrating NASA and Space Explorers everywhere.
Length13=-1

File14=http://ice4.somafm.com/groovesalad-128-aac
Title14=SomaFM: Groove Salad (#1): A nicely chilled plate of ambient/downtempo beats and grooves.
Length14=-1

Version=2
