#!/bin/bash

## source code: https://github.com/mezcel/distro-themes/blob/master/my_iwconfig.sh
## Reqires the net-tools .deb package if on Debian

function greeting {
	echo ""
	echo "########################################################################"
	echo "##                                                                    ##"
	echo "## Manually connect to the wifi of a known and trusted wifi router    ##"
	echo "##                                                                    ##"
	echo "## source code:                                                       ##"
	echo "## https://github.com/mezcel/distro-themes/blob/master/my_iwconfig.sh ##"
	echo "##                                                                    ##"
	echo "########################################################################"
	echo ""
}

function driver_information {
	echo -e "## Network Drivers\nlspci:"
	lspci -vnn | grep Network
	lspci -vnn | grep Ethernet
	echo ""
}

function tput_color_variables {
	FG_RED=$(tput setaf 1)
	FG_GREEN=$(tput setaf 2)
	FG_YELLOW=$(tput setaf 3)
	FG_BLUE=$(tput setaf 4)
	FG_MAGENTA=$(tput setaf 5)
	FG_CYAN=$(tput setaf 6)

	MODE_BOLD=$(tput bold)
	MODE_BEGIN_UNDERLINE=$(tput smul)
	MODE_EXIT_UNDERLINE=$(tput rmul)

	BG_NoColor=$(tput sgr0)
	FG_NoColor=$(tput sgr0)
}

function turn_on_wifi {
	if [ -d /etc/apk ]; then
		echo -e "${FG_CYAN}(step 1 of 4)${FG_NoColor}\tDetect Wifi Hardware Interface:\t\t${MODE_BOLD}$myInterface${FG_NoColor}\n"
		sleep 2s

		ip link

		echo -e "\n---\n"
		read -e -p "Type in the wifi interface [wlan0]: " -i "wlan0" myInterface

		## turn on wifi hardware
		echo -e "${FG_CYAN}(step 2 of 4)${FG_NoColor}\tSwitch ON the wifi hardware ...\t\t${FG_GREEN}processing ...${FG_NoColor}\n"

    	ip link set $myInterface up
	else
		## read the laptop's available wlan interface name
		myInterface=$(sudo iw dev | grep Interface | awk '{print $2}')

		echo -e "${FG_CYAN}(step 1 of 4)${FG_NoColor}\tDetect Wifi Hardware Interface:\t\t${MODE_BOLD}$myInterface${FG_NoColor}\n"
		sleep 2s

		## turn on wifi hardware
		echo -e "${FG_CYAN}(step 2 of 4)${FG_NoColor}\tSwitch ON the wifi hardware ...\t\t${FG_GREEN}processing ...${FG_NoColor}\n"

		command -v ifconfig &>/dev/null
		if [ $? -eq 0 ]; then
			echo "ifconfig $myInterface up ..."
			sudo ifconfig $myInterface up
			sleep 2s
		else
			## Debian 10 default
			echo "ip link set $myInterface up ..."
			sudo ip link set $myInterface up
			sleep 1s
			#sudo ifup $myInterface ## used for eth0
			#sleep 1s
		fi
	fi

	sleep 2s
}

function select_ssid {
	
	if [ -d /etc/apk ]; then
		## list available SSID's
		echo -e "${FG_CYAN}(step 3 of 4)${FG_NoColor}\tIdentify available wifi SSIDs ...\t${FG_GREEN}processing ...${FG_NoColor}\n"
    	
		isSudo=$(command -v sudo)
		if [ $isSudo == "/usr/bin/sudo" ]; then
			sudo iwlist $myInterface scanning | grep SSID | awk '{print $1}'
    	else
			iwlist $myInterface scanning | grep SSID | awk '{print $1}'
		fi

		echo -e "\n---\n Type in the SSID to connect to:"
		
		read -p 'SSID Name: ' myssid
		read -sp 'SSID password: ' mypw
		echo ""

	else
		## list available SSID's
		echo -e "${FG_CYAN}(step 3 of 4)${FG_NoColor}\tIdentify available wifi SSIDs ...\t${FG_GREEN}processing ...${FG_NoColor}\n"

		ssidArr=($(ps -u | sudo iw dev $myInterface scan | grep SSID | awk '{print $2}'))
		sleep 2s

		echo -e "\t\t${MODE_BEGIN_UNDERLINE}List of available SSID's:${MODE_EXIT_UNDERLINE}"
		count=0
		for i in "${ssidArr[@]}"
		do
			:
			echo -e "\t\t $count.\t$i"
			count=$((count+1))
		done

		## Select which SSID to connect to
		echo -e "\n${FG_CYAN}(step 4 of 4)${FG_NoColor}\tSelecting an SSID ...\t\t\t${FG_GREEN}user input ...${FG_NoColor}\n"
		promptTab=$(echo -e "\t\t")
		read -p "${promptTab}Enter the number representing the desired SSID? [0 - $(( ${#ssidArr[@]} - 1 ))]: " ssidNo

		echo ""
		read -e -p "${promptTab}Connect to ${MODE_BOLD}${ssidArr[$ssidNo]}${FG_NoColor} ? [y/N]: " -i "n" yn

		if [ $yn == 'y' ]; then
			myssid=${ssidArr[$ssidNo]}
		else
			## abort script
			echo "aborted script. done."
			exit
		fi
	fi
}

function set_ethernet_connection {
	echo -e "\nSet ethernet port to atomatically connect to a network\n"
	myEthernetInterface=$(ls /sys/class/net/ | grep -E enp)

	#sudo ifdown $myEthernetInterface
	
	if [ -f /etc/network/interface ]; then
		sudo cp /etc/network/interface /etc/network/interface.backup.$(date +%d%b%Y_%H%M%S)
	fi
	sudo echo 'source /etc/network/interfaces.d/*' > /etc/network/interface
	sudo echo "auto lo $myEthernetInterface" >> /etc/network/interface
	sudo echo 'iface lo inet loopback' >> /etc/network/interface

	#sudo ifup $myEthernetInterface

	sleep 2s
}

function define_wpa_credentials {
	if [ -d /etc/apk ]; then
		iwconfig $myInterface essid $myssd
		iwconfig $myInterface

    	wpa_passphrase $myssd $mypw > /etc/wpa_supplicant/
    	wpa_supplicant -B -i $myInterface -c /etc/wpa_supplicant/
	else
		## define ssid permission
		echo ""
		read -e -p "${promptTab}Enter SSID: [ $myssid ]: " -i "$myssid" myssid
		read -p "${promptTab}Enter Wifi password: " mypw
		echo ""

		## mask a config file
		sudo wpa_passphrase $myssid $mypw > /etc/wpa_supplicant/wpa_supplicant.conf
		sleep 1s

		## associate config file with computer
		wifidriver=wext
		sudo wpa_supplicant -B -i $myInterface -c /etc/wpa_supplicant/wpa_supplicant.conf -D $wifidriver
		sleep 1s
	fi
}

function connect_to_network {
	## Do the do
	
	if [ -d /etc/apk ]; then
		sudo ip link set $myInterface up
		sudo /etc/init.d/wpa_supplicant start
	else
		sudo dhclient $myInterface
		sleep 1s

		command -v iwconfig &>/dev/null
		if [ $? -eq 0 ]; then
			echo "sudo iwconfig $myInterface essid $myssid ..."
			sudo iwconfig $myInterface essid $myssid
			sleep 2s
		fi
		#sleep 1s

		## echo verify connectivity
		sudo iw dev $myInterface link

		command -v ping &>/dev/null
		if [ $? -eq 0 ]; then
			sudo ping -c3 google.com
		fi
	fi
}

function package_manager_remider {
	echo -e "\n### Package Manager Repository Lists ###\n"
	echo -e "Debian:\n\tDependancy: net-tools \n\tUpdate \"/etc/apt/sources.list\" as necessary.\n\tExample: echo \"deb http://ftp.us.debian.org/debian/ buster main contrib non-free\" >> /etc/apt/sources.list\n\tExample: echo \"deb-src http://ftp.us.debian.org/debian/ buster main contrib non-free\" >> /etc/apt/sources.list"
	echo -e "Archlinux:\n\tUpdate \"/etc/pacman.d/mirrorlist\" and/or \"/etc/pacman.conf\" as necessary."
	echo ""
}

function make_launcher_script {

	## launcher wifi script file
	laucherSriptName=launch_$myInterface_$myssid

	echo -e "\n${FG_CYAN}(Finishing)${FG_NoColor}\tMaking a wifi launcher script ...\t${FG_GREEN}~/$laucherSriptName.sh ${FG_NoColor}\n"

	echo '#!/bin/bash' > ~/$laucherSriptName.sh
	echo '## Log into a known SSID' >> ~/$laucherSriptName.sh
	echo '## launch dhclient with a predefined wpa_supplicant' >> ~/$laucherSriptName.sh
	echo '##	/etc/wpa_supplicant/wpa_supplicant.conf' >> ~/$laucherSriptName.sh
	echo 'echo -e "\n##\n## Manually log on/off to/from a saved SSID connection..\n##"' >> ~/$laucherSriptName.sh

	echo 'inputFlag=$1' >> ~/$laucherSriptName.sh
	echo 'thisFile=`basename $0`' >> ~/$laucherSriptName.sh

	echo 'function tryAgain {' >> ~/$laucherSriptName.sh
	echo '	echo -e "\n## !!! INPUT ERROR !!!\n##\n## You entered: $inputFlag\tTry again.\n##"' >> ~/$laucherSriptName.sh
	echo '	echo -e "## Type: [ \"up\" (go online) or \"down\" (go offline) or \"restart\" (go offline and then online) ], after the executable file name.\n##\tExample: ./$thisFile up\n##\tExample: ./$thisFile down\n##\tExample: ./$thisFile restart\n##"' >> ~/$laucherSriptName.sh
	echo '	exit' >> ~/$laucherSriptName.sh
	echo '}' >> ~/$laucherSriptName.sh

	echo 'function wifiON {' >> ~/$laucherSriptName.sh
	echo '	echo -e "\n##\n## Log into a known SSID\n## launch dhclient with a predefined wpa_supplicant\n##	/etc/wpa_supplicant/wpa_supplicant.conf\n##"' >> ~/$laucherSriptName.sh
	echo "	echo -e \"\n# activating $myInterface ...\"" >> ~/$laucherSriptName.sh

	if [ -d /etc/apk ]; then

		echo "	echo -e \"\n# ip link set $myInterface up \n\"" >> ~/$laucherSriptName.sh
		echo "	sudo ip link set $myInterface up" >> ~/$laucherSriptName.sh
		#echo "	sudo wpa_supplicant -B -i $wifiInterface -c /etc/wpa_supplicant/wpa_supplicant.conf" >> ~/$laucherSriptName.sh
		echo '	#sleep 5s' >> ~/$laucherSriptName.sh

	else

		echo '	command -v ifconfig &>/dev/null' >> ~/$laucherSriptName.sh
		echo '	if [ $? -eq 0 ]; then' >> ~/$laucherSriptName.sh
		echo "		echo \"ifconfig $myInterface up ...\"" >> ~/$laucherSriptName.sh
		echo "		sudo ifconfig $myInterface up" >> ~/$laucherSriptName.sh
		echo '		sleep 2s' >> ~/$laucherSriptName.sh
		echo '	else' >> ~/$laucherSriptName.sh
		echo '		echo -e "## Debian 10 default"' >> ~/$laucherSriptName.sh
		echo "		echo -e \"\n# ip link set $myInterface up \n\"" >> ~/$laucherSriptName.sh
		echo "		sudo ip link set $myInterface up" >> ~/$laucherSriptName.sh
		echo '		#sleep 1s' >> ~/$laucherSriptName.sh
		echo '	fi' >> ~/$laucherSriptName.sh

	fi

	echo '	echo -e "\n# perform wpa_supplicant ..."' >> ~/$laucherSriptName.sh
	echo "	sudo wpa_supplicant -B -c /etc/wpa_supplicant/wpa_supplicant.conf -i $myInterface" >> ~/$laucherSriptName.sh
	echo '	sleep 1s' >> ~/$laucherSriptName.sh

	if [ -d /etc/apk ]; then
		## Alpine linux
		echo "	sudo /etc/init.d/wpa_supplicant start" >> ~/$laucherSriptName.sh
	else
		echo "	echo -e \"enable dhclient with $myInterface ...\"" >> ~/$laucherSriptName.sh
		echo '	echo ""'>> ~/$laucherSriptName.sh
		echo "	sudo dhclient $myInterface" >> ~/$laucherSriptName.sh
	fi
	
	echo '	sleep 2s' >> ~/$laucherSriptName.sh
	echo '	ping -c3 google.com' >> ~/$laucherSriptName.sh

	echo '	if [ $? -ne 0 ]; then' >> ~/$laucherSriptName.sh
	echo '		echo -e "##\n## No internet connection is available."' >> ~/$laucherSriptName.sh
	echo '	else' >> ~/$laucherSriptName.sh
	echo '		echo -e "\n##\n## You are now connected to wifi. $(date)"' >> ~/$laucherSriptName.sh
	echo "		## iw dev $myInterface station dump -v" >> ~/$laucherSriptName.sh
	echo "		sudo iwconfig $myInterface | grep -i --color signal" >> ~/$laucherSriptName.sh
	echo '	fi' >> ~/$laucherSriptName.sh
	
	echo '}' >> ~/$laucherSriptName.sh

	echo 'function wifiOFF {' >> ~/$laucherSriptName.sh
	
	if [ -d /etc/apk ]; then
		## Alpine linux
		echo '	echo "/etc/init.d/wpa_supplicant stop ..."' >> ~/$laucherSriptName.sh
		echo "	sudo /etc/init.d/wpa_supplicant stop" >> ~/$laucherSriptName.sh
		echo "	echo \"ip link set $myInterface down ...\"" >> ~/$laucherSriptName.sh
		echo "	sudo ip link set $myInterface down" >> ~/$laucherSriptName.sh
	else
		echo '	echo "killall wpa_supplicant ..."' >> ~/$laucherSriptName.sh
		echo '	sudo killall wpa_supplicant' >> ~/$laucherSriptName.sh
		echo '	sleep 2s' >> ~/$laucherSriptName.sh

		echo '	command -v ifconfig &>/dev/null' >> ~/$laucherSriptName.sh
		echo '	if [ $? -eq 0 ]; then' >> ~/$laucherSriptName.sh
		echo "		echo \"ifconfig $myInterface down ...\"" >> ~/$laucherSriptName.sh
		echo "		sudo ifconfig $myInterface down" >> ~/$laucherSriptName.sh
		echo '		sleep 2s' >> ~/$laucherSriptName.sh
		echo '	else' >> ~/$laucherSriptName.sh
		echo '		echo "## Debian 10 default"' >> ~/$laucherSriptName.sh
		echo "		echo \"ip link set $myInterface down ...\"" >> ~/$laucherSriptName.sh
		echo "		sudo ip link set $myInterface down" >> ~/$laucherSriptName.sh
		echo '		sleep 1s' >> ~/$laucherSriptName.sh
		echo '	fi' >> ~/$laucherSriptName.sh
	fi

	echo '	sleep 2s' >> ~/$laucherSriptName.sh
	echo '	echo -e "\n## Disconnected from wifi\n## wifi is off\n##\n"' >> ~/$laucherSriptName.sh
	echo '}' >> ~/$laucherSriptName.sh

	echo 'function wifiRESTART {' >> ~/$laucherSriptName.sh
	echo '	wifiOFF' >> ~/$laucherSriptName.sh
	echo '	wifiON' >> ~/$laucherSriptName.sh
	echo '}' >> ~/$laucherSriptName.sh

	echo 'if [ -z $inputFlag ]; then' >> ~/$laucherSriptName.sh
	echo '	inputFlag="nothing" ## unset var' >> ~/$laucherSriptName.sh
	echo '	tryAgain' >> ~/$laucherSriptName.sh
	echo 'fi' >> ~/$laucherSriptName.sh
	echo 'case $inputFlag in' >> ~/$laucherSriptName.sh
	echo '	"up" )' >> ~/$laucherSriptName.sh
	echo '		wifiON' >> ~/$laucherSriptName.sh
	echo '		;;' >> ~/$laucherSriptName.sh
	echo '	"down" )' >> ~/$laucherSriptName.sh
	echo '		wifiOFF' >> ~/$laucherSriptName.sh
	echo '		;;' >> ~/$laucherSriptName.sh
	echo '	"restart" )' >> ~/$laucherSriptName.sh
	echo '		wifiRESTART' >> ~/$laucherSriptName.sh
	echo '		;;' >> ~/$laucherSriptName.sh
	echo '	* )' >> ~/$laucherSriptName.sh
	echo '		## wrong input' >> ~/$laucherSriptName.sh
	echo '		tryAgain' >> ~/$laucherSriptName.sh
	echo '		;;' >> ~/$laucherSriptName.sh
	echo 'esac' >> ~/$laucherSriptName.sh

	echo 'echo "done."' >> ~/$laucherSriptName.sh

	chmod +x ~/$laucherSriptName.sh
}

################################################################################
## Main
################################################################################

function main {
	tput_color_variables
	greeting

	me=$(whoami)

	## show network divers
	driver_information

	## Confirmation check
	if [ -d /etc/pacman.d ]; then
		
		## Archlinux
		echo -e "\nArchlinux has \"netctl\" as wifi connection tool.\n\tIt can be run using :\"sudo wifi-menu\""
		read -e -p "Continue with this script? [y/n]: " -i "n" yn
		echo ""
		
		if [ $yn == "n" ]; then exit;fi

	elif [ -f /etc/apt/sources.list ]; then

		## Debian
		isWLS=$(uname -r | grep Microsoft &> /dev/null; echo $?)

		if [ $isWLS -eq 0 ]; then
			echo -e "\nWLS gets its wifi from Windows 10\n"
			exit
		fi
	fi

	## wifi and ssid
	turn_on_wifi
	select_ssid

	## wpa_supplicant configs
	if [ $me == "root" ]; then
		define_wpa_credentials
		connect_to_network
		package_manager_remider
	fi

	## quick launch script
	make_launcher_script

	## debian ethernet
	if [ -f /etc/apt/sources.list ]; then set_ethernet_connection; fi
}


################################################################################
## Run
################################################################################

main
