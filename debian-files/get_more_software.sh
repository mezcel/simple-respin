#!/bin/bash

function deb_packages {
    sudo apt update

    sudo apt install -y git
    sudo apt install -y build-essential
    sudo apt install -y gcc
    sudo apt install -y gdb
    sudo apt install -y net-tools
    sudo apt install -y wget
    sudo apt install -y curl
    #sudo apt install -y wicd
    #sudo apt install -y wicd-cli
    #sudo apt install -y wicd-curses
    sudo apt install -y w3c
    sudo apt install -y bmon
    sudo apt install -y wavemon
    sudo apt install -y nmon
    #sudo apt install -y collectl
    #sudo apt install -y sysstat
    sudo apt install -y elinks
    sudo apt install -y nano
    sudo apt install -y vim
    sudo apt install -y tmux
    sudo apt install -y tmux-plugins
    sudo apt install -y ranger
    sudo apt install -y pv
    #sudo apt install -y mc
    sudo apt install -y sc
    sudo apt install -y tpp
    sudo apt install -y alsa-utils
    sudo apt install -y pavucontrol
    #sudo apt install -y mplayer cmus
    sudo apt install -y vlc

    #sudo apt install -y xorg xterm
    sudo apt install -y xinit
    sudo apt install -y twm-xorg
    sudo apt install -y xterm
    sudo apt install -y openbox
    sudo apt install -y obconf
    sudo apt install -y lxappearance
    sudo apt install -y arc-theme

    sudo apt install -y geany
    sudo apt install -y geany-plugins
    sudo apt install -y mousepad
    sudo apt install -y aspell
    #sudo apt install -y libaspell-dev
    sudo apt install -y bc
    sudo apt install -y retext
    sudo apt install -y pandoc
    sudo apt install -y groff
    sudo apt install -y zathura

    sudo apt install -y pcmanfm
    sudo apt install -y libfm-tools
    sudo apt install -y libusbmuxd-tools
    #sudo apt install -y thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin
    sudo apt install -y evince
    sudo apt install -y udiskie
    sudo apt install -y xarchiver
    sudo apt install -y gvfs
    sudo apt install -y tumbler
    sudo apt install -y ffmpegthumbnailer

    sudo apt install -y vim-gtk
    sudo apt install -y xsel
    sudo apt install -y acpi
    sudo apt install -y tlp
    sudo apt install -y feh
    #sudo apt install -y tty-clock
    #sudo apt install -y abiword
    sudo apt install -y gnumeric
    #sudo apt install -y libreoffice-writer
    #sudo apt install -y libreoffice-calc

    sudo apt install -y gimp
    #sudo apt install -y exiv2
    #sudo apt install -y gimp-data-extras
    #sudo apt install -y gimp-help-en

    sudo apt install -y htop
    #sudo apt install -y iotop

    ## dwm prereq.
    sudo apt install -y libx11-dev
    sudo apt install -y libxft-dev
    sudo apt install -y libxinerama-dev
    sudo apt install -y xclip
    sudo apt install -y xvkbd
    sudo apt install -y libgcr-3-dev

    sudo apt install -y suckless-tools
    sudo apt install -y dmenu
    sudo apt install -y rofi

    ## large software
    sudo apt install -y iceweasel
    sudo apt install -y gimp
    sudo apt install -y inkscape
    sudo apt install libreoffice

    ## just in case
    sudo apt remove network-manager
}

function setup_git {
    echo -e "\n\nPreparing Git got Github:\n\n"
    read -p "Enter your github user name: " githubusername
    echo ""
    read -p "Enter github user email: " githubuseremail

    git config --global user.name $githubusername
    git config --global user.email $githubuseremail
    echo ""
}

function set_user_permission {
    me=$(whoami)
    if [ $me == "root" ]; then
        echo ""
        read -p "Give a user (sudo/sudoer) privalages? [y/n] " yn
        if [ $yn == "y" ]; then
            read -p "Enter a user name: " myname
            adduser $myname sudo
            usermod -aG sudo $myname

            echo ""
            read -e -p "Do you want to terminate download/installation and log into that user profile? [y/N] " -i "n" yesno
            if [ $yesno == "y" ]; then
                echo -e "\nThis script will terminate.\nLog out and log into the desired user account.\n"
                read -p "[Press any key to continue.]" readVar
                exit
            fi

        fi
    fi
}

#set_user_permission
deb_packages
