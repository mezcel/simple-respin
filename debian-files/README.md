## README

![logo](https://www.debian.org/logos/openlogo-75.png)

Intended for Debian, but this works for any Debian based distro.

### Iso
* Debian Iso: [link](https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/)
* Debian Derivatives: [link](https://www.debian.org/derivatives/), [MXLinux](https://mxlinux.org/)

### Installer scripts
```sh
bash get_more_software.sh
```.
