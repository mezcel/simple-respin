#!/bin/bash

## Use a wired lan to install broadcom wifi driver

function Step1 {
    lspci -nn | grep Network
}

function Step2 {
    sudo apt-get install wireless-tools
    sudo apt-get install network-manager-gnome
    sudo apt-get install wpasupplicant
}

function Step3 {
    sudo nano /etc/apt/sources.list
    (main contrib non-free)
}

function Step4 {
    sudo apt-get update

    sudo apt-get dist-upgrade

    reboot the computer
}

function Step5 {
    sudo apt-get install linux-image-$(uname -r|sed 's,[^-]*-[^-]*-,,')

    sudo apt-get install linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,') 

    sudo apt-get install broadcom-sta-dkms

}

function Step6 {
    sudo modprobe -r b44 b43 b43legacy ssb brcmsmac bcma

    sudo modprobe wl
}