<!-- pandoc formatting
title: 'distro-themes'
subtitle: 'A suckless desktop environment for Archlinux and Debian'
author: 'Mezcel'
date: 'Jan 1, 2019'
description: 'Installer scripts and configuration scripts for a decorative yet minimal terminal and keyboard based desktop workstation.'
lang: en-US
-->

# simple-respin

<a href="https://suckless.org/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Suckless_logo.svg/220px-Suckless_logo.svg.png" height="30"></a>
<a href="https://dwm.suckless.org/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/c/c7/Dwm-logo.png" height="35"></a>
<a href="https://github.com/systemd/systemd" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Systemd-logo.svg/288px-Systemd-logo.svg.png" height="35"></a>
<a href="https://www.busybox.net/" target="_blank"><img src="https://www.busybox.net/images/busybox1.png" height="35"></a>
<a href="https://www.debian.org/" target="_blank"><img src="https://www.debian.org/Pics/openlogo-50.png" height="38"></a>
<a href="https://www.archlinux.org/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Arch_Linux_logo.svg/200px-Arch_Linux_logo.svg.png" height="35"></a>
<a href="https://www.alpinelinux.org/about/" target="_blank"><img src="https://alpinelinux.org/alpinelinux-logo.svg" height="30"></a>
<a href="https://docs.microsoft.com/en-us/windows/wsl/install-win10" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/5/5f/Windows_logo_-_2012.svg" height="35"></a>

## 1.0 About

* My GNU/Linux installation and configurations.
* **Dwm** desktop environment (DE) themes for: Debian, WSL, Archlinux, and Alpine Linux.
* Contains scripts, notes, clues, and links for using various _[GNU](https://www.gnu.org/philosophy/philosophy.html) apps and tools_.

### 1.1 Usage

* Install and configure <i>**DWM** on **Debian**</i>, Archlinux, Alpine Linux, and WSL.
* The installer will install a vanilla suckless DE and provide further options to apply.
    * Installer will auto detect OS and perform scripts specific to that OS.
* Targeted toward laptops and mini-pc's. 

    | Primary Apps | Recommended Apps | UI |
    | --- | --- | --- |
    | git, tmux, vim, ranger  | pandoc | tty |
    | geany, dmenu, st  | libreoffice, texstudio  | Xorg, VcXsrv |

## 2.0 Install

### 2.1 Installer scripts

Be ```root``` or have ```sudo``` installed and configured for user.

1. Configure **<i>WiFi</i>** using the ```./my_iwconfig.sh``` script.
2. Install **<i>DWM & Packages</i>** using the ```./install_dwm.sh``` script.

> The installer script will detect which OS is in use and perform configurations as necessary. It was tested to work on Debian <sup>[2](#myfootnote2)</sup> or Debian Salsa on WSL <sup>[3](#myfootnote3)</sup>, Archlinux <sup>[4](#myfootnote4)</sup>, and Alpine Linux <sup>[5](#myfootnote5)</sup>.

### 2.2 Home and Dot config files

* Additional ```rc``` config files can be manually ```copy/paste``` from this repo's ```home``` folder.
    * [home](./home)

## 3.0 Suckless Patches

| Suckless App | Patch |
| :---: | :---: |
| dwm | gridmode, rotatestack |
| st | st-dracula, st-nordtheme, st-solarized-both |
| dmenu | `patched from within dwm` |

## 4.0 Additional Themes

* Hosted on [deviantart.com/mezcel](https://www.deviantart.com/mezcel), are minimalist tribute desktop wallpapers.
* In addition to my themes, I imported: [Arc](https://github.com/arc-design/arc-theme), [Nordic](https://github.com/EliverLara/Nordic), [Nordic-Polar](https://github.com/EliverLara/Nordic-Polar)

    Themes installed through this repo:

    ```sh
    ## GTK themes for DWM
    bash simple-themes/dwm_appearance.sh

    ## Github hosted rice
    bash simple-themes/github_rice.sh

    ## Homebrew DWM theme switcher
    bash simple-themes/dwm_appearance.sh
    ```

### 4.1 Color themes

* After full installation [1-4] is complete. A DWM/St theme switching script will be available in the ```home``` directory.
    * ```~/dwm_appearance.sh```
    
    
    ```sh
    ## Example: 
    ##  change the color themes of: tty, st, dwm, dmenu, geany, and firefox 
    ./dwm_appearance.sh solarized
    ```

---

## 5.0 Operating System Installers

Additional scripts to install a linux distro.

* Archlinux [arch-files](arch-files/)
* Debian Linux [debian-files](debian-files/)
* Win10 WSL [wls-files](wls-files/)
    * In conjunction with [Xming](http://www.straightrunning.com/XmingNotes/) or <u>**[VcXsrv](https://sourceforge.net/projects/vcxsrv/)**</u> display servers.
* Alpine Linux [alpine-files](alpine-files/)

<!--
## References

<a id="myfootnote1">1</a>: Suckless philosophy, [suckless.org](https://suckless.org/philosophy/)

<a id="myfootnote2">2</a>: Debian Live, [debian.org](https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/)

<a id="myfootnote3">3</a>: Windows Subsystem for Linux Installation Guide for Windows 10, [WLS](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

<a id="myfootnote4">4</a>: ArchWiki, [archlinux.org](https://www.archlinux.org/download/)

<a id="myfootnote5">4</a>: Alpine Linux, [alpinelinux.org](https://www.alpinelinux.org/downloads/)
-->
