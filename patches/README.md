## Readme 

<a href="https://suckless.org/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Suckless_logo.svg/220px-Suckless_logo.svg.png" height="35"></a>

Within each directory is an installer which will reset respective suckless app to its factory default, and then apply theme patches.

These scripts are intended for 1st time installations or for resetting the suckless apps. 

## Suckless Apps 

* [dwm](https://git.suckless.org/dwm)
* [st](https://git.suckless.org/st)
* [dmenu](https://git.suckless.org/dmenu)
* [surf](http://git.suckless.org/surf)
* [slstatus](https://git.suckless.org/slstatus)

---

### diff notes

generate a ```.diff``` file which compares file changes

```sh
diff -u original.file modified.file > diff_file.diff
```

apply a ```.diff``` file

```sh
## Note:
## I put the .diff within the app's root directory

patch -p1 < diff_file.diff
```
