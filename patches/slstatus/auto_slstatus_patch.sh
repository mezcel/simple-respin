#!/bin/bash

mkdir -p ~/suckless/

function testPing {
	echo -e "\npinging suckless.org ...\n"
	sudo ping -c3 suckless.org &>/dev/null
	pingFlag=$?
}

function copyPatches {
	## copy patches to the slstatus root dir
	cp ~/suckless/patches/slstatus/my_slstatus_diff.diff ~/suckless/slstatus/
	sleep 1s
}

function applyPatches {
	## apply patches
	sudo patch -p1 < my_slstatus_diff.diff
	sleep 1s
}

function buildSlstatus {
	## Compile slstatus
	cd ~/suckless/slstatus/
	sudo make clean install

	## activate user write permissions to files
	sudo chmod u=rw ~/suckless/slstatus/config.h

	if [ -d ~/suckless/patches/slstatus ]; then
		copyPatches
		applyPatches

		## Make a new config.h
		sudo cp ~/suckless/slstatus/config.def.h ~/suckless/slstatus/config.h
		sleep 1s
	fi

	## re-Compile slstatus
	cd ~/suckless/slstatus/
	sudo make clean install
}

function automated_build {
	## get slstatus if it does not exist
	if [ ! -d ~/suckless/factory-default/slstatus ]; then
		mkdir -p ~/suckless/factory-default/
		testPing
		if [ $pingFlag -eq 0 ]; then
			git clone https://git.suckless.org/slstatus ~/suckless/factory-default/slstatus
			sleep 1s
		fi
	fi

	## make a fresh copy of of the factory default slstatus for editing/customization
	if [ -d ~/suckless/factory-default/slstatus ]; then
		if [ -d ~/suckless/slstatus ];then
			## delete any previously modified versions of slstatus
			rm -rf ~/suckless/slstatus
			sleep 1s
		fi

		## place in the default backup slstatus
		cp -rf ~/suckless/factory-default/slstatus ~/suckless/
		sleep 1s

		## build slstatus
		buildSlstatus
	fi
}

automated_build
