#!/bin/bash

colorFlag=$1
mkdir -p ~/suckless/

function testPing {
	echo -e "\npinging suckless.org ...\n"
	ping -c3 suckless.org &>/dev/null
	pingFlag=$?
}

function copyPatches {
	## copy patches to the st root dir
	cp ~/suckless/patches/st/st-nordtheme-0.8.2.diff ~/suckless/st/
	cp ~/suckless/patches/st/st-dracula-0.8.2.diff ~/suckless/st/
	cp ~/suckless/patches/st/st-font2-20190326-f64c2f8.diff ~/suckless/st/
	cp ~/suckless/patches/st/st-xresources-20190105-3be4cf1.diff ~/suckless/st/
	cp ~/suckless/patches/st/st-solarized-both-20190128-3be4cf1.diff ~/suckless/st/
	cp ~/suckless/patches/st/st-solarized-light-20190306-ed68fe7.diff ~/suckless/st/
	cp ~/suckless/patches/st/my_st_diff.diff ~/suckless/st/
	sleep 1s
}

function applyPatches {
	## apply patches

	if [ ! -z  $colorFlag ]; then
		## default black colors
		if [ $colorFlag == "xresources" ]; then
			sudo patch -p1 < st-xresources-20190105-3be4cf1.diff
		elif [ $colorFlag == "solarized" ]; then
			sudo patch -p1 < st-solarized-both-20190128-3be4cf1.diff
		elif [ $colorFlag == "solarized_light" ]; then
			sudo patch -p1 < st-solarized-light-20190306-ed68fe7.diff
		elif [ $colorFlag == "nordtheme" ]; then
			sudo patch -p1 < st-nordtheme-0.8.2.diff
		elif [ $colorFlag == "dracula" ]; then
			sudo patch -p1 < st-dracula-0.8.2.diff
		fi
	fi

	sudo patch -p1 < st-font2-20190326-f64c2f8.diff
	sudo patch -p1 < my_st_diff.diff

	sleep 1s
}

function manualColorChanger {
	if [ $colorFlag == "black" ]; then
		sudo chmod u=rw ~/suckless/st/config.h
		## sed ':again;$!N;$!b again; s/VariableDeclarationEquals{[^}]*}/CharsInsideCurlyBrace/g'
		sudo sed -e ':again' -e N -e '$!b again' -e 's/static\ const\ char\ \*colorname\[\]\ \=\ {[^}]*}/static\ const\ char\ \*colorname\[\]\ \=\
		\{\
		\/\* https\:\/\/github.com\/honza\/base16\-st\/blob\/master\/build\/base16\-grayscale\-dark\-theme\.h \*\/\
		\"#101010\"\, \/\* base00 \*\/\
		\"#7c7c7c\"\, \/\* base08 \*\/\
		\"#8e8e8e\"\, \/\* base0B \*\/\
		\"#a0a0a0\"\, \/\* base0A \*\/\
		\"#686868\"\, \/\* base0D \*\/\
		\"#747474\"\, \/\* base0E \*\/\
		\"#868686\"\, \/\* base0C \*\/\
		\"#b9b9b9\"\, \/\* base05 \*\/\
		\"#525252\"\, \/\* base03 \*\/\
		\"#999999\"\, \/\* base09 \*\/\
		\"#FFFFFF\"\, \/\* base01 adjusted \*\/\
		\"#464646\"\, \/\* base02 \*\/\
		\"#ababab\"\, \/\* base04 \*\/\
		\"#e3e3e3\"\, \/\* base06 \*\/\
		\"#5e5e5e\"\, \/\* base0F \*\/\
		\"#f7f7f7\"\, \/\* base07 \*\/\
		\}/g' ~/suckless/st/config.h > temp && sudo mv temp ~/suckless/st/config.h
	fi
}

function buildSt {
	## Compile st
	cd ~/suckless/st/
	sudo make clean install

	## activate user write permissions to files
	sudo chmod u=rw ~/suckless/st/config.h
	sudo chmod u=rw ~/suckless/st/x.c

	if [ -d ~/suckless/patches/st ]; then
		copyPatches
		applyPatches

		## Make a new config.h
		sudo cp ~/suckless/st/config.def.h ~/suckless/st/config.h

		manualColorChanger

		sleep 1s
	fi

	## re-Compile st
	cd ~/suckless/st/
	sudo make clean install
}

function automated_build {
	## get st if it does not exist
	if [ ! -d ~/suckless/factory-default/st ]; then
		mkdir -p ~/suckless/factory-default/
		testPing
		if [ $pingFlag -eq 0 ]; then
			git clone git://git.suckless.org/st ~/suckless/factory-default/st
			sleep 1s
		fi
	fi

	## make a fresh copy of of the factory default st for editing/customization
	if [ -d ~/suckless/factory-default/st ]; then
		if [ -d ~/suckless/st ];then
			## delete any previously modified versions of st
			rm -rf ~/suckless/st
			sleep 1s
		fi

		## place in the default backup st
		cp -rf ~/suckless/factory-default/st ~/suckless/
		sleep 1s

		## build st
		buildSt
	fi
}

automated_build
