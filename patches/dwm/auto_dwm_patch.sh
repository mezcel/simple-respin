#!/bin/bash

## clone a fresh official standard version
mkdir -p ~/suckless/

function testPing {
	echo -e "\npinging suckless.org ...\n"
	sudo ping -c3 suckless.org &>/dev/null
	pingFlag=$?
}

function copyPatches {
	## copy patches to the dwm root dir
	cp ~/suckless/patches/dwm/dwm-gridmode-20170909-ceac8c9.diff ~/suckless/dwm/
	cp ~/suckless/patches/dwm/dwm-rotatestack-20161021-ab9571b.diff ~/suckless/dwm/

	## my diff
	cp ~/suckless/patches/dwm/my_dwm_diff.diff ~/suckless/dwm/
	sleep 1s
}

function applyPatches {
	## apply patches
	cd ~/suckless/dwm/
	sudo patch -p1 < ~/suckless/dwm/dwm-gridmode-20170909-ceac8c9.diff
	sudo patch -p1 < ~/suckless/dwm/dwm-rotatestack-20161021-ab9571b.diff

	## my diff
	sudo patch -p1 < ~/suckless/dwm/my_dwm_diff.diff
	sleep 1s
}

function buildDwm {
	## Compile dwm
	cd ~/suckless/dwm/
	sudo make clean install

	## activate user write permissions
	sudo chmod u=rw ~/suckless/dwm/config.h
	sudo chmod u=rw ~/suckless/dwm/dwm.c

	if [ -d ~/suckless/patches/dwm ]; then
		copyPatches
		applyPatches

		## Make a new config.h
		sudo cp ~/suckless/dwm/config.def.h ~/suckless/dwm/config.h
		sleep 1s
	fi

	## re-Compile dwm
	cd ~/suckless/dwm/
	sudo make clean install
}

function automated_build {
	## get dwm if it does not exist
	if [ ! -d ~/suckless/factory-default/dwm ]; then
		mkdir -p ~/suckless/factory-default/
		testPing
		if [ $pingFlag -eq 0 ]; then
			git clone git://git.suckless.org/dwm ~/suckless/factory-default/dwm
			sleep 1s
		fi
	fi

	## make a fresh copy of of the factory default dwm for editing/customization
	if [ -d ~/suckless/factory-default/dwm ]; then
		if [ -d ~/suckless/dwm ];then
			## delete any previously modified versions of dwm
			rm -rf ~/suckless/dwm
			sleep 1s
		fi

		## place in the default backup dwm
		cp -rf ~/suckless/factory-default/dwm ~/suckless/
		sleep 1s

		## build dwm
		buildDwm
	fi
}

automated_build
