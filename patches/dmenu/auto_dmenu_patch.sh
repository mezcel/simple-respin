#!/bin/bash

## clone a fresh official standard version
mkdir -p ~/suckless/

function testPing {
	echo -e "\npinging suckless.org ...\n"
	sudo ping -c3 suckless.org &>/dev/null
	pingFlag=$?
}

function copyPatches {
	## copy patches to the dmenu root dir
	echo "no patches for dmenu"
	#cp ~/suckless/patches/dmenu/my_dmenu_diff.diff ~/suckless/dmenu/
	sleep 1s
}

function applyPatches {
	## apply patches
	echo "no patches for dmenu"
	#sudo patch -p1 < ~/suckless/dmenu/my_dmenu_diff.diff
	sleep 1s
}

function buildDmenu {
	## Compile dmenu
	cd ~/suckless/dmenu/
	sudo make clean install

	## activate user write permissions
	#sudo chmod u=rw ~/suckless/dmenu/config.h
	#sudo chmod u=rw ~/suckless/dmenu/dmenu.c

	if [ -d ~/suckless/patches/dmenu ]; then
		copyPatches
		applyPatches

		## Make a new config.h
		#sudo cp ~/suckless/dmenu/config.def.h ~/suckless/dmenu/config.h
		sleep 1s
	fi

	## re-Compile dmenu
	cd ~/suckless/dmenu/
	sudo make clean install
}

function automated_build {
	## get dmenu if it does not exist
	if [ ! -d ~/suckless/factory-default/dmenu ]; then
		mkdir -p ~/suckless/factory-default/
		testPing
		if [ $pingFlag -eq 0 ]; then
			git clone https://git.suckless.org/dmenu ~/suckless/factory-default/dmenu
			sleep 1s
		fi
	fi

	## make a fresh copy of of the factory default dmenu for editing/customization
	if [ -d ~/suckless/factory-default/dmenu ]; then
		if [ -d ~/suckless/dmenu ];then
			## delete any previously modified versions of dmenu
			rm -rf ~/suckless/dmenu
			sleep 1s
		fi

		## place in the default backup dmenu
		cp -rf ~/suckless/factory-default/dmenu ~/suckless/
		sleep 1s

		## build dmenu
		buildDmenu
	fi
}

automated_build
