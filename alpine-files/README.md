## Alpine Linux Notes

[![alpinelinux-logo.svg](https://alpinelinux.org/alpinelinux-logo.svg)](https://alpinelinux.org/)

[gist repo](https://gist.github.com/mezcel/ff833a444f2671879b22e76aa4ed61c5)

* [downloads](https://alpinelinux.org/downloads/)
* [Installation](https://wiki.alpinelinux.org/wiki/Installation)
* [Alpine setup scripts](https://wiki.alpinelinux.org/wiki/Alpine_setup_scripts)
* [Dwm](https://wiki.alpinelinux.org/wiki/Dwm)

### BusyBox: The Swiss Army Knife of Embedded Linux

* [![busybox.net](https://www.busybox.net/images/busybox1.png)](https://www.busybox.net/)
    * BusyBox combines tiny versions of many common UNIX utilities into a single small executable. It provides replacements for most of the utilities you usually find in GNU fileutils, shellutils, etc. The utilities in BusyBox generally have fewer options than their full-featured GNU cousins; however, the options that are included provide the expected functionality and behave very much like their GNU counterparts. BusyBox provides a fairly complete environment for any small or embedded system.

---

## APK

* [Alpine Linux package management](https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management)
* [Alpine setup scripts](https://wiki.alpinelinux.org/wiki/Alpine_setup_scripts)

## APK Repos

* repos ```/etc/apk/repositories```

```sh
## Modify repos

setup-apkrepos
```

### APK search

```sh
## seach for video packages

apk search xf86-video
```

## Wiki Help

* [ What does required by: world $pkgnamemean? ](https://wiki.alpinelinux.org/wiki/Alpine_Linux:FAQ#What_does_.22required_by:_world.5B.24pkgname.5D.22_mean.3F)
* [How to Mount USB drive in Alpine Linux](http://joe.22web.org/how-to-mount-usb-drive-in-alpine-linux/?i=1)
    ```sh
    ## Follow this steps in order to mount an USB Drive on an alpine linux system:

    ## Check the device corresponding to your USB Drive on the system (for example sdb1)
    cat /proc/partitions

    ## Edit your fstab file
    nano /etc/fstab
    ## Add the following line (replace “sdb1” with your device name):
    /dev/sdb1 /media/usb vfat noauto 0 0
    ## Mount the drive
    mount /media/usb

    ## cd into your mount
    cd /media/usb
    ```

