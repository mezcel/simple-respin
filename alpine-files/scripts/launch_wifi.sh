#!/bin/bash

function startWifiDaemon {
    ## Wifi
    ## https://wiki.alpinelinux.org/wiki/Connecting_to_a_wireless_access_point

    wifiInterface=wlan0

    sudo ip link set $wifiInterface up
    #wpa_supplicant -B -i $wifiInterface -c /etc/wpa_supplicant/wpa_supplicant.conf

    sudo /etc/init.d/wpa_supplicant start
}

## Alpine conection manager
# sudo setup-interfaces

## My Conman
startWifiDaemon
