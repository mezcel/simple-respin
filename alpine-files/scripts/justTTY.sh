#!/bin/bash

function justTTY {
    ## Post install apps
	apk add sudo
    sudo apk add attr dialog dialog-doc bash bash-doc bash-completion grep grep-doc
    sudo apk add util-linux util-linux-doc pciutils usbutils binutils findutils readline

    ## My basic needs
    sudo apk add bash bash-completion wget
    sudo apk add vim mc
    sudo apk add git tmux ranger
    sleep 1s

    sudo apk add build-base
    #sudo apk add linux-headers
    sudo apk add ncurses
    sudo apk add bc

    sudo apk add aspell aspell-en aspell-dev aspell-utils
    sudo apk add hunspell hunspell-en

	sudo apk add elinks
	sudo apk add go

	sudo apk add man-db
	sudo apk add man-pages
}

function apkFonts {
	sudo apk add nerd-fonts
	sudo apk add ttf-inconsolata
	sudo apk add ttf-ubuntu-font-family
}

justTTY
apkFonts 
