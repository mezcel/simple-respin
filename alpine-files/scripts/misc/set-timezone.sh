#!/bin/bash

sudo apk add tzdata
ls /usr/share/zoneinfo

## copy time zone
cp /usr/share/zoneinfo/America/New_York /etc/localtime

## set timezone
sudo echo "America/New_York" >  /etc/timezone

date
