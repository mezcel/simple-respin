#!/bin/bash

function DlMPlayer {
    ## https://pkgs.alpinelinux.org/package/edge/community/x86_64/mplayer
    ## https://git.alpinelinux.org/aports/tree/community/mplayer/APKBUILD
    
    sudo apk add libxxf86dga-dev libxv-dev libmad-dev lame-dev libao-dev
	sudo apk add libtheora-dev xvidcore-dev zlib-dev sdl-dev freetype-dev
	sudo apk add x264-dev faac-dev ttf-dejavu libxvmc-dev alsa-lib-dev live-media-dev
	sudo apk add mesa-dev yasm libpng-dev libvdpau-dev libvpx-dev libcdio-paranoia-dev

    ## http://mplayerhq.hu/design7/dload.html#binary_codecs
    #sudo apk add mplayer ## x64 only

    wget -P ~/Downloads/ http://mplayerhq.hu/MPlayer/releases/codecs/essential-20071007.tar.bz2
    
    ## cd ~/Downloads/
    ## tar -xf essential-20071007.tar.bz2

    wget -P ~/Downloads/ http://www.mplayerhq.hu/MPlayer/releases/MPlayer-1.4.tar.xz
    
    ## cd ~/Downloads/
    ## tar -xf MPlayer-1.4.tar.xz
}

DlMPlayer