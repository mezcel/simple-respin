#!/bin/bash

## Must be root

function setupWifi {
    ## Wifi
    ## https://wiki.alpinelinux.org/wiki/Connecting_to_a_wireless_access_point

    confPath=/etc/wpa_supplicant/wpa_supplicant.conf
    wifiInterface=wlan0

    echo -e "\n---\n"

    ip link set $wifiInterface up
    iwlist $wifiInterface scanning

    echo -e "\n---\n Select the SSID to connect to:"
    
    read -p 'SSID Name: ' myssd
    read -sp 'SSID password: ' mypw
    
    iwconfig $wifiInterface essid $myssd
    iwconfig $wifiInterface
    
    wpa_passphrase $myssd $mypw > $confPath

    ip addr show $wifiInterface

    # /etc/init.d/wpa_supplicant start

    # wpa_supplicant -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
    wpa_supplicant -B -i $wifiInterface -c $confPath

}

setupWifi

echo -e "\nDone.\n"