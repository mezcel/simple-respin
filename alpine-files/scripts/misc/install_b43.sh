#!/bin/bash

## https://wiki.alpinelinux.org/wiki/Connecting_to_a_wireless_access_point#Broadcom_Wi-Fi_Chipset_Users
## https://wireless.wiki.kernel.org/en/users/Drivers/b43

function DlAports {
    ## http://git.alpinelinux.org/cgit/aports/tree/non-free/b43-firmware/APKBUILD

    git clone git://git.alpinelinux.org/aports
}

function DlFwcutterAPK {

    ## https://pkgs.org/download/b43-fwcutter
    ## Mirror   dl-cdn.alpinelinux.org

    wget http://dl-cdn.alpinelinux.org/alpine/v3.12/main/x86/b43-fwcutter-019-r1.apk

    wget http://dl-cdn.alpinelinux.org/alpine/v3.12/main/x86_64/b43-fwcutter-019-r1.apk
}

function Prerequisites {
    apk add wireless-tools wpa_supplicant
}


function ApkLocal {
    ## to add a package stored locally, just pass the path to apk add

    apk add /home/dtm_nv/my_new_package.apk
}

function BuildApk {
    addgroup $(whoami) abuild
    abuild-keygen -a -i
    abuild -r
    apk add --allow-untrusted ~/packages/...pkg
}

## You can check if you have a Broadcom chipset by using dmesg:

dmesg | grep Broadcom

## First install the SDK an Git:
#ApkLocal
sudo apk add alpine-sdk git

## Then git clone aports from git.alpinelinux.org.
DlAports

## Change your directory to aports/non-free/b43-firmware, then build it.

cd aports/non-free/b43-firmware


## Tip: You can't be root and must be a user of the group abuild (use groupadd f.e. addgroup $(whoami) abuild)
## Tip: If this is your first time building a package you will need to generate a key for use in signing packages (use abuild-keygen -a -i)

addgroup $(whoami) abuild
abuild-keygen -a -i

abuild -r

## Install the generated packge file (it will be in ~/packages/) - make sure to pass --allow-untrusted

apk add --allow-untrusted ~/packages/...pkg

## Now we need fwcutter, which is executed from the firmware package:

apk add b43-fwcutter b43-firmware

## Now you need to use modprobe so the device will show up:

modprobe b43

## To automate this on startup add it to /etc/modules:

echo b43 >> /etc/modules
