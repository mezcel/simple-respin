#!/bin/bash

## Follow this steps in order to mount an USB Drive on an alpine linux system:

## Check the device corresponding to your USB Drive on the system (for example sdb1)
cat /proc/partitions


echo -e "\n---\nYou may need /dev/sda1 or /dev/mmcblk0p1 etcetera\n\n"
read -p "Enter mount source [/dev/sdb1]: " -i "/dev/sdb1" myUsb

## Edit your fstab file
echo "$myUsb /media/usb vfat noauto 0 0" >> /etc/fstab

## Mount the drive
mount /media/usb

## cd into your mount
cd /media/usb

echo -e "\nDone.\n"
