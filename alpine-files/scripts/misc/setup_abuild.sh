#!/bin/bash

## https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package#:~:text=%20Creating%20an%20Alpine%20package%20%201%20Requirements.,to...%204%20Send%20a%20patch.%20%20More%20

apk add alpine-sdk
adduser <yourusername>

visudo
## sudo echo "<yourusername>    ALL=(ALL) ALL" >> /etc/sudoers

apk add git
git config --global user.name "Your Full Name"
git config --global user.email "your@email.address"

## Aports tree
git clone git://git.alpinelinux.org/aports
#git clone https://github.com/alpinelinux/aports

## Before we start creating or modifying APKBUILD files, we need to setup abuild for our system and user. Edit the file abuild.conf to your requirements:

sudo vi /etc/abuild.conf

## To use 'abuild -r' command to install dependency packages automatically.

sudo addgroup <yourusername> abuild

## We also need to prepare the location where the build process caches files when they are downloaded.

sudo mkdir -p /var/cache/distfiles
sudo chmod a+w /var/cache/distfiles

## The last step is to configure the security keys with the abuild-keygen script for abuild with the command:

abuild-keygen -a -i
