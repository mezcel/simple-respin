#!/bin/bash

## https://wiki.alpinelinux.org/wiki/Connecting_to_a_wireless_access_point

function LoadUsbApplications {
    command -v iwlist
    if [ $? -ne 0 ]; then
        apk add wireless-tools wpa_supplicant
    fi

    command -v bash
    if [ $? -ne 0 ]; then
        apk add bash
    fi
}

function SetupWifi {
    ip link set wlan0 up

    iwlist wlan0 scanning | grep SSID

    read -p "Enter SSID: " myssid
    read -p "Enter password: " mypwd

    wpa_passphrase $myssid $mypwd > /etc/wpa_supplicant/wpa_supplicant.conf

    wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf

    /etc/init.d/wpa_supplicant start
}

function ConfigureMirrors {
    #########################
    ## After internet works
    #########################

    ## Use the fastest repos
    setup-apkrepos

    ## Manually enter known repos

    echo "http://ewr.edge.kernel.org/alpine/v3.12/main" >> /etc/apk/repositories
    echo "http://ewr.edge.kernel.org/alpine/v3.12/community" >> /etc/apk/repositories
    
    echo "https://ewr.edge.kernel.org/alpine/v3.12/main" >> /etc/apk/repositories
    echo "https://ewr.edge.kernel.org/alpine/v3.12/community" >> /etc/apk/repositories


    ## https://ewr.edge.kernel.org/alpine/latest-stable/main/x86/
    ## https://ewr.edge.kernel.org/alpine/latest-stable/community/x86/

}

LoadUsbApplications
SetupWifi

#ConfigureMirrors