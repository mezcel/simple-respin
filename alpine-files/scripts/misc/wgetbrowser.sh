#!/bin/bash

## this is used to view a simple web page offline.
## the aim is to save on browser related browsing
## not substitute for web surfing
## terminal browsers are hard to read and standard web browsers are resource heavy
## recommended websites are wikipedia.org, https://www.usccb.org, or any other simple to read blogs where navigation and multimedia interactivity is not required to read the site.

urlInput=$1
mywebbrowser=firefox
tempWebsite=~/Downloads/WgetWebsites/

mkdir -p $tempWebsite

if [ ! -z $urlInput ]; then
    wget -P $tempWebsite $ -p -k $urlInput

    if [ $? -eq 0 ]; then
        firefox $tempWebsite

        echo -e "\nRemember to delete the $tempWebsite when you are done. Local storage will get overcrowded.\n"
    fi
else
    echo -e "\nEnter a url and try again\n"
fi

