#!/bin/bash

################################################################################
## About:
## This installer will install DWM on a live instance of Alpine Linux
##      This is a modified "setup-alpine" for my netbook and my workflow
## This installer assumes the following:
##  Boot from a usb using the "Extended" .iso
##      https://alpinelinux.org/downloads
##  Using a tablet/netbook with both a USB and MicroSD slot
##      USB = BOOT iso
##          Initial apps will be installed from included .apk packages
##      MicroSD = Personal apk mirror library
##          Additional .apk apps will be installed offline from there
##          I use the x86 packages
################################################################################

function MountMirrorUsb {
    echo ""
    ## List detected partitions
    ## cat /proc/partitions

    echo -e "\nMount candidates:\n"
    sudo grep "sda" /proc/partitions
    sudo grep "sdb" /proc/partitions
    sudo grep "mmcblk" /proc/partitions
    echo -e "\n---\n"
    sleep .5s

    echo ""
    echo -e "Enable Local Cache to /dev/sdb1, /dev/mmcblk0p1, or similar.\n\tsdb is a USB\n\tmmcblk is EUFI Usb/Micro\n"
    promptString="Set usb drive [mmcblk0p1]: "
    read -e -p "${promptString}" -i "mmcblk0p1" myusb

    sudo mkdir -p /media/$myusb/cache
    sleep 1s

    ## https://wiki.debian.org/fstab
    ## Read only
    #sudo echo -e "/dev/$myusb\t/media/$myusb\tvfat\tnoauto,ro 0 0" >> /etc/fstab

    ## Read and write
    sudo echo -e "/dev/$myusb\t/media/$myusb\tvfat\tnoauto,rw 0 0" >> /etc/fstab

    sleep 1s
    sudo mount /media/$myusb
    sleep 1s

    echo ""
    cat /etc/fstab
    echo ""
    sleep 1s

    echo ""
}

function MirrorPrompt {
    clear
    sleep 1s
    echo ""
    promptString="Use a local /media/usb mirror repo? [y/N]: "
    read -e -p "${promptString} " -i "n" yn
    case $yn in
        [Yy]* ) MountMirrorUsb ;;
        * ) echo -e "No.canceled\n";;
    esac

    sleep 1s
    echo ""
}

function AddMirror {
    echo -e "\nInstalling packages from $myusb\n"
    x86=$(cat /etc/apk/arch)

    sleep 1s

    ## Main mirror

    if [ -f /media/$myusb/ewr.edge.kernel.org/main/$x86/APKINDEX.tar.gz ]; then
        sudo echo "" >> /etc/apk/repositories
        sudo echo "## My main mirror" >> /etc/apk/repositories
        sudo echo "/media/$myusb/ewr.edge.kernel.org/main" >> /etc/apk/repositories
        sudo echo "#http://ewr.edge.kernel.org/alpine/v3.12/main" >> /etc/apk/repositories
    elif [ -f /media/usb/ewr.edge.kernel.org/main/$x86/APKINDEX.tar.gz ]; then
        sudo echo "" >> /etc/apk/repositories
        sudo echo "## My main mirror" >> /etc/apk/repositories
        sudo echo "/media/usb/ewr.edge.kernel.org/main" >> /etc/apk/repositories
        sudo echo "#http://ewr.edge.kernel.org/alpine/v3.12/main" >> /etc/apk/repositories
    else
        sudo echo "" >> /etc/apk/repositories
        sudo echo "## My main mirror" >> /etc/apk/repositories
        sudo echo "#/media/usb/ewr.edge.kernel.org/main" >> /etc/apk/repositories
        sudo echo "http://ewr.edge.kernel.org/alpine/v3.12/main" >> /etc/apk/repositories
    fi

    ## Community mirror

    if [ -f /media/$myusb/ewr.edge.kernel.org/community/$x86/APKINDEX.tar.gz ]; then
        sudo echo "" >> /etc/apk/repositories
        sudo echo "## My community mirror" >> /etc/apk/repositories
        sudo echo "/media/$myusb/ewr.edge.kernel.org/community" >> /etc/apk/repositories
        sudo echo "#https://ewr.edge.kernel.org/alpine/v3.12/community" >> /etc/apk/repositories
    elif [ -f /media/usb/ewr.edge.kernel.org/community/$x86/APKINDEX.tar.gz ]; then
        sudo echo "" >> /etc/apk/repositories
        sudo echo "## My community mirror" >> /etc/apk/repositories
        sudo echo "/media/usb/ewr.edge.kernel.org/community" >> /etc/apk/repositories
        sudo echo "#https://ewr.edge.kernel.org/alpine/v3.12/community" >> /etc/apk/repositories
    else
        sudo echo ""
        sudo echo "## My community mirror"
        sudo echo "#/media/usb/ewr.edge.kernel.org/community" >> /etc/apk/repositories
        sudo echo "https://ewr.edge.kernel.org/alpine/v3.12/community" >> /etc/apk/repositories
    fi

    sleep 1s

    sudo apk update
    sleep 1s
}

function MakeCache {
    clear
    sleep 1s
    echo -e "\nExisting media mounts:\n\tuse the /media/ one ...\n"
    cat /etc/fstab
    echo -e "\n---\n"
    sleep 1s


    echo -e "\nPersistent USB configs.\n"
    promptString="Save setting on live session? [y/N]: "
    read -e -p "${promptString} " -i "n" yn
    case $yn in
        [Yy]* )
            sudo setup-lbu
            sudo setup-apkcache
            sudo lbu commit
            ;;
        * )
            sudo setup-apkcache
            ;;
    esac

    sudo setup-lbu
    sudo setup-apkcache
    sudo lbu commit

    sleep 1s
    #sudo apk cache clean
    echo ""
}

function InstallApk {

    ## Post install apps
    sudo apk add attr dialog dialog-doc bash bash-doc bash-completion grep grep-doc
    sudo apk add util-linux util-linux-doc pciutils usbutils binutils findutils readline

    ## My basic needs
    sudo apk add bash bash-completion wget
    sudo apk add vim tmux ranger
    sleep 1s

    sudo apk add build-base
    #sudo apk add linux-headers

    sudo setup-xorg-base
    sudo apk add xinit
    sudo apk add xorg-server

    sudo apk add make
    sudo apk add gcc
    sudo apk add g++
    sudo apk add libx11-dev
    sudo apk add libxft-dev
    sudo apk add libxinerama-dev
    sudo apk add ncurses
    sudo apk add dbus-x11
    sudo apk add adwaita-gtk2-theme
    sudo apk add adwaita-icon-theme
    sudo apk add pcmanfm
    sudo apk add ttf-ubuntu-font-family

    sudo apk add dwm
    sudo apk add dmenu
    sudo apk add st

    sudo apk add aspell aspell-en
    sudo apk add hunspell hunspell-en

    sudo apk add 

    ########################################

    echo -e "\nInstall a GUI web browser.\n"
    promptString="Install firefox-esr? [y/N]: "
    read -e -p "${promptString} " -i "n" yn
    case $yn in
        [Yy]* )
            sudo apk add ttf-dejavu
            sudo apk add webkitgtk-dev
            sudo apk add librsvg
            sudo apk add libsoup libsoup-dev

            sudo apk add firefox-esr
            #sudo apk add midori
            #sudo apk add qutebrowser
            ;;
        * ) echo -e "No.canceled\n";;
    esac

    echo -e "\nInstall a GUI text editor.\n"
    promptString="Install Geany? [y/N]: "
    read -e -p "${promptString} " -i "n" yn
    case $yn in
        [Yy]* )
            sudo apk add geany
            sudo apk add geany-plugins
            ;;
        * ) echo -e "No.canceled\n";;
    esac

    echo -e "\nInstall a GUI word processor.\n"
    promptString="Install Abiword? [y/N]: "
    read -e -p "${promptString} " -i "n" yn
    case $yn in
        [Yy]* )
            #libreoffice-writer
            sudo apk add abiword
            ;;
        * ) echo -e "No.canceled\n";;
    esac


    echo "exec dwm" > ~/.xinitrc
    echo 'PS1="\u@\h:\w\a$ "' > ~/.profile
    sudo cp /etc/profile /ect/profile_backup.$(date +%d%b%Y_%H%M%S)
    sudo echo 'PS1="\u@\h:\w\a$ "' > /etc/profile

    #sudo apk add feh

    echo -e "\nInstalled my general purpose packages from\t$myusb"
    echo ""

    promptString="Do you want audio? [y/N]: "
    read -e -p "${promptString}" -i "n" yn
    case $yn in
        [Yy]* )
            ## First you will need to install Alsa packages, the Linux sound driver and volume adjuster.
            sudo apk add alsa-utils alsa-utils-doc
            sudo apk add alsa-lib
            sudo apk add alsaconf

            ## Then you will need to add all your users (including root) to the audio group
            sudo addgroup $USER audio
            sudo addgroup root audio

            sleep 1s
            sudo rc-service alsa start

            sleep 1s
            sudo rc-update add alsa
            ;;
        * ) echo -e "No.canceled\n";;
    esac

}

function SetHostName {
    echo -e "\n---\n"
    sudo setup-hostname
    sleep 1s
    sudo /etc/init.d/hostname restart
    sleep 1s
    echo ""
}

function DefineUser {

    apk add sudo
    sleep 1s
    echo ""
    echo -e "\nAdd a new user:\n"
    read -p 'new username: ' username
    adduser $username

    sleep 1s
    echo ""

    echo ""
    echo -e "## add the following to /etc/sudoers.tmp"
    echo -e "$username\tALL=(ALL) ALL"
    read -p "Press enter to continue ..." pauseVar
    #visudo

    sleep 1s

    ## Then you will need to add all your users (including root) to the audio group
    sudo addgroup $username audio
    sleep 1s
    echo ""
    sudo addgroup root audio
    sleep 1s
    echo ""
}

function EnableWifi {
    clear
    echo -e "\nMounted cache candidates:\n"
    sudo grep "sda" /proc/partitions
    sudo grep "sdb" /proc/partitions
    sudo grep "mmcblk" /proc/partitions
    echo -e "\n---\n"
    sleep 1s

    sudo setup-interfaces
    sleep 1s
    sudo /etc/init.d/networking start
    sleep 1s
    ping -c3 google.com
    echo ""

    #echo ""
    #promptString="Do you want firefox-esr ? [y/N]: "
    #read -e -p "${promptString}" -i "n" yn
    #case $yn in
    #    [Yy]* ) sudo apk add firefox-esr ;;
    #    * ) echo -e "\nNo.canceled\n" ;;
    #esac
}

function DlGitRepo {

    mkdir -p ~/suckelss/
    mkdir -p ~/Pictures/

    sudo apk add git

    git clone https://git.suckless.org/dwm ~/suckelss/dwm
    git clone https://git.suckless.org/dmenu ~/suckelss/dmenu
    git clone https://git.suckless.org/st ~/suckelss/st

    if [ -d ~/suckelss/st ]; then
        cd ~/suckelss/st
        sudo make clean install
    fi

    if [ -d ~/suckelss/dmenu ]; then
        cd ~/suckelss/dmenu
        sudo make clean install
    fi


    if [ -d ~/suckelss/dwm ]; then
        cd ~/suckelss/dwm
        sudo make clean install
    fi

    #echo "feh --bg-scale ~/Pictures/alpine3.png" > ~/.fehbg
    #echo -e "bash ~/.fehbg & \nexec dwm" > ~/.xinitrc
}

function main {

    apk add sudo bash vim tmux
    sleep 1s

    if [ "$USER" == "root" ]; then

        ## Display current mounts, repos, and partitions

        echo -e "\nAvailable Mount candidates:\n"
        sudo grep "sdb" /proc/partitions
        sudo grep "mmcblk" /proc/partitions

        echo -e "\nCurrent media mounts: ...\n"
        cat /etc/fstab

        echo -e "\nCurrent mirror repos ...\n"
        cat /etc/apk/repositories

        echo -e "\n---\n"

        promptString="Configure usb mount and apk repository? [y/N]: "
        read -e -p "${promptString} " -i "n" yn
        case $yn in
            [Yy]* )
                MirrorPrompt
                AddMirror
                MakeCache
                ;;
            * ) echo -e "No.canceled\n";;
        esac

        InstallApk

        echo -e "\n\n"
        promptString="Configure wifi? [y/N]: "
        read -e -p "${promptString} " -i "n" yn
        case $yn in
            [Yy]* )
                EnableWifi
                ;;
            * ) echo -e "No.canceled\n";;
        esac

        SetHostName
        #DefineUser
    fi
}

main

#DlGitRepo


echo -e "\nDone.\n"
