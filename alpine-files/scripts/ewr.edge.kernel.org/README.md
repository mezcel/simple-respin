# Offline USB Mirror Repository

## [ewr.edge.kernel.org](https://ewr.edge.kernel.org/alpine/v3.12)

Clone the [ewr.edge.kernel.org](https://ewr.edge.kernel.org/alpine/v3.12) Mirror to the local computer

## Scripts

* The ```dl_offline_apks.sh``` script will download **(13G - 14G)** of ```*.apk``` files to the local computer.

* The ```myLiveRepo.sh``` will add a personally curated usb mirror to the ```/etc/apk/repositories``` list
