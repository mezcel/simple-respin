#!/bin/bash

## https://ewr.edge.kernel.org/alpine/
## Download/Fetch each package listed in a textfile


## Fetch from alpine
function FetchApk {
    world=/etc/apk/world

    while IFS="" read -r PackageList || [ -n "$PackageList" ]; do
        sudo apk fetch "$PackageList"
    done <$world
}

## Wget from Main Mirror Repo

function WegetApk {
    ###################
    ## x86 or x86_64
    apkArch=$1

    ## main or community
    apkLib=$2

    ###################

    mirror=https://ewr.edge.kernel.org/alpine/v3.12/$apkLib/$apkArch

    destination=./$apkLib/$apkArch
    mkdir -p $destination

    apkindex=$mirror/APKINDEX.tar.gz
    mirrorHtml=$mirror/

    wget -P $destination $apkindex

    scrapeFile=$apkArch-$apkLib.html
    wget -O ./$scrapeFile $mirrorHtml

    href=" "
    apkUrl=" "
    while IFS="" read -r aTag || [ -n "$aTag" ]; do
        href=$(echo $aTag | sed -r 's/^.+href="([^"]+)".+$/\1/')
        apkUrl=https://ewr.edge.kernel.org/alpine/v3.12/$apkLib/$apkArch/$href

        if [ ! -f $destination/$href ]; then
            wget -P $destination $apkUrl
        fi
    done <$scrapeFile

}

function ScrapeApk {
    ## x86
    WegetApk "x86" "main"
    WegetApk "x86" "community"
    
    ## x86_64
    WegetApk "x86_64" "main"
    WegetApk "x86_64" "community"

}

## Wget from Community Mirror Repo

function ScrapeCommunityApk {

    ## My desired Repo Mirror: https://ewr.edge.kernel.org/alpine/v3.12/community/x86/

    destination=./community/x86
    mkdir -p $destination

    index=https://ewr.edge.kernel.org/alpine/v3.12/community/x86/APKINDEX.tar.gz
    community=https://ewr.edge.kernel.org/alpine/v3.12/community/x86/

    wget -P $destination $index
    wget -O ./x86-community.html $community

    while IFS="" read -r aTag || [ -n "$aTag" ]; do
        href=$(echo $aTag | sed -r 's/^.+href="([^"]+)".+$/\1/')
        apkUrl=https://ewr.edge.kernel.org/alpine/v3.12/community/x86/$href

        if [ ! -f $destination/$href ]; then
            wget -P $destination $apkUrl
        fi
    done <x86-community.html

}

###################
## Run
###################

## apk fetch <package>
#FetchApk

## wget <package.apk>
ScrapeApk
