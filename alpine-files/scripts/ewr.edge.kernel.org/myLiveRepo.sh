#!/bin/bash

if [ -f /media/usb/ewr.edge.kernel.org/main/x86/APKINDEX.tar.gz ]; then
    echo "/media/usb/ewr.edge.kernel.org/main" >> /etc/apk/repositories
fi

if [ -f /media/usb/ewr.edge.kernel.org/community/x86/APKINDEX.tar.gz ]; then
    echo "/media/usb/ewr.edge.kernel.org/community" >> /etc/apk/repositories
fi
